package classdiagram;

import util.PrimitiveType;

public class RamsesScriptMethod extends MethodBuilder {
	private String var;
	// public RamsesScriptMethod(ClassDiagramBuilder cd,String name, String returnType, String modifiers,
	// 		String[] argsName,String[] argsType,String var) {
	// 	super(cd,name, returnType, modifiers, argsName, argsType);
	// 	this.var=var;
	// }

	public RamsesScriptMethod(ClassDiagramBuilder cd,String name, String returnType, int modifiers,
			String[] argsName,String[] argsType, String[] exceptions, String var) {
		super(cd,name, returnType, modifiers, argsName, argsType, exceptions);
		this.var=var;
	}

	//@Override
	public String build() {
		StringBuffer sb=new StringBuffer();
		sb.append("ret="+PrimitiveType.getType(returnType,cd)+";\n");
		sb.append("types=new UMLType["+argsType.length+"];\n");
		sb.append("names=new String["+argsName.length+"];\n");
		sb.append("exceptions=null;\n");
		for(int i=0;i<argsName.length;i++)
			sb.append("names["+i+"]=\""+argsName[i]+"\";\n");
		for(int i=0;i<argsType.length;i++)
			sb.append("types["+i+"]="+PrimitiveType.getType(argsType[i],cd)+";\n");
		if (exceptions != null){
			sb.append("exceptions=new UMLType["+exceptions.length+"];\n");	
			for (int i=0;i<exceptions.length;i++)
				sb.append("exceptions["+i+"]="+PrimitiveType.getType(exceptions[i],cd)+";\n");
		}
		// sb.append(var+".addMethod(\""+name+"\",\""+modifiers+"\",types,names,ret, exceptions);\n");
		sb.append(var+".addMethod(\""+name+"\","+modifiers+",types,names,ret, exceptions);\n");
		return sb.toString();
	}

}
