package classdiagram;

import util.PrimitiveType;

public class RamsesScriptAssocation extends AssociationBuilder {
	private String var;
	public RamsesScriptAssocation(ClassDiagramBuilder cd,String label, String target,
			String sourceLowerValue, String sourceUpperValue,
			String targetLowerValue, String targetUpperValue,String var) {
		super(cd,label, target, sourceLowerValue, sourceUpperValue, targetLowerValue,
				targetUpperValue);
		this.var=var;
	}
	//@Override
	public String build() {
		return var+".addAssocation("+PrimitiveType.getType(target, cd)+",\""+sourceLowerValue+"\",\""+sourceUpperValue+"\",\""+targetLowerValue+"\",\""+targetUpperValue+"\",\""+label+"\");\n";
	}

}
