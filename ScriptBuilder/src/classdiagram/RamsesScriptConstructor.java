package classdiagram;

import util.PrimitiveType;

public class RamsesScriptConstructor extends ConstructorBuilder {

	public RamsesScriptConstructor(ClassDiagramBuilder cd,String modifiers, String[] argsName,
			String[] argsType) {
		super(cd,modifiers, argsName, argsType);
	}

	//@Override
	public String build() {
		StringBuffer sb=new StringBuffer();
		sb.append("types=new UMLType["+argsType.length+"];\n");
		sb.append("names=new String["+argsName.length+"];\n");
		for(int i=0;i<argsName.length;i++)
			sb.append("names["+i+"]=\""+argsName[i]+"\";\n");
		for(int i=0;i<argsType.length;i++)
			sb.append("types["+i+"]="+PrimitiveType.getType(argsType[i],cd)+";\n");
		sb.append("cl.addMethod(\"\",\""+modifiers+"\",types,names,null);\n");
		return sb.toString();
	}

}
