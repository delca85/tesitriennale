package classdiagram;

import util.PrimitiveType;

public class RamsesScriptField extends FieldBuilder {
	private String var;
	public RamsesScriptField(ClassDiagramBuilder cd,String name, String type, String modifiers, boolean multiplicity, String var) {
		super(cd,name, type, modifiers, multiplicity);
		this.var=var;
	}

	//@Override
	public String build() {
		if (!name.equals("i_DAE_IC_0"))
			return var+".addAnAttribute(\""+name+"\",\""+modifiers+"\","+PrimitiveType.getType(type,cd)+ "," + this.multiplicity +");\n";
		return "";
	}

}
