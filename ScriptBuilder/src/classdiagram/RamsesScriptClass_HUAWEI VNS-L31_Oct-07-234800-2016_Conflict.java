package classdiagram;

import util.PrimitiveType;


public class RamsesScriptClass extends ClassBuilder {
	private String var;
	public RamsesScriptClass(ClassDiagramBuilder cd,String name,String visibility, boolean isAbstract, String ext,
			String[] impls,String var) {
		super(cd, name, visibility, isAbstract, ext, impls);
		this.var=var;
	}
	public RamsesScriptClass(ClassDiagramBuilder cd,String name,String var){
		super(cd, name,"public",false,null,new String[]{});
		this.var=var;
	}
	//@Override
	public String build() {
		StringBuffer sb=new StringBuffer();
		sb.append(var+"=getClass(\""+name+"\",\""+visibility+"\","+isAbstract+",c);\n");
//		sb.append(var+"=c.createNewClass(\""+name+"\",\""+visibility+"\","+isAbstract+");\n");
		String ie;
		ClassBuilder cb;
		if(ext!=null){
			//Da modificare
			sb.append(var+".addGeneralization((Class)"+PrimitiveType.getType(ext, cd)+");\n");
		}
		for(String impl:impls){
			try{
				cb=cd.getType(impl);
				ie="getInterface(\""+cb.getName()+"\",\""+cb.getModifiers()+"\",c)";
			}catch(RuntimeException e){
				ie="getInterface(\""+impl+"\",\"public\",c)";
			}
			sb.append(var+".addAnInterface((Interface)"+ie+");\n");
		}
		sb.append(fbs.build());
		sb.append(cbs.build());
		sb.append(mbs.build());
		return sb.toString();
	}
	//@Override
	public FieldBuilder addField(String name, String type,String modifiers, boolean multiplicity) {
		RamsesScriptField field=new RamsesScriptField(cd,name, type, modifiers, multiplicity, var);
		addField(field);
		return field;
	}
	//@Override
	public MethodBuilder addMethod(String name,String returnType,String modifiers,String[] argsName,String[] argsType) {
		RamsesScriptMethod method =new RamsesScriptMethod(cd,name, returnType, modifiers, argsName, argsType,var);
		addMethod(method);
		return method;
	}
	//@Override
	public ConstructorBuilder addConstructor(String modifiers,
			String[] argsName, String[] argsType) {
		RamsesScriptConstructor constructor=new RamsesScriptConstructor(cd,modifiers, argsName, argsType);
		addConstructor(constructor);
		return constructor;
	}
	//@Override
	public AssociationBuilder addAssociation(String target, String label,
			String sourceLowerValue, String sourceUpperValue,
			String targetLowerValue, String targetUpperValue) {
		RamsesScriptAssocation association=new RamsesScriptAssocation(cd,label, target, sourceLowerValue, sourceUpperValue, targetLowerValue, targetUpperValue,var);
		addAssociation(association);
		return association;
	}


}
