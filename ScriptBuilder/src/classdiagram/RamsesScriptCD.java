package classdiagram;


public class RamsesScriptCD extends ClassDiagramBuilder{
	private String varClass,varInterface;
	public RamsesScriptCD(String name) {
		super(name);
		varClass="cl";
		varInterface="iface";
	}

	//@Override
	public String build() {
		StringBuffer sb=new StringBuffer();
		sb.append("c=r.addClassDiagram(\""+name+"\");\n");
		sb.append(cbs.build());
		sb.append(ibs.build());
		return sb.toString();
	}

	//@Override
	public ClassBuilder addClass(String name) {
		RamsesScriptClass clazz=new RamsesScriptClass(this,name,varClass);
		addClass(clazz);
		return clazz;
	}

	//@Override
	public ClassBuilder addInterface(String name) {
		RamsesScriptInterface iface=new RamsesScriptInterface(this,name,varInterface);
		addInterface(iface);
		return iface;
	}
	public static void main(String[] args){
		RamsesScriptCD a=new RamsesScriptCD("ClassDiagram1");
		//a.build();
		//a.exportToFile();
	}

}
