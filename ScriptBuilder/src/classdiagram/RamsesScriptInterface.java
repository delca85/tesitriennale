package classdiagram;

import util.PrimitiveType;

public class RamsesScriptInterface extends ClassBuilder {
	private String var;
	public RamsesScriptInterface(ClassDiagramBuilder cd, String name,
			String visibility, boolean isAbstract, String ext, String[] impls,String var) {
		super(cd, name, visibility, isAbstract, ext, impls);
		this.var=var;
	}
	public RamsesScriptInterface(ClassDiagramBuilder cd,String name,String var){
		super(cd, name,"public",false,null,new String[]{});
		this.var=var;
	}
	//@Override
	public String build() {
		StringBuffer sb=new StringBuffer();
		String ie;
		ClassBuilder cb;
		sb.append(var+"=getInterface(\""+name+"\",\""+visibility+"\",c);\n");
//		sb.append(var+"=c.createNewInterface(\""+name+"\",\""+visibility+"\",false);\n");
		for(String impl:impls){
			try{
				cb=cd.getType(impl);
				ie="getInterface(\""+cb.getName()+"\",\""+cb.getModifiers()+"\",c)";
			}catch(RuntimeException e){
				ie="getInterface(\""+impl+"\",\"public\",c)";
			}
			sb.append(var+".addGeneralization((Interface)"+ie+");\n");
		}
		sb.append(fbs.build());
		//sb.append(cbs.build());
		sb.append(mbs.build());
		return sb.toString();
	}

	//@Override
	public FieldBuilder addField(String name, String type, String modifiers, boolean multiplicity) {
		RamsesScriptField field=new RamsesScriptField(cd,name, type, modifiers, multiplicity, var);
		addField(field);
		return field;
	}

	// //@Override
	// public MethodBuilder addMethod(String name, String returnType,
	// 		String modifiers, String[] argsName, String[] argsType) {
	// 	RamsesScriptMethod method =new RamsesScriptMethod(cd,name, returnType, modifiers, argsName, argsType,var);
	// 	addMethod(method);
	// 	return method;
	// }

	//@Override
	public MethodBuilder addMethod(String name, String returnType,
			int modifiers, String[] argsName, String[] argsType, String[] exceptions) {
		RamsesScriptMethod method =new RamsesScriptMethod(cd,name, returnType, modifiers, argsName, argsType, exceptions, var);
		addMethod(method);
		return method;
	}

	//@Override
	public ConstructorBuilder addConstructor(String modifiers,
			String[] argsName, String[] argsType) {
		return null;
	}

	//@Override
	public AssociationBuilder addAssociation(String target, String label,
			String sourceLowerValue, String sourceUpperValue,
			String targetLowerValue, String targetUpperValue) {
		// TODO Auto-generated method stub
		RamsesScriptAssocation association=new RamsesScriptAssocation(cd,label, target, sourceLowerValue, sourceUpperValue, targetLowerValue, targetUpperValue,var);
		addAssociation(association);
		return association;
	}

}
