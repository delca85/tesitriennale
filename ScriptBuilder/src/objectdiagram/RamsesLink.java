package objectdiagram;

import java.util.Iterator;

import util.BuilderArrayList;
import util.UtilBuilder;

public class RamsesLink extends Link {

	public RamsesLink(String label, String src, String dest, ObjectDiagramBuilder odb) {
		super(label, src, dest, odb);
	}

	public String build() {
		StringBuffer sb=new StringBuffer();
		String srcClazz = odb.getInstanceFromName(src).getClassName();
		String destClazz = odb.getInstanceFromName(dest).getClassName();
		sb.append("firstInstance=od.getInstance(\""+src+"\", \"" + srcClazz + "\");\n");
		sb.append("secondInstance=od.getInstance(\""+dest+"\", \"" + destClazz + "\");\n");
		sb.append("od.connectInstances(firstInstance, secondInstance, \""+label+"\");\n");
		return sb.toString();
	}

	public Link clone() {
		Link clone = new RamsesLink(this.label, this.src, this.dest, this.odb);
		return clone;
	}
}
