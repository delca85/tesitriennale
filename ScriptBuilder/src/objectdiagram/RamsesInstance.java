package objectdiagram;

import java.util.Iterator;

import util.BuilderArrayList;
import util.UtilBuilder;

public class RamsesInstance extends Instance {

	public RamsesInstance(Object o, String name, boolean singleInstance, ObjectDiagramBuilder odb) {
		super(o, name, singleInstance, odb);
	}

	public String build() {
		StringBuffer sb=new StringBuffer();
		sb.append("cl=c.getClass(\""+getClassName()+"\");\n");
		sb.append("odIn="+"od.addInstance(\""+name+"\", cl);\n");
		return sb.toString();
	}

	public String buildSlots(){
		StringBuffer sb = new StringBuffer();
		sb.append("cl=c.getClass(\""+getClassName()+"\");\n");
		sb.append("odIn = od.getInstance(\"" + name + "\", \""+getClassName()+"\");\n");
		sb.append(slots.build());
		return sb.toString();
	}

	public Instance clone(){
		Instance clone = new RamsesInstance(this.obj, this.name, this.singleInstance, this.odb);
		return clone;
	}
}
