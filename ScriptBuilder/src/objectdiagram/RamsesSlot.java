package objectdiagram;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import util.BuilderArrayList;
import util.UtilBuilder;
import util.DeepClone;

public class RamsesSlot extends Slot {

	public RamsesSlot(Instance in, String name, Object value, ObjectDiagramBuilder odb) {
		super(in, name, value, odb);
	}

	public String build() {
		StringBuffer sb=new StringBuffer();
		sb.append("odAttr=cl.getAttribute(\"" + name + "\");\n");
		if (getValueString().size() != 0)
			sb.append("odSlotString = new ArrayList<String>(Arrays.asList" + convert(getValueString()) + ");\n");
		if (getValueString().size() == 0)
			sb.append("odSlotString = new ArrayList<String>();\n");
		sb.append("odIn.addASlot(\"" + name + "\", odSlotString);\n");
		return sb.toString();
	}

	public Slot clone() {
		Object cloneValue = DeepClone.deepClone(this.value);
		Slot clone = new RamsesSlot(in, name, cloneValue, odb);
		return clone;
	}

	private String convert(List<String> getValueStringResult){	//trasformazione per passare stringhe
		StringBuffer sb = new StringBuffer();
		sb.append("(");
		for (String s : getValueStringResult)
			sb.append("\"" + s + "\",");
		if (getValueStringResult.size() != 0)
			sb.deleteCharAt(sb.length()-1);
		sb.append(")");
		return sb.toString();
	}

}
