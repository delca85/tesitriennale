package objectdiagram;

import util.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.HashMap;
import java.lang.Number.*;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;


import util.BuilderArrayList;
import util.UtilBuilder;

import builder.Builder;

public class RamsesOD extends ObjectDiagramBuilder {

	public RamsesOD(String name) {
		super(name);
	}

	public String build() {
		StringBuffer sb = new StringBuffer();
		String inside;
		sb.append("od=r.addObjectDiagram(\""+name+"\");\n"); 
		sb.append(instances.build());
		for (Instance in : instances.values())
			sb.append(((RamsesInstance)in).buildSlots());
		sb.append(links.build());
		sb.append(breakpoints.build());
		return sb.toString();
	}

	public String buildFlows() {
		return null;
	}

	public void addInstance(Object o, String name, boolean singleInstance) {
		Instance in = new RamsesInstance(o, name, singleInstance, this);
		if (in.getName() != null) {
			System.out.println("[RAMSESOD]Creata Instance, name: " + in.getName() + " obj: " + in.getObject().toString());
			this.addInstance(in);
		}
	}

	public void addLink(String label, String source, String destination, Object thisInstance, Object o) {
		String sourceR, destinationR;
		sourceR = getInstanceNameForLink(source, thisInstance);
		destinationR = getInstanceNameForLink(destination, o);
		Link link = new RamsesLink(label, sourceR, destinationR, this);
		if (link.getODName() == null)	//links non creata, ma linkTempCreata
			System.out.println("[RAMSESOD]Creato LinkTmp, src: " + sourceR);
		if (link.getODName() != null) {
			System.out.println("[RAMSESOD]Creato Link, src: " + link.getSrc() + " destination: " + link.getDest());
			this.addLink(link);
		}
	}

	public void rmLink(String label, String source, String destination, Object thisInstance, Object o) {
		String sourceL, destinationL;
		sourceL = getInstanceNameForLink(source, thisInstance);
		destinationL = getInstanceNameForLink(destination, o);
		Link link = new RamsesLink(label, sourceL, destinationL, this);
		if (this.rmLink(link))
			System.out.println("[RAMSESOD]Rimosso Link, src: " + link.getSrc() + " destination: " + link.getDest());
		else
			System.out.println("[RAMSESOD]Non rimosso Link, src: " + link.getSrc() + " destination: " + link.getDest());			
	}

	public void addLinksFromTemp(Object o, String name, boolean singleInstance) {
		Link link;
		ArrayList<LinkInfo> linkInfoSrcArr, linkInfoDestArr;
		//arrayList su cui ciclare per creare link non tmp
		linkInfoSrcArr = getTempSrcLinks(o);		
		linkInfoDestArr = getTempDestLinks(o);
		//rimozione dalle istanze temporanee per permettere costruzione della instance
		rmTempInstance(o);
		addInstance(o, name, singleInstance);
		if (linkInfoSrcArr != null)
			for (LinkInfo linkInfo : linkInfoSrcArr){
				link = new RamsesLink(linkInfo.getLabel(), name, linkInfo.getOther(), this);
				addLink(link);
			}
		if (linkInfoDestArr != null)
			for (LinkInfo linkInfo : linkInfoDestArr){
				link = new RamsesLink(linkInfo.getLabel(), linkInfo.getOther(), name, this);
				addLink(link);
			}
		rmTempSrcLinks(o);		
		rmTempDestLinks(o);
	}

	public void addSlots(Instance in) {
		ArrayList<String> slotsString = classSlots.get(in.getClassName());		//recupero i nomi dei campi che mi interessano per il diagramma, a partire dalla classe dell'istanza corrente
		ArrayList<?> valueArr = null;
		BuilderArrayList<Slot> slots=null;
		Slot s;
		Field f;
		Object value;
		int modifiers;
		Class clazz = in.getObject().getClass();		//recupero proprietà dei campi tramite reflection
		if (slotsString != null) {
			slots = new BuilderArrayList<Slot>();
			for (String fString : slotsString){
				s = null;
				try {
					f = clazz.getDeclaredField(fString);
					f.setAccessible(true);
					value = f.get(in.getObject());
					modifiers = f.getModifiers();
					s = new RamsesSlot(in, fString, value, this);
					if (s != null)
						slots.add(s);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		if (slots != null)
			in.setSlots(slots);
	}

	public ObjectDiagramBuilder clone(){
		ObjectDiagramBuilder clone = new RamsesOD(name);
		clone.instances = DeepClone.deepClone(instances);
		clone.links = DeepClone.deepClone(links);
		for (Map.Entry<String, String> entry : instancesNameObject.entrySet())
			clone.instancesNameObject.put(entry.getKey(), entry.getValue());		//utile per recuperare le istanze in fase di build
		return clone;
	}

	private String getClassName(Object o) {
		return o.getClass().getName();
	}

}
