package sequencediagram;

import java.util.Iterator;
import java.lang.reflect.*;

import util.BuilderArrayList;
import util.UtilBuilder;

import builder.Builder;

public class RamsesSD extends SequenceDiagramBuilder {

	public RamsesSD(String name) {
		super(name);
	}

	//@Override
	public String build() {
		StringBuffer sb = new StringBuffer();
		Lifeline l;
		String inside;
		//Iterator<Lifeline> i = lifelines.values().iterator();
		//Iterator<ControlFlow> cfs;
		sb.append("sd=r.addSequenceDiagram(\""+name+"\");\n"); //Correct
		//sb.append("ad=r.getAllActivityDiagram()[0];\n");
		sb.append(lifelines.build());
		sb.append(messages.build());
		//sb.append(buildFlows());
		/*
		while (i.hasNext()) {
			ce = i.next();
			
			inside = ce.getInsideOf();
			BuilderArrayList<ControlFlow> flows = ce.getFlows();
			
			if (!flows.isEmpty()) {
				sb.append("ase=");
				sb.append(UtilBuilder.getElementString(ce, elements) + ";\n");
				// sb.append("ase="+inside==null?"getElement(\""+ce.getName()+"\",ad)":"getElement(\""+ce.getName()+"\",getElement(\""+inside+"\",ad))");
				cfs=flows.iterator();
				while(cfs.hasNext())
					sb.append("ase"+cfs.next().build());
			}
		}
		*/
		return sb.toString();
	}

	//@Override
	public Lifeline addLifeline(String name, String clazz) {
		RamsesLifeline rl = new RamsesLifeline(name, clazz, this);
		this.addLifeline(rl);
		return rl;
	}
	
	//@Override
	public Message addMessage(String sender, Class senderClass, String receiver, Class receiverClass, Method m, boolean async, Object[] args) {
		Message rm;
		if (async)
			rm = new RamsesAsyncMessage(this, sender, senderClass, receiver, receiverClass, m, args);
		else
			rm = new RamsesMessage(this, sender, senderClass, receiver, receiverClass, m, args);
		this.addMessage(rm);
		return rm;
	}
	
	//@Override
	public Message addInsideMessage(String sender, Class senderClass, String receiver, Class receiverClass, Method m, boolean async, Object[] args) {
		Message rm;
		if (async)
			rm = new RamsesAsyncMessage(this, sender, senderClass, receiver, receiverClass, m, true, args);
		else
			rm = new RamsesMessage(this, sender, senderClass, receiver, receiverClass, m, true, args);
		this.addInsideMessage(rm);
		return rm;
	}

	//@Override
	public String buildFlows() {
		return lifelines.buildFlows();
	}

}
