package sequencediagram;

import java.util.Iterator;
import java.lang.reflect.*;

import util.BuilderArrayList;
import util.UtilBuilder;

public class RamsesMessage extends Message {
//	protected String varName;
	public RamsesMessage(SequenceDiagramBuilder sd, String sender, Class senderClass, String receiver, Class receiverClass, Method m, Object[] params) {
		super(sd,"sdm", sender, receiver,senderClass, receiverClass,m, params);
//		varName="ase";
	}
	
	public RamsesMessage(SequenceDiagramBuilder sd, String sender, Class senderClass, String receiver, Class receiverClass, Method m, boolean inside, Object[] params) {
		super(sd,"sdm", sender, receiver,senderClass, receiverClass,m, inside, params);
//		varName="ase";
	}

	public String build() {
		StringBuffer sb=new StringBuffer();
		//sb.append("CallAction "+name+"=");
		/*Method(Reification reification, String name, String visibility, 
			UMLType[] parameterTypes, String[] parameterNames, 
			UMLType returnParameterType, String returnParameterName, 
			UMLEntityWithMethodAndParent owner)*/
		sb.append("cl=c.getClass(\""+receiverClass.getCanonicalName()+"\");\n");
		Class[] par = method.getParameterTypes();
		sb.append("types = new UMLType["+par.length+"];\n");
		for (int i = 0; i < par.length; i++)
			sb.append("types["+i+"]=getClass(\""+par[i].getSimpleName()+"\", \"public\", false, c);\n");		
		//sb.append("cl.getMethod(\""+m.getName()+"\")[0];\n");
		if (!isInside()) {
			sb.append(varName+"="+
				"sd.addSynchronousMessage(sd.getLifeLine(\""+sender+"\",c.getClass(\""+senderClass.getCanonicalName()+"\")),sd.getLifeLine(\""+receiver+"\",cl), getMethod(\""+method.getName()+"\",cl,types), \"UAL\", new String[]{");
				Object[] p = getParams();
				for (int i = 0; i < p.length; i++) {
					sb.append("\""+p[i]+"\"");
					if (i != p.length-1)
						sb.append(",");
				}
			sb.append("});\n");
		}
		else {
			sb.append(varName+"=messageStack.peek();\n");
			sb.append(varName+"=((MessageWithExecution)"+varName+
				").addSyncInside(sd.getLifeLine(\""+sender+"\",c.getClass(\""+senderClass.getCanonicalName()+"\")),sd.getLifeLine(\""+receiver+"\",cl), getMethod(\""+method.getName()+"\",cl,types), \"UAL\", new String[]{");
				Object[] p = getParams();
				for (int i = 0; i < p.length; i++) {
					sb.append("\""+p[i]+"\"");
					if (i != p.length-1)
						sb.append(",");
				}
			sb.append("});\n");
		}
		sb.append("messageStack.push("+varName+");\n");
		for (Message mex : insideMessage) {
			//System.out.println("Fuond inside!!");
			sb.append(mex.build());
		}
		sb.append("messageStack.pop();\n");
		//sb.append("message " + name+"\n");
		//sb.append(flows.build());
		//sb.append(pins.build());
		return sb.toString();
	}
}
/*
TO ADD
Class[] par = method.getParameterTypes();
print type = new type[par.length];
for (int i = 0; i < par.length; i++) {
	print types[i]=getClass(par[i].getCanonicalName(),"public",false,c);
}
print:
for (Method me : cl.getAllMethods()) {
	if (me.getAllInParameters().length == par.length) {
		for (int i = 0; i < par.length; i++) {
			if (!me.getAllInParameters[i].equals(type[i]))
				break;
		}
		
	}
}

*/