package sequencediagram;

import java.util.Iterator;
import java.lang.reflect.*;

import util.BuilderArrayList;
import util.UtilBuilder;

public class RamsesAsyncMessage extends Message {
//	protected String varName;
	public RamsesAsyncMessage(SequenceDiagramBuilder sd, String sender, Class senderClass, String receiver, Class receiverClass, Method m, Object[] params) {
		super(sd,"sdm", sender, receiver,senderClass, receiverClass,m, params);
//		varName="ase";
	}
	
	public RamsesAsyncMessage(SequenceDiagramBuilder sd, String sender, Class senderClass, String receiver, Class receiverClass, Method m, boolean inside, Object[] params) {
		super(sd,"sdm", sender, receiver,senderClass, receiverClass,m, inside, params);
//		varName="ase";
	}

	public String build() {
		StringBuffer sb=new StringBuffer();
		//sb.append("CallAction "+name+"=");
		/*Method(Reification reification, String name, String visibility, 
			UMLType[] parameterTypes, String[] parameterNames, 
			UMLType returnParameterType, String returnParameterName, 
			UMLEntityWithMethodAndParent owner)*/
		sb.append("cl=c.getClass(\""+receiverClass.getCanonicalName()+"\");\n");
		//sb.append("cl.getMethod(\""+m.getName()+"\")[0];\n");
		if (!isInside()) {
			sb.append(varName+"="+
				"sd.addAsynchronousMessage(sd.getLifeLine(\""+sender+"\",c.getClass(\""+senderClass.getCanonicalName()+"\")),sd.getLifeLine(\""+receiver+"\",cl), cl.getMethod(\""+method.getName()+"\")[0]);\n");
		}
		else {
			sb.append(varName+"=messageStack.peek();\n");
			sb.append(varName+"=((MessageWithExecution)"+varName+
				").addAsyncInside(sd.getLifeLine(\""+sender+"\",c.getClass(\""+senderClass.getCanonicalName()+"\")),sd.getLifeLine(\""+receiver+"\",cl), cl.getMethod(\""+method.getName()+"\")[0]);\n");
		}
		sb.append("messageStack.push("+varName+");\n");
		for (Message mex : insideMessage) {
			//System.out.println("Fuond inside!!");
			sb.append(mex.build());
		}
		sb.append("messageStack.pop();\n");
		//sb.append("message " + name+"\n");
		//sb.append(flows.build());
		//sb.append(pins.build());
		return sb.toString();
	}
}