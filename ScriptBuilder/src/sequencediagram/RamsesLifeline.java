package sequencediagram;

import java.util.Iterator;

import util.BuilderArrayList;
import util.UtilBuilder;

public class RamsesLifeline extends Lifeline {
//	protected String varName;
	public RamsesLifeline(String name, String clazz, SequenceDiagramBuilder sd) {
		super(name,clazz,sd,"sdl");
//		varName="ase";
	}

	public String build() {
		StringBuffer sb=new StringBuffer();
		//sb.append("CallAction "+name+"=");
		sb.append("cl=c.getClass(\""+clazz+"\");\n");
		sb.append(varName+"="+"sd.addNewLifeLine(\""+name+"\",cl);\n");
		//sb.append("lifeline " + name+"\n");
		//sb.append(flows.build());
		//sb.append(pins.build());
		return sb.toString();
	}
}
