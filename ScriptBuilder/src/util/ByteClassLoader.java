package util;

public class ByteClassLoader extends ClassLoader {
	public Class<?> loadClass(String name,byte[] clazz){
		Class<?> ret=this.defineClass(name, clazz, 0, clazz.length);
		if(ret==null)
			throw new RuntimeException("Bytecode error!");
		return ret;
	}
}
