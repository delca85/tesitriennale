package util;

import activitydiagram.ControllableElement;
import classdiagram.ClassBuilder;
import classdiagram.RamsesScriptInterface;

public class UtilBuilder {
	private static int COUNTER;
	public static String getStringForType(ClassBuilder cb){
		return (RamsesScriptInterface.class.isAssignableFrom(cb.getClass()))?"getInterface(\""+cb.getName()+"\",\""+cb.getModifiers()+"\",c)":"getClass(\""+cb.getName()+"\",\""+cb.getModifiers()+"\","+cb.isAbstract()+",c)";
	}
	public static String getElementString(ControllableElement ce,BuilderHashMap<String, ControllableElement> elements){
		if(ce.getInsideOf()==null)
			return "getElement(\""+ce.getName()+"\",ad)";
		else
			return "getElement(\""+ce.getName()+"\",(GroupActivityNode)"+getElementString(elements.get(ce.getInsideOf().getName()),elements)+")";
		
	}
	public static int getNextId() {
		return COUNTER++;
	}
}
