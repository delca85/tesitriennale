package util;

import classdiagram.ClassBuilder;
import classdiagram.ClassDiagramBuilder;

public enum PrimitiveType {
	INT,BOOLEAN,DOUBLE,FLOAT,BYTE,CHAR,LONG;
	public static String getType(String type,ClassDiagramBuilder cd) {
		if(type.equals("void"))
			return "getClass(\"void\",\"public\",false,c)";
		try{		
			return "new PrimitiveType(Type."+PrimitiveType.valueOf(type.toUpperCase()).name()+")";
		}catch(IllegalArgumentException e){
			ClassBuilder c;
			try{
				c=cd.getType(type);
			}catch(RuntimeException ex){
				return "getClass(\""+type+"\",\"public\",false,c)";
			}
			return UtilBuilder.getStringForType(c);
		}
			
	}
}
