package factory;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;



import util.PrimitiveType;

import activitydiagram.ActivityDiagramBuilder;
import activitydiagram.CompositeElement;
import activitydiagram.ControllableElement;
import activitydiagram.RamsesAD;
import classdiagram.ClassBuilder;
import classdiagram.ClassDiagramBuilder;
import classdiagram.RamsesScriptCD;
import classdiagram.RamsesScriptMethod;
import sequencediagram.SequenceDiagramBuilder;
import sequencediagram.RamsesSD;
import objectdiagram.ObjectDiagramBuilder;
import objectdiagram.RamsesOD;
import exporter.Exporter;

public class RamsesScriptFactory extends DiagramFactory{
	private String build;
	public RamsesScriptFactory(){
		super();
	}
	//@Override
	public ClassDiagramBuilder getClassDiagram(String name) {
		if(classDiagrams.containsKey(name))
			return classDiagrams.get(name);
		else{
			ClassDiagramBuilder cdb=new RamsesScriptCD(name);
			classDiagrams.put(name, cdb);
			return cdb;
		}
	}

	//@Override
	public ActivityDiagramBuilder getActivityDiagram(String name) {
		if(activityDiagrams.containsKey(name))
			return activityDiagrams.get(name);
		else{
			ActivityDiagramBuilder adb=new RamsesAD(name);
			activityDiagrams.put(name, adb);
			return adb;
		}
	}
	
	//@Override
	public SequenceDiagramBuilder getSequenceDiagram(String name) {
		if(sequenceDiagrams.containsKey(name))
			return sequenceDiagrams.get(name);
		else{
			SequenceDiagramBuilder sdb=new RamsesSD(name);
			sequenceDiagrams.put(name, sdb);
			return sdb;
		}
	}

	//@Override
	public ObjectDiagramBuilder getObjectDiagram(String name) {
		if(objectDiagrams.containsKey(name))
			return objectDiagrams.get(name);
		else{
			ObjectDiagramBuilder odb=new RamsesOD(name);
			objectDiagrams.put(name, odb);
			return odb;
		}
	}


	//@Override
	public void buildClassDiagrams() {
		StringBuffer sb=new StringBuffer();
		sb.append("import ramses.reification.*; \n");
		sb.append("import ramses.reification.PrimitiveType.Type;\n");
		sb.append("import ramses.reification.classdiagram.*;\n");
		sb.append("import ramses.reification.classdiagram.Class; \n");
		sb.append("import ramses.reification.exception.InterfaceDoesNotExistException;\n");
		sb.append("public class ClassDiagrams{\n");
		sb.append("private static String blank=\"empty.emx\";\n");
		sb.append("private static Reification r;\n");
		sb.append("private static ClassDiagram c;\n");
		sb.append("private static Class cl;\n");
		sb.append("private static Interface iface;\n");
		sb.append("private static UMLType[] types;\n");
		sb.append("private static UMLType ret;\n");
		sb.append("private static String[] names;\n");
		sb.append("private static UMLType[] exceptions;\n");		
		sb.append("public static void main(String[] args) throws Exception{\n");
		sb.append("r=new Reification(blank);\n");
		sb.append(classDiagrams.build());
		sb.append("r.save(\"ClassDiagrams.emx\");\n");
		sb.append("}\n");
		
		sb.append("public static Class getClass(String name,String modifiers,boolean abs,ClassDiagram cd){\n");
		sb.append("Class ret;\n " +
				  "return (ret=cd.getClass(name))!=null?ret:cd.createNewClass(name, modifiers, abs);\n");
		sb.append("}\n");
		
		sb.append("public static Interface getInterface(String name,String modifiers,ClassDiagram cd){\n");
		sb.append("Interface ret;\n " +
				  "try {return cd.getInterface(name);} catch (InterfaceDoesNotExistException e) {return cd.createNewInterface(name, modifiers, false);}\n");
		sb.append("}\n");
		
		sb.append("}");
		build=sb.toString();
	}
	public static void main(String[] args){
		RamsesScriptFactory rsf=new RamsesScriptFactory();
		ActivityDiagramBuilder ad=rsf.getActivityDiagram("ad1");
		ControllableElement ce=ad.addAcceptEvent("acceptEvento1");
		ControllableElement ce1=ad.addAcceptEvent("acceptEvento2");
		ad.addCallAction("ca1", "", "");
		ad.addConditional("cond");
		ad.addDecision("dec");
		ad.addFinal("fin");
		ad.addFork("fork");
		ad.addInit("init");
		ad.addJoin("join");
		CompositeElement l=ad.addLoop("loop");
		l.addAcceptEvent("lae");
		CompositeElement l2=l.addLoop("iLoop");
		
		l2.addCallAction("caiLoop","","");
		l.addCallAction("caLoop","","");
		ad.addMerge("merge");
		ad.addPartition("parition");
		ad.addSendSignal("send");
		ad.addStructured("struct");
		ce.addCFlowTo("acceptEvento2", "wewe", "UAL","guardia", " ");
		rsf.buildActivityDiagrams();
		rsf.exportToFile("ActivityDiagrams.java");
	}
	//@Override
	public void exportToFile(String path) {
		try {
	        BufferedWriter out = new BufferedWriter(new FileWriter(path));
	        out.write(build);
	        out.close();
	    } catch (IOException e) {
	    	e.printStackTrace();
	    }
		
	}
	//@Override
	public void buildActivityDiagrams() {
		StringBuffer sb=new StringBuffer();
		sb.append("import ramses.reification.*; \n");
		sb.append("import ramses.reification.PrimitiveType.Type;\n");
		sb.append("import ramses.reification.activitydiagram.*;\n");
		//sb.append("import ramses.reification.classdiagram.Class; \n");
		sb.append("import ramses.reification.exception.InterfaceDoesNotExistException;\n");
		sb.append("public class ActivityDiagrams{\n");
		sb.append("private static String blank=\"empty.emx\";\n"); //Correct
//		sb.append("private static String blank=\"DemoActivity_output.emx\";\n");
		
		sb.append("private static Reification r;\n");
		sb.append("private static ActivityDiagram ad;\n");
		sb.append("private static ActivitySimpleElement ase;\n");
		sb.append("private static ActivityElement ae;\n");
		sb.append("private static GroupActivityNode gan;\n");
		//sb.append("private static Interface iface;\n");
		//sb.append("private static UMLType[] types;\n");
		sb.append("private static UMLType pin;\n");
		//sb.append("private static String[] names;\n");
		sb.append("public static void main(String[] args) throws Exception{\n");
		sb.append("r=new Reification(blank);\n");
		sb.append(activityDiagrams.build());
		sb.append("r.save(\"ActivityDiagrams.emx\");\n");
		sb.append("}\n");
		//sb.append("public static ActivitySimpleElement getElement(String name,ActivityDiagram ad){return ad.getAcceptEventAction(name);}\n");
		
		//Bello questo metodo
		sb.append("public static ActivitySimpleElement getElement(String name,ActivityDiagram ad) {"+
		"ActivitySimpleElement ret=null;"+
		"if((ret=ad.getAcceptEventAction(name))!=null) return ret;"+
		"if((ret=ad.getCallAction(name))!=null) return ret;"+
		"if((ret=ad.getCallBehavior(name))!=null) return ret;"+
		"if((ret=ad.getActivityPartition(name))!=null) return ret;"+
		"if((ret=ad.getConditionalActivityNode(name))!=null) return ret;"+
		"if((ret=ad.getDecisionActivityNode(name))!=null) return ret;"+
		"if((ret=ad.getFinalActivityNode(name))!=null) return ret;"+
		"if((ret=ad.getForkActivityNode(name))!=null) return ret;"+
		"if((ret=ad.getInitialActivityNode(name))!=null) return ret;"+
		"if((ret=ad.getJoinActivityNode(name))!=null) return ret;"+
		"if((ret=ad.getLoopActivityNode(name))!=null) return ret;"+
		"if((ret=ad.getMergeActivityNode(name))!=null) return ret;"+
		"if((ret=ad.getSendSignalAction(name))!=null) return ret;"+
		"if((ret=ad.getStructuredActivityNode(name))!=null) return ret;"+
		"return ret;"+
		"}");
		sb.append("public static ActivitySimpleElement getElement(String name,GroupActivityNode ad) {" +
				"ActivitySimpleElement ret=null;" +
				"if((ret=ad.getAcceptEventAction(name))!=null) return ret;" +
				"if((ret=ad.getCallAction(name))!=null) return ret;" +
				"if((ret=ad.getCallBehavior(name))!=null) return ret;"+
				"if((ret=ad.getConditionalActivityNode(name))!=null) return ret;" +
				"if((ret=ad.getDecisionActivityNode(name))!=null) return ret;" +
				"if((ret=ad.getFinalActivityNode(name))!=null) return ret;" +
				"if((ret=ad.getForkActivityNode(name))!=null) return ret;" +
				"if((ret=ad.getInitialActivityNode(name))!=null) return ret;" +
				"if((ret=ad.getJoinActivityNode(name))!=null) return ret;" +
				"if((ret=ad.getLoopActivityNode(name))!=null) return ret;" +
				"if((ret=ad.getMergeActivityNode(name))!=null) return ret;" +
				"if((ret=ad.getSendSignalAction(name))!=null) return ret;" +
				"if((ret=ad.getStructuredActivityNode(name))!=null) return ret;" +
				"return ret;}");
//		sb.append("public static Class getClass(String name,String modifiers,boolean abs,ClassDiagram cd){\n");
//		sb.append("Class ret;\n " +
//				  "return (ret=cd.getClass(name))!=null?ret:cd.createNewClass(name, modifiers, abs);\n");
//		sb.append("}\n");
//		
//		sb.append("public static Interface getInterface(String name,String modifiers,ClassDiagram cd){\n");
//		sb.append("Interface ret;\n " +
//				  "try {return cd.getInterface(name);} catch (InterfaceDoesNotExistException e) {return cd.createNewInterface(name, modifiers, false);}\n");
//		sb.append("}\n");
		
		sb.append("}");
		build=sb.toString();
		
	}
	
	public void buildActivityDiagrams(String classDiagramFile) {
		StringBuffer sb=new StringBuffer();
		sb.append("import ramses.reification.*; \n");
		sb.append("import ramses.reification.sequencediagram.*;\n");
		sb.append("import ramses.reification.classdiagram.*;\n");
		sb.append("import ramses.reification.PrimitiveType.Type;\n");
		sb.append("import ramses.reification.activitydiagram.*;\n");
		sb.append("import ramses.reification.classdiagram.Class; \n");
		sb.append("import ramses.reification.exception.InterfaceDoesNotExistException;\n");
		sb.append("public class ActivityDiagrams{\n");
		sb.append("private static String blank=\"empty.emx\";\n"); //Correct
//		sb.append("private static String blank=\"DemoActivity_output.emx\";\n");
		
		sb.append("private static Reification r;\n");
		//
		sb.append("private static ClassDiagram c;\n");
		sb.append("private static Class cl;\n");
		sb.append("private static Interface iface;\n");
		sb.append("private static UMLType[] types;\n");
		sb.append("private static UMLType ret;\n");
		sb.append("private static String[] names;\n");
		sb.append("private static UMLType[] exceptions;\n");
		//
		sb.append("private static ActivityDiagram ad;\n");
		sb.append("private static ActivitySimpleElement ase;\n");
		sb.append("private static ActivityElement ae;\n");
		sb.append("private static GroupActivityNode gan;\n");
		//sb.append("private static Interface iface;\n");
		//sb.append("private static UMLType[] types;\n");
		sb.append("private static UMLType pin;\n");
		//sb.append("private static String[] names;\n");
		sb.append("public static void main(String[] args) throws Exception{\n");
		sb.append("r=new Reification(blank);\n");
		//retrieve class diagram
		try {
			BufferedReader br = new BufferedReader(new FileReader(classDiagramFile));
			try {
			//StringBuilder sb = new StringBuilder();
				String line = br.readLine();
				while (!line.equals("r=new Reification(blank);")) {
					line = br.readLine();
				}
				while (!line.startsWith("r.save")) {
					sb.append(line+"\n");
					line = br.readLine();
				}
			} finally {
				br.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		sb.append(activityDiagrams.build());
		sb.append("r.save(\"ActivityDiagrams.emx\");\n");
		sb.append("}\n");
		//sb.append("public static ActivitySimpleElement getElement(String name,ActivityDiagram ad){return ad.getAcceptEventAction(name);}\n");
		
		sb.append("public static Class getClass(String name,String modifiers,boolean abs,ClassDiagram cd){\n");
		sb.append("Class ret;\n " +
				  "return (ret=cd.getClass(name))!=null?ret:cd.createNewClass(name, modifiers, abs);\n");
		sb.append("}\n");
		
		//Bello questo metodo
		sb.append("public static ActivitySimpleElement getElement(String name,ActivityDiagram ad) {"+
		"ActivitySimpleElement ret=null;"+
		"if((ret=ad.getAcceptEventAction(name))!=null) return ret;"+
		"if((ret=ad.getCallAction(name))!=null) return ret;"+
		"if((ret=ad.getCallBehavior(name))!=null) return ret;"+
		"if((ret=ad.getActivityPartition(name))!=null) return ret;"+
		"if((ret=ad.getConditionalActivityNode(name))!=null) return ret;"+
		"if((ret=ad.getDecisionActivityNode(name))!=null) return ret;"+
		"if((ret=ad.getFinalActivityNode(name))!=null) return ret;"+
		"if((ret=ad.getForkActivityNode(name))!=null) return ret;"+
		"if((ret=ad.getInitialActivityNode(name))!=null) return ret;"+
		"if((ret=ad.getJoinActivityNode(name))!=null) return ret;"+
		"if((ret=ad.getLoopActivityNode(name))!=null) return ret;"+
		"if((ret=ad.getMergeActivityNode(name))!=null) return ret;"+
		"if((ret=ad.getSendSignalAction(name))!=null) return ret;"+
		"if((ret=ad.getStructuredActivityNode(name))!=null) return ret;"+
		"return ret;"+
		"}");
		sb.append("public static ActivitySimpleElement getElement(String name,GroupActivityNode ad) {" +
				"ActivitySimpleElement ret=null;" +
				"if((ret=ad.getAcceptEventAction(name))!=null) return ret;" +
				"if((ret=ad.getCallAction(name))!=null) return ret;" +
				"if((ret=ad.getCallBehavior(name))!=null) return ret;"+
				"if((ret=ad.getConditionalActivityNode(name))!=null) return ret;" +
				"if((ret=ad.getDecisionActivityNode(name))!=null) return ret;" +
				"if((ret=ad.getFinalActivityNode(name))!=null) return ret;" +
				"if((ret=ad.getForkActivityNode(name))!=null) return ret;" +
				"if((ret=ad.getInitialActivityNode(name))!=null) return ret;" +
				"if((ret=ad.getJoinActivityNode(name))!=null) return ret;" +
				"if((ret=ad.getLoopActivityNode(name))!=null) return ret;" +
				"if((ret=ad.getMergeActivityNode(name))!=null) return ret;" +
				"if((ret=ad.getSendSignalAction(name))!=null) return ret;" +
				"if((ret=ad.getStructuredActivityNode(name))!=null) return ret;" +
				"return ret;}");
//		sb.append("public static Class getClass(String name,String modifiers,boolean abs,ClassDiagram cd){\n");
//		sb.append("Class ret;\n " +
//				  "return (ret=cd.getClass(name))!=null?ret:cd.createNewClass(name, modifiers, abs);\n");
//		sb.append("}\n");
//		
//		sb.append("public static Interface getInterface(String name,String modifiers,ClassDiagram cd){\n");
//		sb.append("Interface ret;\n " +
//				  "try {return cd.getInterface(name);} catch (InterfaceDoesNotExistException e) {return cd.createNewInterface(name, modifiers, false);}\n");
//		sb.append("}\n");
		
		sb.append("}");
		build=sb.toString();
		
	}
	
	//@Override
	public void buildSequenceDiagrams() {
		StringBuffer sb=new StringBuffer();
		sb.append("import ramses.reification.*; \n");
		sb.append("import ramses.reification.PrimitiveType.Type;\n");
		sb.append("import ramses.reification.sequencediagram.*;\n");
		sb.append("import ramses.reification.classdiagram.*;\n");
		//sb.append("import ramses.reification.classdiagram.Class; \n");
		sb.append("import ramses.reification.exception.InterfaceDoesNotExistException;\n");
		sb.append("public class SequenceDiagrams{\n");
		sb.append("private static String blank=\"empty.emx\";\n"); //Correct
//		sb.append("private static String blank=\"DemoActivity_output.emx\";\n");
		
		sb.append("private static Reification r;\n");
		sb.append("private static SequenceDiagram sd;\n");
		sb.append("private static SequenceMessage sdm;\n");
		sb.append("private static LifeLine sdl;\n");
		sb.append("//To add field.\n");
		sb.append("public static void main(String[] args) throws Exception{\n");
		sb.append("r=new Reification(blank);\n");
		sb.append(sequenceDiagrams.build());
		sb.append("r.save(\"SequenceDiagrams.emx\");\n");
		sb.append("}\n");
		sb.append("//To add get element method??.\n");
		sb.append("}");
		build=sb.toString();		
	}
	
	//@Override
	public void buildSequenceDiagrams(String classDiagramFile) {
		StringBuffer sb=new StringBuffer();
		sb.append("import ramses.reification.*; \n");
		sb.append("import ramses.reification.PrimitiveType.Type;\n");
		sb.append("import ramses.reification.sequencediagram.*;\n");
		sb.append("import ramses.reification.classdiagram.*;\n");
		sb.append("import ramses.reification.classdiagram.Class; \n");
		//sb.append("import ramses.reification.classdiagram.Class; \n");
		sb.append("import ramses.reification.exception.InterfaceDoesNotExistException;\n");
		sb.append("import java.util.Stack;\n");
		sb.append("public class SequenceDiagrams{\n");
		sb.append("private static String blank=\"empty.emx\";\n"); //Correct
//		sb.append("private static String blank=\"DemoActivity_output.emx\";\n");
		
		sb.append("private static Reification r;\n");
		//
		sb.append("private static ClassDiagram c;\n");
		sb.append("private static Class cl;\n");
		sb.append("private static Interface iface;\n");
		sb.append("private static UMLType[] types;\n");
		sb.append("private static UMLType ret;\n");
		sb.append("private static String[] names;\n");
		sb.append("private static UMLType[] exceptions;\n");	
		//
		sb.append("private static SequenceDiagram sd;\n");
		sb.append("private static SequenceMessage sdm;\n");
		sb.append("private static LifeLine sdl;\n");
		
		sb.append("private static Stack<SequenceMessage> messageStack = new Stack<SequenceMessage>();\n");

		sb.append("public static void main(String[] args) throws Exception{\n");
		
		sb.append("r=new Reification(blank);\n");
		//retrieve class diagram
		try {
			BufferedReader br = new BufferedReader(new FileReader(classDiagramFile));
			try {
			//StringBuilder sb = new StringBuilder();
				String line = br.readLine();
				while (!line.equals("r=new Reification(blank);")) {
					line = br.readLine();
				}
				while (!line.startsWith("r.save")) {
					sb.append(line+"\n");
					line = br.readLine();
				}
			} finally {
				br.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
        //
		sb.append(sequenceDiagrams.build());
		sb.append("r.save(\"SequenceDiagrams.emx\");\n");
		sb.append("}\n");
		
		sb.append("public static Class getClass(String name,String modifiers,boolean abs,ClassDiagram cd){\n");
		sb.append("Class ret;\n " +
				  "return (ret=cd.getClass(name))!=null?ret:cd.createNewClass(name, modifiers, abs);\n");
		sb.append("}\n");
		
		sb.append("public static Interface getInterface(String name,String modifiers,ClassDiagram cd){\n");
		sb.append("Interface ret;\n " +
				  "try {return cd.getInterface(name);} catch (InterfaceDoesNotExistException e) {return cd.createNewInterface(name, modifiers, false);}\n");
		sb.append("}\n");
		
		sb.append("public static Method getMethod(String name,Class clazz,UMLType[] par){\n");
		sb.append("	Method m = null;\nint c;\n" +
//				"	System.out.println(name);\n"+
				"	for (Method me : clazz.getAllMethods()) {\n" +
				"		if (me.getAllInParameters().length == par.length && me.getName().equals(name)) {\n" + 
				"			for (c = 0; c < par.length; c++) {\n" +
//				"				System.out.println(par[c].getName() + \" \"+ me.getAllInParameters()[c].getType().getName());\n" +
				"				if (!me.getAllInParameters()[c].getType().equals(par[c]))\n" +
				"				break;\n" +
				"			}\n" +
//				"			System.out.println(\"c = \"+c);\n"+
				"			if (c == par.length) {\n" +
				"				m = me;\n" +
								"break;\n" +
				"			}\n" +
				"		}\n" +
				"	}\n" +
//				"	System.out.println(name +\"->\"+ m.getName() + m.getAllInParameters().length);\n" +
				"	return m;\n" +
				"}\n");
		
		sb.append("}");
		build=sb.toString();		
	}

	@Override
	public void buildObjectDiagrams(String classDiagramFile) {
		StringBuffer sb=new StringBuffer();
		sb.append("import ramses.reification.*; \n");
		sb.append("import ramses.reification.PrimitiveType.Type;\n");
		sb.append("import ramses.reification.objectdiagram.*;\n");
		sb.append("import ramses.reification.classdiagram.*;\n");
		sb.append("import ramses.reification.classdiagram.Class; \n");
		sb.append("import ramses.reification.exception.InterfaceDoesNotExistException;\n");
		sb.append("import java.util.ArrayList;\n");
		sb.append("import java.util.Arrays;\n");
		sb.append("public class ObjectDiagrams{\n");
		sb.append("private static String blank=\"empty.emx\";\n"); 		
		sb.append("private static Reification r;\n");
		sb.append("private static ClassDiagram c;\n");
		sb.append("private static Class cl;\n");
		sb.append("private static Interface iface;\n");
		sb.append("private static UMLType[] types;\n");
		sb.append("private static UMLType ret;\n");
		sb.append("private static String[] names;\n");
		sb.append("private static UMLType[] exceptions;\n");
		sb.append("private static ObjectDiagram od;\n");
		sb.append("private static Instance odIn;\n");
		sb.append("private static Attribute odAttr;\n");
		sb.append("private static ArrayList<String> odSlotString;\n");
		sb.append("private static Instance firstInstance;\n");
		sb.append("private static Instance secondInstance;\n");
		sb.append("private static String srcClazz;\n");
		sb.append("private static String destClazz;\n");

		sb.append("public static void main(String[] args) throws Exception{\n");
		
		//retrieve class diagram
		try {
			BufferedReader br = new BufferedReader(new FileReader(classDiagramFile));
			try {
				String line = br.readLine();
				while (!line.equals("r=new Reification(blank);")) {
					line = br.readLine();
				}
				while (!line.startsWith("r.save")) {
					sb.append(line+"\n");
					line = br.readLine();
				}
			} finally {
				br.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
        //
		sb.append(objectDiagrams.build());
		sb.append("r.save(\"ObjectDiagrams.emx\");\n");
		sb.append("}\n");
		
		sb.append("public static Class getClass(String name,String modifiers,boolean abs,ClassDiagram cd){\n");
		sb.append("Class ret;\n " +
				  "return (ret=cd.getClass(name))!=null?ret:cd.createNewClass(name, modifiers, abs);\n");
		sb.append("}\n");
		sb.append("}");
		build=sb.toString();		
	}
	

}
