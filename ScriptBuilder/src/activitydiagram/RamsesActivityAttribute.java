package activitydiagram;

public class RamsesActivityAttribute extends ActivityAttribute {

	public RamsesActivityAttribute(String name, String clazz, ActivityDiagramBuilder ad) {
		super(name, clazz, ad);
	}

	//@Override
	public String build() {
		StringBuffer sb=new StringBuffer();
		sb.append("ret=getClass(\""+getClazz()+"\", \""+getModifier()+"\","+isStatic()+", c);\n");
		sb.append("ae=ad.addActivityAttribute(\""+getName()+"\", ret, \""+getModifier()+"\");\n");
		return sb.toString();
	}

}
