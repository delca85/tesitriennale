package activitydiagram;

public class RamsesCallBehavior extends RamsesPinnableElement {

	public RamsesCallBehavior(String name, String behavior, ActivityDiagramBuilder ad) {
		super(name, behavior, ad);
	}

	//@Override
	public String build() {
		StringBuffer sb=new StringBuffer();
		//sb.append("CallAction "+name+"=");
		
		sb.append(varName+"="+(getInsideOf()==null?"ad":getInsideOf())+".addCallBehavior(\""+name+"\",r.getActivityDiagram(\""+behavior+"\"));\n");
		//sb.append(flows.build());
		sb.append(pins.build());
		return sb.toString();
	}

}
