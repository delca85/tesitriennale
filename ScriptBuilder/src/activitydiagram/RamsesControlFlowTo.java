package activitydiagram;

import util.UtilBuilder;
import org.apache.commons.lang.StringEscapeUtils;

public class RamsesControlFlowTo extends ControlFlow {
	private String varName;
	private ActivityDiagramBuilder adb;
	
	public RamsesControlFlowTo(String to, String name, String language, String guard, String weight,ActivityDiagramBuilder adb) {
		super(to, name, language, guard, weight);
		this.adb=adb;
	}

	//@Override
	public String build() {
		try {
			return ".addControlFlowTo("+UtilBuilder.getElementString(adb.getElement(target), adb.getElements())+",\""+name+"\",\""+language+"\",\""+StringEscapeUtils.escapeJava(guard)+"\",\""+weight+"\");\n";
		}
		catch (RuntimeException e) {
			//in caso non trovi l'elemento. 
			//Aggiunto per poter dichiarare i cf in una @Decision in modo da inserire la guard (~la condizione) 
			//(NON sono sicuro che percorrerò entrambi i cf ma li devo esplicitare entrambi) 
			return "=ase;\n/*"+e.getMessage()+"*/\n";
		}
	
	}

}
