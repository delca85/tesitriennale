package activitydiagram;

public class RamsesOutputPin extends Pin {
	private String varName;
	public RamsesOutputPin(String name, String type, int upper, int lower,String varName) {
		super(name, type, upper, lower);
		this.varName=varName;
	}

	//@Override
	public String build() {
		return varName+".addOutputPin(\""+name+"\",null,"+upper+","+lower+");\n"; //null in umltype could raise exceptions
	}

}
