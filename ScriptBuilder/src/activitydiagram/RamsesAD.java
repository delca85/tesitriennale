package activitydiagram;

import java.util.Iterator;

import util.BuilderArrayList;
import util.UtilBuilder;

import builder.Builder;

public class RamsesAD extends ActivityDiagramBuilder {

	public RamsesAD(String name) {
		super(name);
	}

	//@Override
	public String build() {
		StringBuffer sb = new StringBuffer();
		ControllableElement ce;
		String inside;
		Iterator<ControllableElement> i = elements.values().iterator();
		Iterator<ControlFlow> cfs;
		sb.append("ad=r.addActivityDiagram(\""+name+"\");\n"); //Correct
		//sb.append("ad=r.getAllActivityDiagram()[0];\n");
		sb.append(attributes.build());
		sb.append(elements.build());
		sb.append(buildFlows());
		/*
		while (i.hasNext()) {
			ce = i.next();
			
			inside = ce.getInsideOf();
			BuilderArrayList<ControlFlow> flows = ce.getFlows();
			
			if (!flows.isEmpty()) {
				sb.append("ase=");
				sb.append(UtilBuilder.getElementString(ce, elements) + ";\n");
				// sb.append("ase="+inside==null?"getElement(\""+ce.getName()+"\",ad)":"getElement(\""+ce.getName()+"\",getElement(\""+inside+"\",ad))");
				cfs=flows.iterator();
				while(cfs.hasNext())
					sb.append("ase"+cfs.next().build());
			}
		}
		*/
		return sb.toString();
	}

	//@Override
	public PinnableElement addAcceptEvent(String name) {
		RamsesAcceptEvent rae = new RamsesAcceptEvent(name, this);
		this.addActivity(rae);
		return rae;
	}

	//@Override
	public PinnableElement addCallAction(String name, String language, String body) {
		RamsesCallAction rca = new RamsesCallAction(name, language, body, this);
		this.addActivity(rca);
		return rca;
	}
	
	//@Override
	public PinnableElement addCallBehavior(String name, String behavior) {
		RamsesCallBehavior rcb = new RamsesCallBehavior(name, behavior, this);
		this.addActivity(rcb);
		return rcb;
	}

	//@Override
	public CompositeElement addPartition(String name) {
		RamsesPartitionActivity rpa = new RamsesPartitionActivity(name, this);
		this.addActivity(rpa);
		return rpa;
	}

	//@Override
	public CompositeElement addConditional(String name) {
		RamsesConditionalActivity ret = new RamsesConditionalActivity(name,
				this);
		this.addActivity(ret);
		return ret;
	}

	//@Override
	public ControllableElement addDecision(String name) {
		RamsesDecisionNode ret = new RamsesDecisionNode(name, this);
		this.addActivity(ret);
		return ret;
	}

	//@Override
	public ControllableElement addFinal(String name) {
		RamsesFinalNode ret = new RamsesFinalNode(name, this);
		this.addActivity(ret);
		return ret;
	}

	//@Override
	public ControllableElement addInit(String name) {
		RamsesInitialNode ret = new RamsesInitialNode(name, this);
		this.addActivity(ret);
		return ret;
	}

	//@Override
	public ControllableElement addFork(String name) {
		RamsesForkNode ret = new RamsesForkNode(name, this);
		this.addActivity(ret);
		return ret;
	}

	//@Override
	public ControllableElement addJoin(String name) {
		RamsesJoinNode ret = new RamsesJoinNode(name, this);
		this.addActivity(ret);
		return ret;
	}

	//@Override
	public ControllableElement addMerge(String name) {
		RamsesMergeNode ret = new RamsesMergeNode(name, this);
		this.addActivity(ret);
		return ret;
	}

	//@Override
	public CompositeElement addLoop(String name) {
		RamsesLoopActivity ret = new RamsesLoopActivity(name, this);
		this.addActivity(ret);
		return ret;
	}

	//@Override
	public PinnableElement addSendSignal(String name) {
		RamsesSendSignalAction ret = new RamsesSendSignalAction(name, this);
		this.addActivity(ret);
		return ret;
	}

	//@Override
	public CompositeElement addStructured(String name) {
		RamsesStructuredActivity ret = new RamsesStructuredActivity(name, this);
		this.addActivity(ret);
		return ret;
	}
	
	public ActivityAttribute addAttribute(String name, String clazz) {
            RamsesActivityAttribute ret = new RamsesActivityAttribute(name, clazz, this);
            this.addAttribute(ret);
            return ret;
	}
	
	
	//@Override
	public String buildFlows() {
		return elements.buildFlows();
	}

}
