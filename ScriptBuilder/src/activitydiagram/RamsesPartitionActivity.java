package activitydiagram;

public class RamsesPartitionActivity extends RamsesCompositeElement {

	public RamsesPartitionActivity(String name,ActivityDiagramBuilder ad) {
		super(name,ad);
		// TODO Auto-generated constructor stub
	}

	//@Override
	public String build() {
		StringBuffer sb=new StringBuffer();
//		sb.append("ActivityPartition "+name+"=");
		sb.append(super.build());
		sb.append(varName+"="+(getInsideOf()==null?"ad":getInsideOf())+".addActivityPartition(\""+name+"\");\n");
		sb.append(activities.build());
//		sb.append(flows.build());
		sb.append(pins.build());
		return sb.toString();
	}

}
