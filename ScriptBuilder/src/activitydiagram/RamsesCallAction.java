package activitydiagram;

public class RamsesCallAction extends RamsesPinnableElement {

	public RamsesCallAction(String name, String language, String body, ActivityDiagramBuilder ad) {
		super(name, language, body, ad);
	}

	//@Override
	public String build() {
		StringBuffer sb=new StringBuffer();
		//sb.append("CallAction "+name+"=");
		
		sb.append(varName+"="+(getInsideOf()==null?"ad":getInsideOf())+".addCallAction(\""+name+"\",\""+language+"\",\""+body+"\");\n");
		//sb.append(flows.build());
		sb.append(pins.build());
		return sb.toString();
	}

}
