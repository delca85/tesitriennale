package activitydiagram;

public class RamsesJoinNode extends RamsesControllableElement {

	public RamsesJoinNode(String name,ActivityDiagramBuilder ad) {
		super(name,ad);
	}

	//@Override
	public String build() {
		StringBuffer sb=new StringBuffer();
//		sb.append("JoinActivityNode "+name+"=");
		sb.append(varName+"="+(getInsideOf()==null?"ad":getInsideOf())+".addJoinActivityNode(\""+name+"\");\n");
//		sb.append(flows.build());
		return sb.toString();
	}

}
