package activitydiagram;

public class RamsesForkNode extends RamsesControllableElement {

	public RamsesForkNode(String name,ActivityDiagramBuilder ad) {
		super(name,ad);
	}

	//@Override
	public String build() {
		StringBuffer sb=new StringBuffer();
//		sb.append("ForkActivityNode "+name+"=");
		sb.append(varName+"="+(getInsideOf()==null?"ad":getInsideOf())+".addForkActivityNode(\""+name+"\");\n");
//		sb.append(flows.build());
		return sb.toString();
	}

}
