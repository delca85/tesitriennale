package activitydiagram;

public class RamsesAcceptEvent extends RamsesPinnableElement {

	public RamsesAcceptEvent(String name,ActivityDiagramBuilder ad) {
		super(name,ad);
	}
	
	//@Override
	public String build() {
		StringBuffer sb=new StringBuffer();
//		sb.append("AcceptEventAction "+name+"=");
		sb.append(varName+"="+(getInsideOf()==null?"ad":getInsideOf())+".addAcceptEventAction(\""+name+"\");\n");
//		sb.append(flows.build());
		sb.append(pins.build());
		return sb.toString();
	}

}
