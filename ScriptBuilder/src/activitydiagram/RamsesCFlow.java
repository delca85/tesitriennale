package activitydiagram;

public class RamsesCFlow extends ControlFlow {
	private String varName;
	
	public RamsesCFlow(String to, String name, String guard, String weight,String varName) {
		super(to, name, guard, weight);
		this.varName=varName;
	}

	//@Override
	public String build() {
		return varName+".addControlFlowTo(\""+target+"\",\""+name+"\",\""+guard+"\",\""+weight+"\");\n";
	}

}
