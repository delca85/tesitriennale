package activitydiagram;

public class RamsesStructuredActivity extends RamsesCompositeElement {

	public RamsesStructuredActivity(String name,ActivityDiagramBuilder ad) {
		super(name,ad);
	}

	//@Override
	public String build() {
		StringBuffer sb=new StringBuffer();
//		sb.append("StructuredActivityNode "+name+"=");
		sb.append(super.build());
		sb.append(varName+"="+(getInsideOf()==null?"ad":getInsideOf())+".addStructuredActivityNode(\""+name+"\");\n");
//		sb.append(flows.build());
		sb.append(activities.build());
		sb.append(pins.build());
		return sb.toString();
	}

}
