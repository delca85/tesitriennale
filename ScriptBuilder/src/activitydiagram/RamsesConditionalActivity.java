package activitydiagram;

public class RamsesConditionalActivity extends RamsesCompositeElement {

	public RamsesConditionalActivity(String name,ActivityDiagramBuilder ad) {
		super(name,ad);
	}

	//@Override
	public String build() {
		StringBuffer sb=new StringBuffer();
		//sb.append("ConditionalActivityNode "+name+"=");
		sb.append(super.build());
		sb.append(varName+"="+(getInsideOf()==null?"ad":getInsideOf())+".addConditionalActivityNode(\""+name+"\");\n");
		sb.append(activities.build());
//		sb.append(flows.build());
		sb.append(pins.build());
		return sb.toString();
	}

}
