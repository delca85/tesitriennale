package activitydiagram;

public class RamsesFinalNode extends RamsesControllableElement {

	public RamsesFinalNode(String name,ActivityDiagramBuilder ad) {
		super(name,ad);
		// TODO Auto-generated constructor stub
	}

	//@Override
	public String build() {
		StringBuffer sb=new StringBuffer();
//		sb.append("FinalActivityNode "+name+"=");
		sb.append(varName+"="+(getInsideOf()==null?"ad":getInsideOf())+".addFinalActivityNode(\""+name+"\");\n");
//		sb.append(flows.build());
		return sb.toString();
	}

}
