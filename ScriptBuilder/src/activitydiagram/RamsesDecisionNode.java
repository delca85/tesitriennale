package activitydiagram;

public class RamsesDecisionNode extends RamsesControllableElement {

	public RamsesDecisionNode(String name,ActivityDiagramBuilder ad) {
		super(name,ad);
		// TODO Auto-generated constructor stub
	}

	//@Override
	public String build() {
		StringBuffer sb=new StringBuffer();
//		sb.append("DecisionActivityNode "+name+"=");
		sb.append(varName+"="+(getInsideOf()==null?"ad":getInsideOf())+".addDecisionActivityNode(\""+name+"\");\n");
//		sb.append(flows.build());
		return sb.toString();
	}

}
