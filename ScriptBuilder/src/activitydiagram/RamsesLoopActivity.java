package activitydiagram;

public class RamsesLoopActivity extends RamsesCompositeElement {

	public RamsesLoopActivity(String name,ActivityDiagramBuilder ad) {
		super(name,ad);
	}

	//@Override
	public String build() {
		StringBuffer sb=new StringBuffer();
//		sb.append("LoopActivityNode "+name+"=");
		sb.append(super.build());
		sb.append(varName+"="+(getInsideOf()==null?"ad":getInsideOf())+".addLoopActivityNode(\""+name+"\");\n");
		sb.append(activities.build());
//		sb.append(flows.build());
		sb.append(pins.build());
		return sb.toString();
	}

}
