package activitydiagram;

public class RamsesSendSignalAction extends RamsesPinnableElement {

	public RamsesSendSignalAction(String name,ActivityDiagramBuilder ad) {
		super(name,ad);
		// TODO Auto-generated constructor stub
	}

	//@Override
	public String build() {
		StringBuffer sb=new StringBuffer();
//		sb.append("SendSignalAction "+name+"=");
		sb.append(varName+"="+(getInsideOf()==null?"ad":getInsideOf())+".addSendSignalAction(\""+name+"\");\n");
//		sb.append(flows.build());
		sb.append(pins.build());
		return sb.toString();
	}

}
