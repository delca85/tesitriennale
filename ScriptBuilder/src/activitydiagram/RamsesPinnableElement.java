package activitydiagram;

import java.util.Iterator;

import util.BuilderArrayList;
import util.UtilBuilder;

public abstract class RamsesPinnableElement extends PinnableElement {

//	protected String varName;

	public RamsesPinnableElement(String name, ActivityDiagramBuilder ad) {
		super(name, ad,"ae");
//		varName="ae";
	}
	public RamsesPinnableElement(String name, String language, String body, ActivityDiagramBuilder ad) {
		super(name, language, body, ad,"ae");
//		varName="ae";
	}
	public RamsesPinnableElement(String name, String behavior, ActivityDiagramBuilder ad) {
		super(name, behavior, ad,"ae");
//		varName="ae";
	}

	@Override
	public void addInputPin(String name, String type, int upper, int lower) {
		pins.add(new RamsesInputPin(name, type, upper, lower,varName));
	}

	@Override
	public void addOutputPin(String name, String type, int upper, int lower) {
		pins.add(new RamsesOutputPin(name, type, upper, lower,varName));
	}

	@Override
	public ControlFlow addCFlowTo(String to, String name, String language, String guard, String weight) {
		ControlFlow cf;
		if((cf=getControlFlow(to))==null){
			cf=new RamsesControlFlowTo(to,name,language,guard,weight,ad);
			addControlFlow(cf);
		}
		return cf;
	}

	@Override
	public ControlFlow addCFlowFrom(String from, String name, String guard, String weight) {
		//TODO
		flows.add(new RamsesControlFlowFrom(from,name,guard,weight,varName));
		return null;	
	}
	public String buildFlows(){
		Iterator<ControlFlow> cfs;
		BuilderArrayList<ControlFlow> flows = getFlows();
		StringBuffer sb=new StringBuffer();
		if (!flows.isEmpty()) {
			sb.append("ase=");
			sb.append(UtilBuilder.getElementString(this, ad.getElements()) + ";\n");
			// sb.append("ase="+inside==null?"getElement(\""+ce.getName()+"\",ad)":"getElement(\""+ce.getName()+"\",getElement(\""+inside+"\",ad))");
			cfs=flows.iterator();
			while(cfs.hasNext())
				sb.append("ase"+cfs.next().build());
		}
		return sb.toString();
		
	}
	public String build(){
		return "ActivityElement "+varName+"; \n";
	}

}
