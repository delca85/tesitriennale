package activitydiagram;

import java.util.Iterator;

import util.BuilderArrayList;
import util.UtilBuilder;

public abstract class RamsesCompositeElement extends CompositeElement {
//	protected String varName;
	public RamsesCompositeElement(String name,ActivityDiagramBuilder ad) {
		super(name,ad,"gan"+UtilBuilder.getNextId());
//		varName="gan";
	}
	@Override
	public void addActivity(ControllableElement eb){
		eb.setInsideOf(this);
		System.out.println("Setting insideOf ("+eb.getName()+") to "+varName);
		super.addActivity(eb);
	}
	@Override
	public void addInputPin(String name, String type, int upper, int lower) {
		pins.add(new RamsesInputPin(name, type, upper, lower,varName));
	}

	@Override
	public void addOutputPin(String name, String type, int upper, int lower) {
		pins.add(new RamsesOutputPin(name, type, upper, lower,varName));
	}

	@Override
	public ControlFlow addCFlowTo(String to, String name, String language, String guard, String weight) {
		ControlFlow cf;
		if((cf=getControlFlow(to))==null){
			cf=new RamsesControlFlowTo(to,name,language,guard,weight,ad);
			addControlFlow(cf);
		}
		return null;
	}

	@Override
	public ControlFlow addCFlowFrom(String from, String name, String guard,
			String weight) {
		//TODO
		flows.add(new RamsesControlFlowFrom(from,name,guard,weight,varName));
		return null;
	}
	
	//@Override
	public PinnableElement addAcceptEvent(String name) {
		RamsesAcceptEvent rae = new RamsesAcceptEvent(name, ad);
		this.addActivity(rae);
		return rae;
	}

	//@Override
	public PinnableElement addCallAction(String name, String language, String body) {
		RamsesCallAction rca = new RamsesCallAction(name, language, body, ad);
		this.addActivity(rca);
		return rca;
	}

	//@Override
	public CompositeElement addPartition(String name) {
		RamsesPartitionActivity rpa = new RamsesPartitionActivity(name, ad);
		this.addActivity(rpa);
		return rpa;
	}

	//@Override
	public CompositeElement addConditional(String name) {
		RamsesConditionalActivity ret = new RamsesConditionalActivity(name,
				ad);
		this.addActivity(ret);
		return ret;
	}

	//@Override
	public ControllableElement addDecision(String name) {
		RamsesDecisionNode ret = new RamsesDecisionNode(name, ad);
		this.addActivity(ret);
		return ret;
	}

	//@Override
	public ControllableElement addFinal(String name) {
		RamsesFinalNode ret = new RamsesFinalNode(name, ad);
		this.addActivity(ret);
		return ret;
	}

	//@Override
	public ControllableElement addInit(String name) {
		RamsesInitialNode ret = new RamsesInitialNode(name, ad);
		this.addActivity(ret);
		return ret;
	}

	//@Override
	public ControllableElement addFork(String name) {
		RamsesForkNode ret = new RamsesForkNode(name, ad);
		this.addActivity(ret);
		return ret;
	}

	//@Override
	public ControllableElement addJoin(String name) {
		RamsesJoinNode ret = new RamsesJoinNode(name, ad);
		this.addActivity(ret);
		return ret;
	}

	//@Override
	public ControllableElement addMerge(String name) {
		RamsesMergeNode ret = new RamsesMergeNode(name, ad);
		this.addActivity(ret);
		return ret;
	}

	//@Override
	public CompositeElement addLoop(String name) {
		RamsesLoopActivity ret = new RamsesLoopActivity(name, ad);
		this.addActivity(ret);
		return ret;
	}

	//@Override
	public PinnableElement addSendSignal(String name) {
		RamsesSendSignalAction ret = new RamsesSendSignalAction(name, ad);
		this.addActivity(ret);
		return ret;
	}

	//@Override
	public CompositeElement addStructured(String name) {
		RamsesStructuredActivity ret = new RamsesStructuredActivity(name, ad);
		this.addActivity(ret);
		return ret;
	}
	public String buildFlows(){
		Iterator<ControlFlow> cfs;
		BuilderArrayList<ControlFlow> flows = getFlows();
		StringBuffer sb=new StringBuffer();
		if (!flows.isEmpty()) {
			sb.append("ase=");
			sb.append(UtilBuilder.getElementString(this, ad.getElements()) + ";\n");
			// sb.append("ase="+inside==null?"getElement(\""+ce.getName()+"\",ad)":"getElement(\""+ce.getName()+"\",getElement(\""+inside+"\",ad))");
			cfs=flows.iterator();
			while(cfs.hasNext())
				sb.append("ase"+cfs.next().build());
		}
		sb.append(activities.buildFlows());
		return sb.toString();
		
	}
	public String build(){
		return "GroupActivityNode "+varName+";\n";
	}
	public String toString(){
		return varName;
	}
}
