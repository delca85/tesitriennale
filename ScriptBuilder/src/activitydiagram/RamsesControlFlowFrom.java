package activitydiagram;

public class RamsesControlFlowFrom extends ControlFlow {
	private String varName;
	public RamsesControlFlowFrom(String from, String name, String guard,
			String weight,String varName) {
		super(from, name, guard, weight);
		this.varName=varName;
	}

	//@Override
	public String build() {
		return ".addControlFlowFrom(getElement(\""+target+"\",ad),\""+name+"\",\""+guard+"\",\""+weight+"\");\n";
	}

}
