package activitydiagram;

public class RamsesInputPin extends Pin {
	private String varName;
	
	public RamsesInputPin(String name, String type, int upper, int lower,String varName) {
		super(name, type, upper, lower);
		this.varName=varName;
	}

	//@Override
	public String build() {
		
		return varName+".addInputPin(\""+name+"\",null,"+upper+","+lower+");\n"; //null in umltype could raise exceptions
	}

}
