package activitydiagram;

import java.util.Iterator;

import util.BuilderArrayList;
import util.UtilBuilder;

public abstract class RamsesControllableElement extends ControllableElement {
//	protected String varName;
	public RamsesControllableElement(String name,ActivityDiagramBuilder ad) {
		super(name,ad,"ase");
//		varName="ase";
	}

	@Override
	public ControlFlow addCFlowTo(String to, String name, String language, String guard, String weight) {
		ControlFlow cf;
		if((cf=getControlFlow(to))==null){
			cf=new RamsesControlFlowTo(to,name,language,guard,weight,ad);
			addControlFlow(cf);
		}
		return cf;
			
	}

	@Override
	public ControlFlow addCFlowFrom(String from, String name, String guard,	String weight) {
	    	//TODO it has to be looking like the "to" version
		addControlFlow(new RamsesControlFlowFrom(from,name,guard,weight,varName));
		return null;
	}
	public String buildFlows(){
		
		Iterator<ControlFlow> cfs;
		BuilderArrayList<ControlFlow> flows = getFlows();
		StringBuffer sb=new StringBuffer();
		sb.append("//start buildflow for element "+this.getName()+"\n");
		if (!flows.isEmpty()) {
			sb.append("/*buildflow1*/ase=");
			sb.append(UtilBuilder.getElementString(this, ad.getElements()) + ";\n");
			// sb.append("ase="+inside==null?"getElement(\""+ce.getName()+"\",ad)":"getElement(\""+ce.getName()+"\",getElement(\""+inside+"\",ad))");
			cfs=flows.iterator();
			while(cfs.hasNext())
				sb.append("/*buildflow2*/ase"+cfs.next().build());
		}
		sb.append("//end buildflow for element "+this.getName()+"\n");
		return sb.toString();
		
	}
	public String build(){
		return "ActivitySimpleElement "+varName+"; \n";
	}
}
