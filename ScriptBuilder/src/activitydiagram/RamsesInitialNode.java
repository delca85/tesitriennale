package activitydiagram;

public class RamsesInitialNode extends RamsesControllableElement {

	public RamsesInitialNode(String name,ActivityDiagramBuilder ad) {
		super(name,ad);
	}

	//@Override
	public String build() {
		StringBuffer sb=new StringBuffer();
//		sb.append("InitialActivityNode "+name+"=");
		sb.append(varName+"="+(getInsideOf()==null?"ad":getInsideOf())+".addInitialActivityNode(\""+name+"\");\n");
//		sb.append(flows.build());
		return sb.toString();
	}

}
