import ramses.reification.*; 
import ramses.reification.PrimitiveType.Type;
import ramses.reification.activitydiagram.*;
import ramses.reification.exception.InterfaceDoesNotExistException;
public class ActivityDiagrams{
private static String blank="DemoActivity_output.emx";
private static Reification r;
private static ActivityDiagram ad;
private static ActivitySimpleElement ase;
private static ActivityElement ae;
private static GroupActivityNode gan;
private static UMLType pin;
public static void main(String[] args) throws Exception{
r=new Reification(blank);
ad=r.addActivityDiagram("ad1");
ase=ad.addForkActivityNode("fork");
ae=ad.addCallAction("ca1");
ae=ad.addSendSignalAction("send");
ase=ad.addMergeActivityNode("merge");
ase=ad.addInitialActivityNode("init");
ase=ad.addFinalActivityNode("fin");
ase=ad.addJoinActivityNode("join");
GroupActivityNode gan0;
gan0=ad.addConditionalActivityNode("cond");
ase=ad.addDecisionActivityNode("dec");
GroupActivityNode gan4;
gan4=ad.addStructuredActivityNode("struct");
ae=ad.addAcceptEventAction("acceptEvento2");
GroupActivityNode gan1;
gan1=ad.addLoopActivityNode("loop");
ae=gan1.addAcceptEventAction("lae");
ae=gan1.addCallAction("caLoop");
GroupActivityNode gan2;
gan2=gan.addLoopActivityNode("iLoop");
ae=gan2.addCallAction("caiLoop");
ae=ad.addAcceptEventAction("acceptEvento1");
GroupActivityNode gan3;
gan3=ad.addActivityPartition("parition");
ase=getElement("acceptEvento1",ad);
ase.addControlFlowTo(getElement("acceptEvento2",ad),"wewe","guardia"," ");
r.save("ActivityDiagrams.emx");
}
public static ActivitySimpleElement getElement(String name,ActivityDiagram ad) {ActivitySimpleElement ret=null;if((ret=ad.getAcceptEventAction(name))!=null) return ret;if((ret=ad.getCallAction(name))!=null) return ret;if((ret=ad.getActivityPartition(name))!=null) return ret;if((ret=ad.getConditionalActivityNode(name))!=null) return ret;if((ret=ad.getDecisionActivityNode(name))!=null) return ret;if((ret=ad.getFinalActivityNode(name))!=null) return ret;if((ret=ad.getForkActivityNode(name))!=null) return ret;if((ret=ad.getInitialActivityNode(name))!=null) return ret;if((ret=ad.getJoinActivityNode(name))!=null) return ret;if((ret=ad.getLoopActivityNode(name))!=null) return ret;if((ret=ad.getMergeActivityNode(name))!=null) return ret;if((ret=ad.addSendSignalAction(name))!=null) return ret;if((ret=ad.getStructuredActivityNode(name))!=null) return ret;return ret;}public static ActivitySimpleElement getElement(String name,GroupActivityNode ad) {ActivitySimpleElement ret=null;if((ret=ad.getAcceptEventAction(name))!=null) return ret;if((ret=ad.getCallAction(name))!=null) return ret;if((ret=ad.getConditionalActivityNode(name))!=null) return ret;if((ret=ad.getDecisionActivityNode(name))!=null) return ret;if((ret=ad.getFinalActivityNode(name))!=null) return ret;if((ret=ad.getForkActivityNode(name))!=null) return ret;if((ret=ad.getInitialActivityNode(name))!=null) return ret;if((ret=ad.getJoinActivityNode(name))!=null) return ret;if((ret=ad.getLoopActivityNode(name))!=null) return ret;if((ret=ad.getMergeActivityNode(name))!=null) return ret;if((ret=ad.addSendSignalAction(name))!=null) return ret;if((ret=ad.getStructuredActivityNode(name))!=null) return ret;return ret;}}