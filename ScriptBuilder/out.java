import ramses.reification.*; 
import ramses.reification.exception.*;
public class ClassDiagrams{
private static String blank="Ramses_blank.xmi";
private static Reification r;
public static void main(String[] args){
r=new Reification(blank);
ClassDiagram c=r.addClassDiagram("cd1");
Class cl=c.createNewClass("Class1","public",false);
ClassDiagram c=r.addClassDiagram("cd2");
r.save("ClassDiagrams.xmi");
}
public static UMLType getClass(String name,ClassDiagram cd){
try{UMLType ret=cd.getClass(name);}catch(ClassNotFoundException e){ret=cd.createNewClass(name);}
return ret;
}
}