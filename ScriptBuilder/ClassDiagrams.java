import ramses.reification.*; 
import ramses.reification.Class; 
import ramses.reification.exception.*;
public class ClassDiagrams{
private static String blank="Ramses_blank.xmi";
private static Reification r;
private static ClassDiagram c;
private static Class cl;
private static UMLType[] types;
private static UMLType ret;
private static String[] names;
public static void main(String[] args) throws Exception{
r=new Reification(blank);
c=r.addClassDiagram("cd1");
cl=c.createNewClass("Class1","public",false);
ret=getClass("void",c);
types=new UMLType[1];
names=new String[1];
names[0]="arg1Type";
types[0]=getClass("arg1Type",c);
cl.addMethod("method1","public",types,names,ret);
c=r.addClassDiagram("cd2");
r.save("ClassDiagrams.xmi");
}
public static UMLType getClass(String name,ClassDiagram cd){
try{return cd.getClass(name);}catch(ClassDoesNotExistException e){return cd.createNewClass(name,"public",false);}
}
}