package test;

import java.io.File;
import java.util.ArrayList;

import classdiagram.CodeToClassDiagram;
import configuration.ConfigurationManager;

public class TestReflect {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ArrayList<String> classes=new ArrayList<String>();
		processDirectory(new File("./test/Reflect/bin/"),"",classes);
		ConfigurationManager cm=new ConfigurationManager("./test/Reflect/conf.xml");
		CodeToClassDiagram ctcd=new CodeToClassDiagram(cm.getCDConfig(), cm.getGeneralConfig());
		ctcd.addClasses(classes.toArray(new String[]{}));
		ctcd.build();
		
	}
	private static Class<?> loadClass(String className) {
		try {
			return Class.forName(className);
		} 
		catch (ClassNotFoundException e) {
			throw new RuntimeException("Unexpected ClassNotFoundException loading class '" + className + "'");
		}
	}
	private static void processDirectory(File directory, String pkgname, ArrayList<String> classes) {
		String[] files = directory.list();
		for (int i = 0; i < files.length; i++) {
			String fileName = files[i];
			String className = null;
			// we are only interested in .class files
			if (fileName.endsWith(".class")) {
				// removes the .class extension
				className = (pkgname.equalsIgnoreCase("")?"":(pkgname + '.')) + fileName.substring(0, fileName.length() - 6);
			}
			
			if (className != null) {
				System.out.println(className);
				classes.add(loadClass(className).getName());
			}
			File subdir = new File(directory, fileName);
			if (subdir.isDirectory()) {
				processDirectory(subdir, pkgname.equalsIgnoreCase("")?fileName:(pkgname+ '.' + fileName), classes);
			}
		}
	}
}
