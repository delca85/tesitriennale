package test;

import configuration.FILTER;
import annotations.classdiagram.*;

@ShowMethods(FILTER.PUBLIC)
public class Test extends Parent{
	private int priv1;
	public int pub1;
	protected int pro1;
	public String str;
	@Show("vista2")private int priv2;
	@Hide public int pub2;

	public Test(int arg1){
		super();
	}
	
	@ArgsNames({"argomentoUno"})
	public void pubMethod(int a1){
		System.out.println("foo");		
	}
	@Show private void privMethod1(int a,char b,long c,byte d){
	    System.out.println("foo");    
	}

	@Show("vista2")
	private int privMethod2(){
	    return -1;
	}
}
