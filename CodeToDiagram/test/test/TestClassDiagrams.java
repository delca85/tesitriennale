package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import configuration.CDConfiguration;
import configuration.ConfigurationManager;
import configuration.GeneralConfiguration;

import classdiagram.CodeToClassDiagram;

public class TestClassDiagrams extends CodeToClassDiagram{
	
	public TestClassDiagrams(CDConfiguration conf,
			GeneralConfiguration generalConf) {
		super(conf, generalConf);
		// TODO Auto-generated constructor stub
	}

	
	
	@BeforeClass
	public static void setUpBeforeClass(){
		CodeToClassDiagram ctcd;
		ConfigurationManager cm;
		cm=new ConfigurationManager("conf.xml");
		ctcd=new CodeToClassDiagram(cm.getCDConfig(),cm.getGeneralConfig());
		
	}
	public void setUp(String file) throws Exception {
		
	}

	@Test
	public void test1() throws Exception {
		setUp("conf.xml");
		String[] classes={"test.A"};
		this.addClasses(classes);
		
	}

	@Test
	public void testBuild() {
		fail("Not yet implemented");
	}

}
