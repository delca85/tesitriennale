package classdiagram;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URLClassLoader;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Method;



import annotations.classdiagram.Association;

import classdiagram.extractor.AnnotationsExtractor;
import classdiagram.extractor.ClassAnnotationsExtractor;
import classdiagram.extractor.FieldAnnotationsExtractor;
import classdiagram.extractor.MethodAnnotationsExtractor;

import util.Util;
import configuration.CDConfiguration;
import configuration.ConfigurationManager;
import configuration.FILTER;
import configuration.GeneralConfiguration;
import configuration.ViewConfiguration;
import factory.DiagramFactory;

public class CodeToClassDiagram {
	private CDConfiguration conf;
	private GeneralConfiguration generalConf;
	private URLClassLoader classLoader;
	private DiagramFactory factory;

	private static Map<String,Class> builtInMap = new HashMap<String,Class>();
	static {
       builtInMap.put("int", Integer.class);
       builtInMap.put("long", Long.class );
       builtInMap.put("double", Double.class );
       builtInMap.put("float", Float.class );
       builtInMap.put("bool", Boolean.class );
       builtInMap.put("char", Character.class );
       builtInMap.put("byte", Byte.class );
       builtInMap.put("void", Void.class );
       builtInMap.put("short", Short.class );
	}

	public CodeToClassDiagram(CDConfiguration conf,
			GeneralConfiguration generalConf) {
		super();
		this.conf = conf;
		this.generalConf = generalConf;
		classLoader = new URLClassLoader(Util.StringsToURLs(generalConf
				.getClassPaths()));
		URLClassLoader modelClassLoader = new URLClassLoader(
				Util.StringsToURLs(generalConf.getModelClassPaths()));
		try {
			factory = ((DiagramFactory) modelClassLoader.loadClass(
					generalConf.getModelFactory()).newInstance());
		} catch (Exception e) {
			throw new RuntimeException("Cannot load correctly Model Factory : "
					+ e.getMessage());
		}
	}

	public void addClasses(String[] toAdd) {
		for (String cl : toAdd)
			addClass(getClass(cl));
	}

	public void build() {
		factory.buildClassDiagrams();
		factory.exportToFile(generalConf.getOutputFolder()+"/ClassDiagrams.java");
	}

	private void addClass(Class<?> cl) {
		ClassAnnotationsExtractor cae = AnnotationsExtractor
				.getTypeExtractor(cl);
		Iterator<String> finalViews = getViewsForClass(cl, conf.getViews(),
				cae.getShow(), cae.getHide()).iterator();
		while (finalViews.hasNext()) {
			addClassInView(cl, finalViews.next(), cae);
		}
	}

	private void addClassInView(Class<?> cl, String view,
			ClassAnnotationsExtractor cae) {
		System.out.println("BUILDING : " + cl.getName());
		ClassBuilder builder;
		ClassDiagramBuilder cd = factory.getClassDiagram(view);
		ViewConfiguration viewConf = conf.getView(view);
		boolean qualifiedNames = viewConf.isShowQualifiedNames();
		boolean showAssoc = cae.isShowAssociations() == null ? viewConf
				.isShowAssociations() : cae.isShowAssociations();
		boolean showExt = cae.isShowExtends() == null ? viewConf
				.isShowExtends() : cae.isShowExtends();
		boolean showImpls = cae.isShowImpls() == null ? viewConf
				.isShowImplements() : cae.isShowAssociations();
		FILTER addFields = cae.getShowFields() == null ? viewConf
				.getShowFields() : cae.getShowFields();
		FILTER addConstructors = cae.getShowConstructors() == null ? viewConf
				.getShowConstructors() : cae.getShowConstructors();
		FILTER addMethods = cae.getShowMethods() == null ? viewConf
				.getShowMethods() : cae.getShowMethods();
		Association assoc = cae.getAssoc();
		try {
			builder = cd.getType(qualifiedNames ? cl.getName() : cl
					.getSimpleName());
		} catch (RuntimeException e) {
			if (cl.isInterface()){
				builder = cd.addInterface(qualifiedNames ? cl.getName() : cl
						.getSimpleName());
			}
			else
				builder = cd.addClass(qualifiedNames ? cl.getName() : cl
						.getSimpleName());
		}

		if (assoc != null && showAssoc)
			builder.addAssociation(assoc.target(), assoc.label(),
					assoc.sourceLowerValue(), assoc.sourceUpperValue(),
					assoc.targetLowerValue(), assoc.targetUpperValue());

		if (showExt) {
			if(!cl.getSuperclass().getSimpleName().equalsIgnoreCase("Object"))
			builder.addExtends(qualifiedNames ? cl.getSuperclass().getName()
					: cl.getSuperclass().getSimpleName());
		}
		if (showImpls) {
			String[] impls = Util.extractNames(cl.getInterfaces(),
					qualifiedNames);
			builder.addImplements(impls);
		}
		addFieldsToClass(cl.getDeclaredFields(), builder, viewConf, addFields);
		addConstructorsToClass(cl.getDeclaredConstructors(), builder, viewConf,
				addConstructors);
		addMethodsToClass(cl.getDeclaredMethods(), builder, viewConf,
				addMethods);
	}

	private void addConstructorsToClass(Constructor<?>[] constructors,
			ClassBuilder builder, ViewConfiguration view, FILTER addConstructors) {
		Member[] toAdd = addConstructors.filter(constructors, view.getName());
		String[] args = new String[] {};
		String[] newNames;
		String[] types = new String[] {};
		String mod;
		int i;
		boolean qualifiedNames = view.isShowQualifiedNames();
		Constructor<?> c;
		
		for (Member m : toAdd) {
			c = (Constructor<?>) m;
			// System.out.println("Constructor : "+c);
			MethodAnnotationsExtractor ae = new MethodAnnotationsExtractor(c);
			if (ae.getShowArgs()) {
				types = Util
						.extractNames(c.getParameterTypes(), qualifiedNames);
				newNames = ae.getArgsNames();
				args = new String[types.length];
				for (i = 0; i < newNames.length; i++)
					args[i] = newNames[i];
				for (; i < args.length; i++)
					args[i] = "arg" + i;
			}
			mod = Modifier.toString(c.getModifiers());
			builder.addConstructor(mod, args, types);
		}
	}

	private void addMethodsToClass(Method[] methods, ClassBuilder builder,
			ViewConfiguration view, FILTER addMethods) {
		Member[] toAdd = addMethods.filter(methods, view.getName());
		String[] args = new String[] {};
		String[] newNames;
		String[] types = new String[] {};
		String[] newExTypes;
		String[] exceptionsTypes = new String[]{};
		String returnType;
		String mod;
		int i;
		boolean qualifiedNames = view.isShowQualifiedNames();
		Method method;
		System.out.println(addMethods);
		for (Member m : toAdd) {
			//System.out.println(m);
			method = (Method) m;
			MethodAnnotationsExtractor ae = new MethodAnnotationsExtractor(
					method);
			if (ae.getShowArgs()) {
				types = Util.extractNames(method.getParameterTypes(),
						qualifiedNames);
				newNames = ae.getArgsNames();
				// System.out.println(newNames[0]);
				args = new String[types.length];
				for (i = 0; i < newNames.length; i++)
					args[i] = newNames[i];

				for (; i < args.length; i++)
					args[i] = "arg" + i;
			}
			if (view.isShowMethodExceptions()){
				newExTypes = Util.extractNames(method.getExceptionTypes(), qualifiedNames);
				exceptionsTypes = new String[newExTypes.length];
				for (i=0; i < newExTypes.length; i++)
					exceptionsTypes[i]=newExTypes[i];
			}
			if(qualifiedNames || method.getReturnType().isArray())
				returnType = method.getReturnType().getName();
			else
				returnType = method.getReturnType().getSimpleName();
			
			// mod = Modifier.toString(method.getModifiers()).split(" ")[0];	 // RAMSES can't handle multiple modifiers		
//			mod = Modifier.toString(method.getModifiers()); //CORRECT
			// System.out.println(args[0]);
	
			builder.addMethod(method.getName(), returnType, method.getModifiers(), args, types, exceptionsTypes);
		}

	}

	private void addFieldsToClass(Field[] fields, ClassBuilder builder,
			ViewConfiguration view, FILTER addFields) {
		Member[] toAdd = addFields.filter(fields, view.getName());
		String type=null, upperValue=null;
		boolean multiplicity=false;
		Field fi;
		FieldAnnotationsExtractor ae;
		for (Member f : toAdd) {
			fi = (Field) f;
			ae = new FieldAnnotationsExtractor(fi);
			if (Collection.class.isAssignableFrom(fi.getType())) {
				ParameterizedType pType = (ParameterizedType) fi.getGenericType();
				type = (pType.getActualTypeArguments()[0]).toString();
				multiplicity = true;
			}
			if (!Collection.class.isAssignableFrom(fi.getType())){
				type = ae.getFieldType();
				if (type == null)
					type = fi.getType().getSimpleName();
				if (type.endsWith("[]")){
					type = builtInMap.get(type.substring(0, type.lastIndexOf("["))).getSimpleName();
					multiplicity = true;
				}
			}
			System.out
					.println(f.getName() + " " + fi.getType().getSimpleName());
			if (type.startsWith("class"))
				type = type.substring(6);
			if (type.startsWith("java."))
				type = type.substring(type.lastIndexOf(".") + 1);
			builder.addField(f.getName(), type, Modifier.toString(f.getModifiers()), multiplicity);
		}
	}

	private Set<String> getViewsForClass(Class<?> cl,
			Collection<ViewConfiguration> views, String[] shows, String[] hides) {
		Iterator<ViewConfiguration> it = conf.getViews().iterator();
		Set<String> finalViews = new HashSet<String>();
		while (it.hasNext()) {
			ViewConfiguration vc = it.next();
			// System.out.println(vc.getName());
			if (classInsideView(cl, vc)) {
				finalViews.add(vc.getName());
			}
			for (String show : shows)
				finalViews.add(show);
			for (String hide : hides)
				finalViews.remove(hide);
		}
		return finalViews;
	}

	private boolean classInsideView(Class<?> cl, ViewConfiguration vc) {
		System.out.println("CHECKING "+cl.getName()+" "+cl.isEnum()+" "+cl.getSuperclass()+" "+(Enum.class.isAssignableFrom(cl)));
		boolean in = true;
		Iterator<String> incl;
		Iterator<String> excl;
		if (cl.isAnnotation() && !vc.isShowAnnotations())
			return false;
		if (Enum.class.isAssignableFrom(cl) && !vc.isShowEnumerations())
			return false;
		if (cl.isAssignableFrom(Exception.class) && !vc.isShowExceptions()) // cambia come enum
			return false;
		incl = vc.getIncludes().iterator();
		excl = vc.getExcludes().iterator();
		while (incl.hasNext()) {
			String prepPack = preparePackage(incl.next());
			try {
				classLoader.loadClass(prepPack + cl.getSimpleName());
				in = true;
				break;
			} catch (ClassNotFoundException e) {
				in = false;
			}
		}
		while (excl.hasNext()) {
			String prepPack = preparePackage(excl.next());
			try {
				classLoader.loadClass(prepPack + cl.getSimpleName());
				return false;
			} catch (ClassNotFoundException e) {
			}
		}
		
		return in;
	}

	private String preparePackage(String pack) {
		StringBuffer sb = new StringBuffer(pack);
		return sb.charAt(sb.length() - 1) == '*' ? sb.deleteCharAt(
				sb.length() - 1).toString() : sb.append('.').toString();
	}

	private Class<?> getClass(String className) {
		Class<?> c;
		try {
			c = classLoader.loadClass(className);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Class Not Found : " + e.getMessage());
		}
		return c;
	}

	public static void main(String[] args) {
		ConfigurationManager cm = new ConfigurationManager("conf.xml");
		CodeToClassDiagram ctcd = new CodeToClassDiagram(cm.getCDConfig(),
				cm.getGeneralConfig());
		ctcd.addClasses(args);
		//ctcd.addClasses(new String[] { "test.Test", "test.Parent" }); //TEST
		ctcd.build();

	}

}
