package classdiagram.extractor;

import java.lang.reflect.AnnotatedElement;

import configuration.FILTER;


import annotations.classdiagram.*;


public class ClassAnnotationsExtractor extends AnnotationsExtractor {
	private Boolean showAssociations;
	protected Boolean showExtends;
	protected Boolean showImpls;
	private FILTER showMethods;
	private FILTER showFields;
	private FILTER showConstructors;
	private Association assoc;

	protected ClassAnnotationsExtractor(AnnotatedElement ae) {
		super(ae);
		extractAnnotations();
	}

	private void extractAnnotations() {
		extractShowAssociations();
		extractShowExtends();
		extractShowImpls();
		extractShowMethods();
		extractShowFields();
		extractShowConstructors();
		extractAssociation();
	}

	private void extractAssociation() {
		if(ae.isAnnotationPresent(Association.class))
			assoc=ae.getAnnotation(Association.class);	
	}

	private void extractShowConstructors() {
		if (ae.isAnnotationPresent(ShowConstructors.class))
			showConstructors = ae.getAnnotation(ShowConstructors.class).value();	
	}
	public Association getAssoc() {
		return assoc;
	}
	public Boolean isShowAssociations() {
		return showAssociations;
	}

	public Boolean isShowExtends() {
		return showExtends;
	}

	public Boolean isShowImpls() {
		return showImpls;
	}

	public FILTER getShowMethods() {
		return showMethods;
	}

	public FILTER getShowFields() {
		return showFields;
	}

	public FILTER getShowConstructors() {
		return showConstructors;
	}

	private void extractShowFields() {
		if (ae.isAnnotationPresent(ShowFields.class))
			showFields = ae.getAnnotation(ShowFields.class).value();		
	}

	private void extractShowMethods() {
		if (ae.isAnnotationPresent(ShowMethods.class))
			showMethods = ae.getAnnotation(ShowMethods.class).value();		
	}

	protected void extractShowImpls() {
		if (ae.isAnnotationPresent(ShowImplements.class))
			showImpls = ae.getAnnotation(ShowImplements.class).value();			
	}

	protected void extractShowExtends() {
		if (ae.isAnnotationPresent(ShowExtends.class))
			showExtends = ae.getAnnotation(ShowExtends.class).value();		
	}

	private void extractShowAssociations() {
		if (ae.isAnnotationPresent(ShowAssociations.class))
			showAssociations = ae.getAnnotation(ShowAssociations.class).value();		
	}

}
