package classdiagram.extractor;

import java.lang.reflect.AnnotatedElement;

import annotations.classdiagram.Hide;
import annotations.classdiagram.Show;

public abstract class AnnotationsExtractor {

	protected AnnotatedElement ae;
	private String[] show;
	private String[] hide;

	protected AnnotationsExtractor(AnnotatedElement ae) {
		super();
		this.ae=ae;
		extractShow();
		extractHide();
	}
	public static ClassAnnotationsExtractor getTypeExtractor(Class<?> cl){
		if(cl.isInterface())
			return new InterfaceAnnotationsExtractor(cl);
		else
			return new ClassAnnotationsExtractor(cl);
	}
	public String[] getShow() {
		return show;
	}

	public String[] getHide() {
		return hide;
	}

	protected void extractHide() {
		if (ae.isAnnotationPresent(Hide.class))
			hide = ae.getAnnotation(Hide.class).value();
		else
			hide = new String[] {};
	}

	protected void extractShow() {
		if (ae.isAnnotationPresent(Show.class))
			show = ae.getAnnotation(Show.class).value();
		else
			show = new String[] {};
	}

}