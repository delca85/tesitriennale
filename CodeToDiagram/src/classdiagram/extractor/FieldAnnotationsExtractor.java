package classdiagram.extractor;

import java.lang.reflect.AnnotatedElement;
import annotations.classdiagram.*;

public class FieldAnnotationsExtractor extends AnnotationsExtractor{
	
	private String fieldType;
	
	public FieldAnnotationsExtractor(AnnotatedElement ae) {
		super(ae);
		extractFieldType();
	}
	private void extractFieldType(){
		if(ae.isAnnotationPresent(FieldType.class))
			fieldType=ae.getAnnotation(FieldType.class).value();
	}
	public String getFieldType(){
		return fieldType;
	}
}
