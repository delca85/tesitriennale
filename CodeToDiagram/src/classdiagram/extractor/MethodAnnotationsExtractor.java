package classdiagram.extractor;

import java.lang.reflect.AnnotatedElement;
import annotations.classdiagram.*;

public class MethodAnnotationsExtractor extends AnnotationsExtractor{
	private Boolean showArgs;
	private String[] argsNames;
	public MethodAnnotationsExtractor(AnnotatedElement ae) {
		super(ae);
		showArgs=true;
		argsNames=new String[]{};
		extractArgsName();
		extractShowArgs();
	}
	private void extractArgsName() {
		if(ae.isAnnotationPresent(ArgsNames.class)){
			String[] names=ae.getAnnotation(ArgsNames.class).value();
			argsNames=names==null?new String[]{}:names;
		}
	}
	private void extractShowArgs() {
		if(ae.isAnnotationPresent(ShowArgs.class)){
			showArgs=ae.getAnnotation(ShowArgs.class).value();
		}
	}
	public Boolean getShowArgs() {
		return showArgs;
	}
	public String[] getArgsNames() {
		return argsNames;
	}

}
