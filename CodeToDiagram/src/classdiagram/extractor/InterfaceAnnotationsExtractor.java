package classdiagram.extractor;

import java.lang.reflect.AnnotatedElement;

import annotations.classdiagram.ShowExtends;

public class InterfaceAnnotationsExtractor extends ClassAnnotationsExtractor {

	public InterfaceAnnotationsExtractor(AnnotatedElement ae) {
		super(ae);
		showExtends=false;
	}
	protected void extractShowExtends() {}
	protected void extractShowImpls(){
		if (ae.isAnnotationPresent(ShowExtends.class))
			showImpls = ae.getAnnotation(ShowExtends.class).value();		
	}

}
