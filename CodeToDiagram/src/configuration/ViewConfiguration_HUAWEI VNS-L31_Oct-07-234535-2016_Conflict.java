package configuration;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.configuration.HierarchicalConfiguration;




public class ViewConfiguration {
	private List<String> includes;
	private List<String> excludes;
	private String name;
	
	private boolean showAnnotations;
	private boolean showEnumerations;
	private boolean showExceptions;
	private boolean showLibraries;
	private boolean showStandardTypes;
	private boolean showQualifiedNames;
	
	private boolean showImplements;
	private boolean showExtends;
	private boolean showAssociations;
	
	private FILTER showFields;
	private FILTER showConstructors;
	private FILTER showMethods;
	private boolean showMethodArgs;
	
	public ViewConfiguration(HierarchicalConfiguration conf){
		this.name=conf.getString("[@name]");
		includes=new ArrayList<String>();
		excludes=new ArrayList<String>();
		Iterator<HierarchicalConfiguration> i=conf.configurationsAt("include").iterator();
		while(i.hasNext())
			includes.add(i.next().getString("package"));
		i=conf.configurationsAt("exclude").iterator();
		while(i.hasNext())
			excludes.add(i.next().getString("package"));
		
		showAnnotations=conf.getBoolean("showAnnotations");
		showEnumerations=conf.getBoolean("showEnumerations");
		showExceptions=conf.getBoolean("showExceptions");
		showLibraries=conf.getBoolean("showLibraries");
		showStandardTypes=conf.getBoolean("showStandardTypes");
		showQualifiedNames=conf.getBoolean("showQualifiedNames");
		showImplements=conf.getBoolean("showImplements");
		showExtends=conf.getBoolean("showExtends");
		showAssociations=conf.getBoolean("showAssociations");
		showFields=FILTER.valueOf(conf.getString("showFields").toUpperCase());
		showConstructors=FILTER.valueOf(conf.getString("showConstructors").toUpperCase());
		showMethods=FILTER.valueOf(conf.getString("showMethods").toUpperCase());
		showMethodArgs=conf.getBoolean("showMethodArgs");
		
	}

	public List<String> getIncludes() {
		return includes;
	}

	public List<String> getExcludes() {
		return excludes;
	}

	public String getName() {
		return name;
	}

	public boolean isShowAnnotations() {
		return showAnnotations;
	}

	public boolean isShowEnumerations() {
		return showEnumerations;
	}

	public boolean isShowExceptions() {
		return showExceptions;
	}

	public boolean isShowLibraries() {
		return showLibraries;
	}

	public boolean isShowStandardTypes() {
		return showStandardTypes;
	}

	public boolean isShowQualifiedNames() {
		return showQualifiedNames;
	}

	public boolean isShowImplements() {
		return showImplements;
	}

	public boolean isShowExtends() {
		return showExtends;
	}

	public boolean isShowAssociations() {
		return showAssociations;
	}

	public FILTER getShowFields() {
		return showFields;
	}

	public FILTER getShowConstructors() {
		return showConstructors;
	}

	public FILTER getShowMethods() {
		return showMethods;
	}

	public boolean isShowMethodArgs() {
		return showMethodArgs;
	}
}
