package configuration;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration.HierarchicalConfiguration;

public class GeneralConfiguration {
	private HierarchicalConfiguration conf;
	
	public GeneralConfiguration(HierarchicalConfiguration conf){
		this.conf=conf;
	}
	public boolean getCreateImages(){
		return conf.getBoolean("createImages");
	}
	public boolean getCreateFiles(){
		return conf.getBoolean("createFiles");
	}
	public String getOutputFolder(){
		return conf.getString("outputFolder");
	}
	public Object[] getClassPaths(){
		List<Object> cps=new ArrayList<Object>();
		HierarchicalConfiguration paths=conf.configurationAt("classpath");
		cps=paths.getList("path");
		if(cps.size()==0)
			throw new RuntimeException("No ClassPath found!");
		return cps.toArray();
	}
	public String getModelFactory(){
		return conf.getString("modelFactory");
	}
	public Object[] getModelClassPaths(){
		List<Object> cps=new ArrayList<Object>();
		HierarchicalConfiguration paths=conf.configurationAt("modelClasspath");
		cps=paths.getList("path");
		if(cps.size()==0)
			throw new RuntimeException("No Model ClassPath found!");
		return cps.toArray();
	}
}
