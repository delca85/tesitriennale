package configuration;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Member;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import classdiagram.extractor.AnnotationsExtractor;

import annotations.classdiagram.Hide;
import annotations.classdiagram.Show;



public enum FILTER implements Filterable{
		NONE{
			public Member[] filter(Member[] members,String view){
				List<Member> ret=new ArrayList<Member>();
				AnnotatedElement ann;
				for(Member m:members){
					ann=(AnnotatedElement)m;
					if(ann.isAnnotationPresent(Show.class)){
						String[] views=ann.getAnnotation(Show.class).value();
						for(String v:views){
							if(v.equalsIgnoreCase("ALL")||v.equalsIgnoreCase(view)){
								ret.add(m);
								break;
							}
						}
					}
					if(ann.isAnnotationPresent(Hide.class)){
						String[] views=ann.getAnnotation(Hide.class).value();
						for(String v:views){
							if(v.equalsIgnoreCase("ALL")||v.equalsIgnoreCase(view)){
								ret.remove(m);
								break;
							}
						}
					}
				}
				return ret.toArray(new Member[]{});
			}	
		}
		,NOTPRIVATE{
			//@Override
			public Member[] filter(Member[] members, String view) {
				HashSet<Member> ret=new HashSet<Member>();
				AnnotatedElement ann;
				for(Member m:members){
					ann=(AnnotatedElement)m;
					if(!Modifier.isPrivate(m.getModifiers()))
						ret.add(m);
					if(ann.isAnnotationPresent(Show.class)){
						String[] views=ann.getAnnotation(Show.class).value();
						for(String v:views){
							if(v.equalsIgnoreCase("ALL")||v.equalsIgnoreCase(view)){
								ret.add(m);
								break;
							}
						}
					}
					if(ann.isAnnotationPresent(Hide.class)){
						String[] views=ann.getAnnotation(Hide.class).value();
						for(String v:views){
							if(v.equalsIgnoreCase("ALL")||v.equalsIgnoreCase(view)){
								ret.remove(m);
								break;
							}
						}
					}
				}
				return ret.toArray(new Member[]{});
			}
		}
		,PUBLIC{
			//@Override
			public Member[] filter(Member[] members, String view) {
				List<Member> ret=new ArrayList<Member>();
				AnnotatedElement ann;
				for(Member m:members){
					ann=(AnnotatedElement)m;
					if(Modifier.isPublic(m.getModifiers()))
						ret.add(m);
					if(ann.isAnnotationPresent(Show.class)){
						String[] views=ann.getAnnotation(Show.class).value();
						for(String v:views){
							if(v.equalsIgnoreCase("ALL")||v.equalsIgnoreCase(view)){
								ret.add(m);
								break;
							}
						}
					}
					if(ann.isAnnotationPresent(Hide.class)){
						String[] views=ann.getAnnotation(Hide.class).value();
						for(String v:views){
							if(v.equalsIgnoreCase("ALL")||v.equalsIgnoreCase(view)){
								ret.remove(m);
								break;
							}
						}
					}
				}
				return ret.toArray(new Member[]{});
			}
		}
		,ALL{
			//@Override
			public Member[] filter(Member[] members, String view) {
				List<Member> ret=new ArrayList<Member>();
				AnnotatedElement ann;
				for(Member m:members){
					ann=(AnnotatedElement)m;
					ret.add(m);
					if(ann.isAnnotationPresent(Hide.class)){
						String[] views=ann.getAnnotation(Hide.class).value();
						for(String v:views){
							if(v.equalsIgnoreCase("ALL")||v.equalsIgnoreCase(view)){
								ret.remove(m);
								break;
							}
						}
					}
				}
				return ret.toArray(new Member[]{});
			}
		};


}
