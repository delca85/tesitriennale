package configuration;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;

public class ConfigurationManager {
	//public enum FILTER{NONE,NOTPRIVATE,PUBLIC,ALL};
	private GeneralConfiguration generalConfig;
	private CDConfiguration CDConfig;
	private ADConfiguration ADConfig;
	private SDConfiguration SDConfig;
	private ODConfiguration ODConfig;

	public ConfigurationManager(String configFile){
		XMLConfiguration conf;
		try {
			conf = new XMLConfiguration(configFile);
			generalConfig=new GeneralConfiguration(conf.configurationAt("general"));
			CDConfig=new CDConfiguration(conf.configurationAt("classDiagram"));
			ADConfig=new ADConfiguration(conf.configurationAt("activityDiagram"));
			SDConfig=new SDConfiguration(conf.configurationAt("sequenceDiagram"));
			ODConfig=new ODConfiguration(conf.configurationAt("objectDiagram"));

		} catch (ConfigurationException e) {
			throw new RuntimeException("Configuration error : "+e.getMessage());
		}
		
	}
	
	public GeneralConfiguration getGeneralConfig() {
		return generalConfig;
	}

	public CDConfiguration getCDConfig() {
		return CDConfig;
	}
	public ADConfiguration getADConfig() {
		return ADConfig;
	}
	public SDConfiguration getSDConfig() {
		return SDConfig;
	}

	public ODConfiguration getODConfig() {
		return ODConfig;
	}
	
	public static void main(String[] args) {
		//System.out.println(ConfigurationManager.CDConfig.getView("Vista1").getShowFields());
		ConfigurationManager cm=new ConfigurationManager("conf.xml");
		System.out.println(cm.generalConfig.getClassPaths()[0]);

	}

}
