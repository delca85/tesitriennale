package configuration;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.HierarchicalConfiguration;

public class CDConfiguration extends DiagramConfiguration {
	
	private Map<String,ViewConfiguration> views;
	
	public CDConfiguration(HierarchicalConfiguration conf) {
		super(conf);
		views=new HashMap<String, ViewConfiguration>();
		Iterator<HierarchicalConfiguration> it=conf.configurationsAt("view").iterator();
		while(it.hasNext())
			addView(it.next());
		
	}

	private void addView(HierarchicalConfiguration view) {
		ViewConfiguration newView=new ViewConfiguration(view);
		views.put(newView.getName(), newView);
	}
	public ViewConfiguration getView(String name){
		return views.get(name);
	}
	public Collection<ViewConfiguration> getViews(){
		return views.values();
	}
}
