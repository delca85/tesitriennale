package configuration;

import java.lang.reflect.Member;

public interface Filterable {
	public Member[] filter(Member[] members,String view);
}
