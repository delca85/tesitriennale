package configuration;

import org.apache.commons.configuration.HierarchicalConfiguration;

public abstract class DiagramConfiguration {
	private HierarchicalConfiguration conf;
	
	public DiagramConfiguration(HierarchicalConfiguration conf){
		this.conf=conf;
	}
	public boolean getGenerate(){
		return conf.getBoolean("generate");
	}
}
