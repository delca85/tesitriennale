package annotations.activitydiagram;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.BLOCK})
public @interface Structured {
        String ad_id() default "";
        String id() ;
        String inside() default "";
        Pin[] pins() default {};
        ControlFlow[] flows() default {};
}

