package annotations.activitydiagram;

public @interface Pin {
	String id(); 
	String name() default ""; 
	String type() default "";
	int upper() default 0;
	int lower() default 0;
	PINTYPE role() default PINTYPE.IN; 
}
