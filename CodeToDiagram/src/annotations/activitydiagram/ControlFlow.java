package annotations.activitydiagram;

import java.lang.annotation.*;
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.BLOCK,ElementType.EXPRESSION})
public @interface ControlFlow {
	String src() default ""; 
	String dest(); 
	String name() default "";
	String guard() default ""; 
	String weight() default ""; 
}
