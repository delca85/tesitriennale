package annotations.activitydiagram;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.BLOCK,ElemenType.EXPRESSION})
public @interface Fork {
        String ad_id() default "";
        String id() ;
        String inside() default "";
        ControlFlow[] flows() default {};
}

