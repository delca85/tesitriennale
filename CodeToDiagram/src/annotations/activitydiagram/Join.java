package annotations.activitydiagram;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.BLOCK, ElementType.EXPRESSION})
public @interface Join {
        String ad_id() default "";
        String id();
        String inside() default "";
        ControlFlow[] flows() default {};
}

