package annotations.activitydiagram;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.BLOCK,ElementType.EXPRESSION})
public @interface CallBehavior {
	String ad_id() default "";
	String id(); 
	String inside() default "";
	String behavior();
	Pin[] pins() default {};
	ControlFlow[] flows() default {};
}