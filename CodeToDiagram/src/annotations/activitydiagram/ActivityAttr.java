package annotations.activitydiagram;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.BLOCK,ElementType.EXPRESSION})
public @interface ActivityAttr {
	String name();
	String clazz();
	String modifier() default "public";
	boolean isStatic() default false;
}