package annotations.activitydiagram;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.BLOCK,ElementType.EXPRESSION})
public @interface CallAction {
	String ad_id() default "";
	String id(); 
	String inside() default "";
	Pin[] pins() default {};
	ControlFlow[] flows() default {};
}
