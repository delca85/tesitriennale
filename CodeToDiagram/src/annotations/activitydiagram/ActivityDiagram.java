package annotations.activitydiagram;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import atjava.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.BLOCK,ElementType.EXPRESSION})
public @interface ActivityDiagram {
	String id();  
	String name() default "";	
	boolean initialNode() default true;	
	boolean finalNode() default true;
	ActivityAttr[] args() default {};
}
