package annotations.classdiagram;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Association {
	String target();
	String label() default "";
	String sourceLowerValue() default "";
	String sourceUpperValue() default "";
	String targetLowerValue() default "";
	String targetUpperValue() default "";
}
