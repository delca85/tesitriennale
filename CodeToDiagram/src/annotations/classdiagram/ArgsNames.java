package annotations.classdiagram;

import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/** Define class diagram methods arguments name */
@Retention(RUNTIME)
@Target({CONSTRUCTOR,METHOD})
public @interface ArgsNames {
	String[] value() default {""};
}
