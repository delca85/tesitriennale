package annotations.classdiagram;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/** Specify if a class diagram element must be hidden */
@Retention(RUNTIME)
@Target({TYPE, CONSTRUCTOR, METHOD,FIELD})
public @interface Hide {
	String[] value() default {""}; // add keyword ALL as default
}
