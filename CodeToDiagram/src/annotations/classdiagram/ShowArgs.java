package annotations.classdiagram;

import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/** Define if class diagram method args are to be shown or not */
@Retention(RUNTIME)
@Target({CONSTRUCTOR,METHOD})
public @interface ShowArgs {
	boolean value();
}
