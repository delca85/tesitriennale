package annotations.classdiagram;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import configuration.FILTER;


/** Define if class diagram constructors are to be shown or not */
@Retention(RUNTIME)
@Target({TYPE})
public @interface ShowConstructors {
	FILTER value();
}
