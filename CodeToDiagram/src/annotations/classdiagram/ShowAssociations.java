package annotations.classdiagram;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/** Define if class diagram associations are to be shown or not */
@Retention(RUNTIME)
@Target({TYPE})
public @interface ShowAssociations {
	boolean value();
}
