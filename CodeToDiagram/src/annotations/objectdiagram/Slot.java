package annotations.objectdiagram;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD) 

public @interface Slot{}
