package annotations.objectdiagram;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.BLOCK})
public @interface ObjectDiagram {
	String id(); // identificatore diagramma
	String name() default ""; // nome diagramma
}
