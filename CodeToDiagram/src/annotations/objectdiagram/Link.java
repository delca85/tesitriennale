package annotations.objectdiagram;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.BLOCK, ElementType.EXPRESSION}) 
public @interface Link {
	String od_id() default ""; // id diagramma 
	boolean remove() default false; //true implica rimozione del link
	String label() default ""; //etichetta che comparirà sul link
	String source() default ""; // nome istanza sorgente
	String destination() default ""; // nome istanza destinazione
}