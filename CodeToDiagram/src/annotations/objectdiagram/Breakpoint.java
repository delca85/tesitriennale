package annotations.objectdiagram;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.BLOCK}) 
public @interface Breakpoint{}

