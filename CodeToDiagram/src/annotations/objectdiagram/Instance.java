package annotations.objectdiagram;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.BLOCK, ElementType.EXPRESSION}) 
public @interface Instance {
	String od_id() default ""; // id diagramma 
	String name() default ""; // nome istanza
	boolean single_instance() default false; //se true crea un'unica istanza indipendentemente da quante volte verrà eseguita l'annotazione
} 