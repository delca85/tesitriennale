package annotations.sequencediagram;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.BLOCK, ElementType.EXPRESSION})
public @interface Lifeline {
	String sd_id() default ""; // identificatore diagramma
	String name() default ""; // nome lifeline
}
