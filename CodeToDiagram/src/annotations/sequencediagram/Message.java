package annotations.sequencediagram;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.BLOCK, ElementType.EXPRESSION})
public @interface Message {
	String sd_id() default ""; // id diagramma
	String name() default ""; // nome messaggio
	String sender() default ""; // identificatore sorgente
	String receiver() default ""; // identificatore destinazione
	boolean asynchronous() default false; // indica se il messaggio e' sincrono
}
