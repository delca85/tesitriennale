package annotations.sequencediagram;

import java.lang.annotation.*;

public @interface NewLifeline {
	String type(); // tipo
	String name() default ""; // nome
}
