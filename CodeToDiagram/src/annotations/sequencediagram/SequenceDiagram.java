package annotations.sequencediagram;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import atjava.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.BLOCK})
public @interface SequenceDiagram {
	String id(); // identificatore diagramma
	String name() default ""; // nome diagramma
	NewLifeline[] lifelines() default {}; //lifeline da creare insieme al diagramma 
}
