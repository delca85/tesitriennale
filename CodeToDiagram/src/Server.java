import annotations.sequencediagram.*;
import annotations.classdiagram.*;
import configuration.FILTER;
import java.util.ArrayList;

public class Server {

	String name;
	ArrayList<String> clientAuth = new ArrayList<String>();
	
	public Server(String name) {
		this.name = name;
	}
	public String requestAuthentication(String name, String pwd) {
		System.out.println("requesting autentication..");
		AuthenticationManager am = new AuthenticationManager();
		String id = "cookie"+pwd+name;
		if (am.checkCredentials(name,pwd)) {
                    
                    // @Message(name="Add Client") {
                        this.addAuthenticatedClient(id);
                    // }
                }
                return id;
	}
	
	public String requestResource(String id, String res) {
                if (clientAuth.contains(id))
                    return "RES: "+res;
                else
                    return "Client not authenticated";
	}
	
	public void addAuthenticatedClient(String id) {
            clientAuth.add(id);
	}
}
