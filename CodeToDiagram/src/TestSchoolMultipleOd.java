import annotations.objectdiagram.*;
import java.util.ArrayList;

public class TestSchoolMultipleOd {
	public static void main(String[] args) {
		@ObjectDiagram(id="TestSchool1", name="TestSchool simple") {
			School s = @Instance(name="School") {new School()};
			Teacher t = @Instance(single_instance=true){s.addTeacher("Andrea", 32)};
			Teacher t2 = @Instance{s.addTeacher("Paolo", 40)};
			ClassRoom cr = s.addClassRoom("v1");
			ClassRoom cr2 = s.addClassRoom("v3");
			@Breakpoint{}
			s.addCourse(t,cr);
			s.addCourse(t2,cr2);
			t.setName("Luca");
			@Breakpoint{}
			s.rmTeacher(t);
		}
		@ObjectDiagram(id="TestSchool2") {
			School s0 = @Instance(name="School1") {new School()}; 
			Teacher t0 = @Instance{s0.addTeacher("Andrea", 32)};
			Teacher t1 = @Instance(name="Marco"){s0.addTeacher("Marco", 32)};
			ClassRoom cr0 = s0.addClassRoom("v1");
			@Breakpoint{}
			ClassRoom cr1 = s0.addClassRoom("v3");
			Course c0 = @Instance{new Course(t0,cr0)}; 
			Course c1 = @Instance(name="CorsoDiMarco"){new Course(t1,cr1)};
			@Breakpoint{}
			ArrayList<Course> courses0 = new ArrayList<Course>();
			courses0.add(c0);
			courses0.add(c1);
			t0.setName("Paolo");
			s0.setCourses(courses0);
		}
	}
}