import annotations.objectdiagram.*;

public class Course {
	
	@Slot()protected Teacher teacher;
	@Slot()protected ClassRoom classRoom;	
	
	public Course(Teacher t, ClassRoom c) { 
		@Link{ this.teacher = t;}
		System.out.print("");
		@Link{ this.classRoom = c; } 
	}
}