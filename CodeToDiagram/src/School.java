import annotations.objectdiagram.*;
import java.util.ArrayList;

public class School {
	
	@Slot()public ArrayList<Teacher> teachers;
	@Slot()protected ArrayList<ClassRoom> rooms;  
	@Slot()protected ArrayList<Course> courses;

	public School() {
		teachers = new ArrayList<Teacher>();
		rooms = new ArrayList<ClassRoom>();
		courses = new ArrayList<Course>();
	}
	
	public Teacher addTeacher(String name, int age) { 
		Teacher t = new Teacher(name, age);
		@Link(source="School") {teachers.add(t);}
		return t;
	}

	public static void getOne(){
		System.out.println("1");
	}

	public void addTeacher(Teacher t) throws CustomException, RuntimeException{
		teachers.add(t);
	}
	
	public ClassRoom addClassRoom(String name) { 
		ClassRoom cr = @Instance{new ClassRoom(name)};
		@Link(source="School") {rooms.add(cr);}
		return cr;
	}

	public void addCourse(Teacher t, ClassRoom cr) { 
		Course c = @Instance{new Course(t,cr)};
		@Link(source="School") {courses.add(c);} 
	}

	public void setCourses(ArrayList<Course> co) {
		for (Course c : co)
			@Link(label="corsi"){courses.add(c);}
	}

	public void rmTeacher(Teacher t){
		@Link(source="School", remove=true){
			teachers.remove(t);}
	}

	public void getException() throws IllegalArgumentException{
		
	}

}