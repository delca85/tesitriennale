package objectdiagram;

import java.net.URLClassLoader;
import java.lang.reflect.*;
import java.lang.annotation.*;
import util.*;
import java.util.*;
import java.util.regex.*;
import configuration.*;
import factory.DiagramFactory;
import annotations.objectdiagram.*;
import atjava.lang.annotation.*;

public class CodeToObjectDiagram{
	private ODConfiguration conf;
	private GeneralConfiguration generalConf;
	private URLClassLoader classLoader;
	private DiagramFactory factory;
	private String lastOD;				//id dell'ultimo object diagram che ho valutato
	private AnnotatedBlock ab;
	private AnnotatedExpression ae;
	private Object instance;
	private int startSourceLine,endSourceLine;
	private String source;
	private String bodySource;


	public CodeToObjectDiagram(ODConfiguration conf, GeneralConfiguration generalConf){
		this.conf = conf;
		this.generalConf = generalConf;
		classLoader = new URLClassLoader(Util.StringsToURLs(generalConf.getClassPaths()));
		URLClassLoader modelClassLoader = new URLClassLoader(
				Util.StringsToURLs(generalConf.getModelClassPaths()));
		try {
			factory = ((DiagramFactory) modelClassLoader.loadClass(
					generalConf.getModelFactory()).newInstance());
		} catch (Exception e) {
			throw new RuntimeException("Cannot load correctly Model Factory : "
					+ e.getMessage());
		}	
		setContext();
	}

	public void setInstance(Object instance){
		this.instance=instance;
	}

	// public void build(){
	// 	factory.buildObjectDiagrams();
	// 	factory.exportToFile(generalConf.getOutputFolder()+"/ObjectDiagrams.java");
	// }
	
	public void build(String s){                      
		factory.buildObjectDiagrams(generalConf.getOutputFolder()+s);
		factory.exportToFile(generalConf.getOutputFolder()+"/ObjectDiagrams.java");
	}

	private void setContext(){
		try{
			Class<?> c=Class.forName("objectdiagram.ObjectDiagramAspect");
			Method m=c.getMethod("setContext",CodeToObjectDiagram.class);
			m.invoke(null,this);			
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void blockAnnotationEvent(Annotation a, AnnotatedBlock ab, Object[] args){
		this.ab=ab;
		extractStartEnd(ab);
		String annName = a.annotationType().getSimpleName();
		int i=0;
		if (args.length > 1 && this.instance == null) {
			this.instance = args[0];
			i=1;
		}
		if (annName.equalsIgnoreCase("objectdiagram")){
			System.out.println("[CODETOOBJ]ObjectDiagram Annotation");
			objectDiagramEvent(a);
		}	
		if (annName.equals("Link")) {
			System.out.println("[CODETOOBJ]BlockLink Annotation");
			//nel caso in cui this.instance sia null e args abbia più di un elemento, args[0] = source della linkazioone
			for (; i < args.length; i++) {
				if (args[i] instanceof Collection)		//gli elementi di args potrebbero essere a loro volta una collezione
					for (Object o : ((Collection) args[i]))		//in quel caso devo trattare ogni elemento della collezione separatamente
						linkEvent(a, o);		//linkEvent ha come parametri annotazione e istanza (parametro del joinpoint)
				if (! (args[i] instanceof Collection))
					linkEvent(a, args[i]);
			}
		}
		if (annName.equals("Instance")) {
			System.out.println("[CODETOOBJ]BlockInstance Annotation");
			for (; i < args.length; i++) {
				if (args[i] instanceof Collection)
					for (Object o : ((Collection) args[i]))
						instanceEvent(a, o);		//instanceEvent accetta come parametri annotazioe e oggetto di cui creare istanza
				if (! (args[i] instanceof Collection))
						instanceEvent(a, args[i]);
			}
		}
		if (annName.equals("Breakpoint")) {
			System.out.println("[CODETOOBJ]BlockBreakpoint Annotation");
			breakpointEvent(a);			//breakpointEvent non ha parametri oltre all'annotazione
		}
		this.ab=null;
	}

	public void exprAnnotationEvent(Annotation a, AnnotatedExpression ae, Object obj){
		this.ae=ae;
		extractStartEnd(ae);
		String annName = a.annotationType().getSimpleName();
		if (annName.equalsIgnoreCase("objectdiagram")){
			System.out.println("[CODETOOBJ]ObjectDiagram Annotation");
			objectDiagramEvent(a);
		}	
		if (annName.equals("Link")) {
			System.out.println("[CODETOOBJ]ExprLink Annotation");
			linkEvent(a, obj);
		}
		if (annName.equals("Instance")) {
			System.out.println("[CODETOOBJ]ExprInstance Annotation");
			instanceEvent(a, obj);		
		}
		this.ae=null;
	}

	public void objectDiagramEvent(Annotation a){
		System.out.println("[CODETOOBJ]OD EVENT");
		System.out.println("[CODETOOBJ]Annotation: " + a);
		ObjectDiagram toAdd=(ObjectDiagram)a;
		lastOD=toAdd.id();			//richiama campo id annotazione object diagram
		System.out.println("[CODETOOBJ]Adding "+lastOD+" object diagram");
		ObjectDiagramBuilder odb=factory.getObjectDiagram(lastOD);
		System.out.println("[CODETOOBJ]Added OD with ID = " + lastOD);
	}

	public void instanceEvent(Annotation a, Object o) {
		// System.out.println("[CODETOOBJ]This is a instanceEvent : "+a.annotationType().getSimpleName());
		String target = lastOD;
		//cerco informazioni scritte nell'annotazione
		String name = getFromAnnotation(a, "name").toString();
		boolean singleInstance = ((Boolean) getFromAnnotation(a, "single_instance")).booleanValue();
		ObjectDiagramBuilder odb = factory.getObjectDiagram(target);
		addInstance(odb, name, o, singleInstance);	
		if (!odb.containsSlotsClass(o.getClass()))
			slotEvent(odb, o.getClass());	//ad ogni nuova istanza aggiunta, se è la prima della sua classe, aggiungo i campi linkativi nell'hashMap che mi indica quali rappresentare	
	}

	public void addInstance(ObjectDiagramBuilder odb, String name, Object o, boolean singleInstance) {
		try {
			Method m;
			Class<?> clazz=odb.getClass();
			Object obj=odb;
			m=clazz.getMethod("addInstance",new Class[]{Object.class, String.class, boolean.class});
			m.invoke(obj,new Object[]{o, name, singleInstance});			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void slotEvent(ObjectDiagramBuilder odb, Class claz) {
		ArrayList<String> fieldAnnotated = new ArrayList<String>();
		Field[] fields = claz.getDeclaredFields();
		for (Field f : fields) {		//tra tutti i campi della classe, cerco quelli annotati come di interesse per il diagramma
			f.setAccessible(true);
			if (f.getAnnotation(annotations.objectdiagram.Slot.class) != null)
				fieldAnnotated.add(f.getName());
		}
		if (fieldAnnotated.size() != 0) //se c'è almeno un campo di interesse, lo passo all'odb
			try{
				Method m;
				Class<?> clazz=odb.getClass();
				Object obj=odb;
				m=clazz.getMethod("addSlots",new Class[]{Class.class, ArrayList.class});
				m.invoke(obj,new Object[]{claz, fieldAnnotated});	
			} catch(Exception e) {
				e.printStackTrace();
			}
	}

	public void linkEvent(Annotation a, Object o){
		// System.out.println("[CODETOOBJ]This is a linkEvent : "+a.annotationType().getSimpleName());
		// System.out.println("[CODETOOBJ]linkEvent, this.instance: " + this.instance + " arg: "+o);
		String target = lastOD;
		//cerco informazioni scritte nell'annotazione
		String label = getFromAnnotation(a, "label").toString();
		String source = getFromAnnotation(a, "source").toString();
		String destination = getFromAnnotation(a, "destination").toString();
		boolean remove = ((Boolean) getFromAnnotation(a, "remove")).booleanValue();
		ObjectDiagramBuilder odb=factory.getObjectDiagram(target);
		//odb imposta le stringhe corrette per source e destination
		if (!remove)
			addLink(odb, label, source, destination, o);			
		if (remove)
			rmLink(odb, label, source, destination, o);
	}

	public void addLink(ObjectDiagramBuilder odb, String label, String source, String destination, Object o) {
		try {
			Method m;
			Class<?> clazz=odb.getClass();
			Object obj=odb;
			m=clazz.getMethod("addLink",new Class[]{String.class, String.class, String.class, Object.class, Object.class});
			m.invoke(obj,new Object[]{label, source, destination, this.instance, o});			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void rmLink(ObjectDiagramBuilder odb, String label, String source, String destination, Object o) {
		try {
			Method m;
			Class<?> clazz=odb.getClass();
			Object obj=odb;
			m=clazz.getMethod("rmLink",new Class[]{String.class, String.class, String.class, Object.class, Object.class});
			m.invoke(obj,new Object[]{label, source, destination, this.instance, o});			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void breakpointEvent(Annotation a) {
		String target = lastOD;
		ObjectDiagramBuilder odb = factory.getObjectDiagram(target);
		breakpoint(odb);				
	}

	public void breakpoint(ObjectDiagramBuilder odb) {
		try {
			Method m;
			Class<?> clazz=odb.getClass();
			Object obj=odb;
			m=clazz.getMethod("breakpoint", new Class[]{});
			m.invoke(obj, new Object[]{});			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void endEvent() {
		String target = lastOD;
		ObjectDiagramBuilder odb = factory.getObjectDiagram(target);
		try {
			Method m;
			Class<?> clazz=odb.getClass();
			Object obj=odb;
			m=clazz.getMethod("endEvent", new Class[]{});
			m.invoke(obj, new Object[]{});			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void resetStartEnd(){
		startSourceLine=-1;
		endSourceLine=-1;
	}
	public void extractStartEnd(AnnotatedExpression ae){
		startSourceLine=ae.startSourceLine();
		endSourceLine=ae.endSourceLine();
		try{
			source=ae.getSourceCode();
			bodySource=ae.getBodySourceCode();
		}catch(Exception e){
			source=e.getMessage();
			bodySource=e.getMessage();
		}
	}

	public void extractStartEnd(AnnotatedBlock ab){
		startSourceLine=ab.startSourceLine();
		endSourceLine=ab.endSourceLine();
		try{
			source=ab.getSourceCode();
			bodySource=ab.getBodySourceCode();
		}catch(Exception e){
			source=e.getMessage();
			bodySource=e.getMessage();
		}		
	}

	public Object getFromAnnotation(Annotation a, String prop){
		Object ret = null;
		try{
			Class<?> clazz=a.getClass();
			Method m=clazz.getMethod(prop,new Class[]{});
			ret= m.invoke(a,new Object[]{});
		}catch(Exception e){
			System.err.println(prop+" does not exists!");
			e.printStackTrace();
		}
		return ret;
	}

	public static void main(String[] args){
		ConfigurationManager cm = new ConfigurationManager("conf.xml");
		CodeToObjectDiagram ctod=new CodeToObjectDiagram(cm.getODConfig(),cm.getGeneralConfig());
		if(args.length>0){
			ctod.invokeMain(args[0], Util.copyOfRange(args,1,args.length));
		}
		ctod.build("ClassDiagrams.java");
	}

	public void invokeMain(String mainClass, String[] args){
		try{
			Class<?> c=getClass(mainClass);
			Method main=c.getMethod("main",String[].class);
			System.out.println("[OBJECTDIAGRAMASPECT]" + mainClass+" "+args[0]+" "+args.getClass()+" "+main.getParameterTypes()[0]);
			main.invoke(null,new Object[]{args});
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	private Class<?> getClass(String className) {
        Class<?> c;
        try {
            c = classLoader.loadClass(className);
        }catch (ClassNotFoundException e) {
            throw new RuntimeException("Class Not Found : " + e.getMessage());
        }
        return c;
	}

	private String getClassName(Object o) {
		return o.getClass().getName();
	}


}