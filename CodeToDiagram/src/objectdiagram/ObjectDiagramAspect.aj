package objectdiagram;

import java.lang.reflect.*;
import java.lang.annotation.*;
import annotations.objectdiagram.*;
import util.*;
import org.aspectj.lang.*;
import ataspectj.reflect.*;
import atjava.lang.annotation.*;

public aspect ObjectDiagramAspect {
	private static CodeToObjectDiagram LISTENER;
	
	public pointcut od(): @block(ObjectDiagram);
	public pointcut link(): @block(Link);	//chiamata di metodo
	public pointcut instance(): @block(Instance);	//chiamata di metodo di una factory
	public pointcut breakpoint(): @block(Breakpoint);

	public pointcut linkE(): @expr(Link);	//assegnamento
	public pointcut instanceE(): @expr(Instance);	//assegnamento dopo azione di un costruttore

	public static void setContext(CodeToObjectDiagram listener){
		LISTENER=listener;
		System.out.println("[OBJECTDIAGRAMASPECT]Listener setted");
	}

	private Object[] extractAnnotationAndBlock(JoinPoint jp){
		BlockJoinPointStaticPart bjp=(BlockJoinPointStaticPart) JoinPointFactory.getJoinPointStaticPart(jp);		
		Object[] args = jp.getArgs();
		String annName;
		AnnotatedBlock ab;
		Annotation ann;
		Method getAB,getAnnName;
		try{
			getAB=BlockJoinPointStaticPart.class.getDeclaredMethod("getAnnotatedBlock",new Class[]{});
			getAB.setAccessible(true);
			getAnnName=BlockJoinPointStaticPart.class.getDeclaredMethod("getAnnotationName",new Class[]{});
			getAnnName.setAccessible(true);
			annName=(String)getAnnName.invoke(bjp,new Object[]{});
			annName=annName.substring(annName.lastIndexOf('.')+1);	
			ab=(AnnotatedBlock)getAB.invoke(bjp,new Object[]{});
			ann=ab.getAnnotation();
			return new Object[]{ann,ab,annName, args};
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	private Object[] extractAnnotationAndExpr(JoinPoint jp){
		ExprJoinPointStaticPart ejp=(ExprJoinPointStaticPart) JoinPointFactory.getJoinPointStaticPart(jp);
		Object[] args = jp.getArgs();
		String annName;
        AnnotatedExpression ae;
        Annotation ann;
        Method getAE,getAnnName;
        try{
	        getAE=ExprJoinPointStaticPart.class.getDeclaredMethod("getAnnotatedExpression",new Class[]{});
	        getAE.setAccessible(true);
	        getAnnName=ExprJoinPointStaticPart.class.getDeclaredMethod("getAnnotationName",new Class[]{});
	        getAnnName.setAccessible(true);
	        annName=(String)getAnnName.invoke(ejp,new Object[]{});
	        annName=annName.substring(annName.lastIndexOf('.')+1);
	        ae=(AnnotatedExpression)getAE.invoke(ejp,new Object[]{});
	        ann=ae.getAnnotation();
	        int i = 0;
			for (Object o : args)
				System.out.println("[OBJECTDIAGRAMASPECT]Expr args[" + (i++) + "]: "+ o.toString());	
	        return new Object[]{ann, ae, annName, args[1]};
        }catch(Exception e){
                e.printStackTrace();
        }
        return null;
	}

	void around(): od() {
		Object[] extract=extractAnnotationAndBlock(thisJoinPoint);
		Annotation ann;
		AnnotatedBlock ab;
		String annName;
		if(extract!=null){
			ann=(Annotation)extract[0];
			ab=(AnnotatedBlock)extract[1];
			annName=(String)extract[2];
			LISTENER.setInstance(thisJoinPoint.getThis());
			LISTENER.objectDiagramEvent(ann);
			System.out.println("[OBJECTDIAGRAMASPECT]Around: Begin of object diagram");
			System.out.println("[OBJECTDIAGRAMASPECT]Around: Annotation: "+ ann);
			proceed();
			LISTENER.endEvent();
			System.out.println("[OBJECTDIAGRAMASPECT]Around: End of object diagram");			
		}
	}

	after(): link() || instance() || breakpoint() {
		Object[] extract=extractAnnotationAndBlock(thisJoinPoint);
		Annotation ann;
		AnnotatedBlock ab;
		String annName;
		Object[] args;
		if(extract!=null){
			ann=(Annotation)extract[0];
			ab=(AnnotatedBlock)extract[1];
			annName=(String)extract[2];
			args = (Object[]) extract[3];
			System.out.println("[OBJECTDIAGRAMASPECT]Block: " + annName.toUpperCase());
			System.out.println("[OBJECTDIAGRAMASPECT]Annotation: "+ ann);
			int i = 0;
			for (Object o : args)
				System.out.println("[OBJECTDIAGRAMASPECT]args[" + (i++) + "]: "+ o.toString());
			LISTENER.setInstance(thisJoinPoint.getThis());
			LISTENER.blockAnnotationEvent(ann, ab, args);			//ctod riceve annotazione, blocco annotato, array di oggetti (istanze Java) che sono parametri del jp
		}	
	}

	after(): instanceE() || linkE() {
		Object[] extract=extractAnnotationAndExpr(thisJoinPoint);
		String annName;
        AnnotatedExpression ae;
        Annotation ann;
        Object arg;
        if(extract!=null){
			ann=(Annotation)extract[0];
			ae=(AnnotatedExpression)extract[1];
			annName = (String) extract[2];
			arg = extract[3];
			System.out.println("[OBJECTDIAGRAMASPECT]Expr: " + annName.toUpperCase());
			System.out.println("[OBJECTDIAGRAMASPECT]Annotation: "+ ann);
			// System.out.println("[OBJECTDIAGRAMASPECT]Annotated expression: " + ae);
			LISTENER.setInstance(thisJoinPoint.getThis());
			LISTENER.exprAnnotationEvent(ann, ae, arg); 	//ctod riceve annotazione, expr annotata, istanza creata
		}	
	}

}