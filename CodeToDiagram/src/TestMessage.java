import annotations.sequencediagram.*;

public class TestMessage {
	
	public void a(String s) {
		System.out.println("Metodo a " + s);
	}

	public void b() {
		System.out.println("Metodo b");
	}

	public void c() {
		System.out.println("Metodo c");
	}
	
	public static void main(String[] argv) {
		@SequenceDiagram(id="TestSD", name="Test", lifelines={}) {
			TestMessage tm = new TestMessage();
			String s = "ciao";
			@Message {
				tm.a(s);
			}
			@Message {
				tm.b();
			}
			@Message {
				tm.c();
			}
		}
	}
}
