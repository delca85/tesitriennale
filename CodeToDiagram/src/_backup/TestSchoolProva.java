import annotations.objectdiagram.*;

public class TestSchoolProva {
	public static void main(String[] args) {
		@ObjectDiagram(id="TestSchool") {
			School s = @Instance(name="School_0") {new School()};
			Teacher t = @Instance(name="Science Teacher"){s.addTeacher("Andrea", 32)};
			Teacher t1 = @Instance{s.addTeacher("Alberto", 49)};
			@Breakpoint{}
			s.rmTeacher(t);
		}
	}
}