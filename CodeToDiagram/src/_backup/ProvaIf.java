import annotations.activitydiagram.*;

import java.util.*;

public class ProvaIf{

	//public ArrayList<String> lista = new ArrayList<String>();
	
	public void a(boolean t,boolean t2){
	  @ActivityDiagram(id="1", args={@ActivityAttr(name="t", clazz="boolean"),@ActivityAttr(name="t2", clazz="boolean")}){
	    System.out.println("as");
	    @Decision(ad_id="1",id="if",flows={@ControlFlow(src="if", dest="iif", guard="t", name="true"),@ControlFlow(src="if", dest="false", guard="!(t)", name="false")}){
	    	if(t){
			@Decision(ad_id="1",id="iif",flows={@ControlFlow(src="iif", dest="itrue", guard="t2", name="true"),@ControlFlow(src="iif", dest="ifalse", guard="!(t2)", name="false")}){
				if(t2){
					@CallAction(ad_id="1",id="itrue"){
					System.out.println("itrue");
				    }
				}else{
				    @CallAction(ad_id="1",id="ifalse"){
					System.out.println("ifalse");
				    }
				    //@CallBehavior(ad_id="1",id="callbehav1", behavior="2"){
				    //@CallAction(ad_id="1",id="callbehav"){
				    /*@CallBehavior(ad_id="1",id="callbehav1", behavior="2"){
					//System.out.println("this is a call behavior: object.method(args)");
					b("callbehav");
				    }*/
			}

			}
			@CallAction(ad_id="1",id="ipost"){

			}
		}else if (!t){
			@CallAction(ad_id="1",id="false"){
			    System.out.println("false");
			}
		}	
	    }
		@CallAction(ad_id="1",id="post"){
			System.out.println("post");
			//lista.add("exe");
		}

	  }
	}
/*	
	public void b(String a) {
		@ActivityDiagram(id="2", name="testbehavior_1"){
		System.out.println("Enter method b");
		@Loop(ad_id="2",id="loopFor"){
		for (int i = 0; i < lista.size(); i++) {
		@CallAction(ad_id="2",id="stampa"){
		System.out.println("Parametro : " +a);
		}
		}
		}
		@CallAction(ad_id="2",id="endB") {
			System.out.println("End b()");
		}
		}
	}
*/
	public static void main(String[] args){
		ProvaIf p=new ProvaIf();
		p.a(true,true);
		p.a(true,false);
		p.a(false,false);
		p.a(false,true);
	}

}
