import annotations.objectdiagram.*;

public class Teacher {
	
	@Slot()protected String name;
	@Slot()protected int age;
	
	public Teacher(String name, int age) {
		this.name = name;
		this.age = age;
	}

	public void setName(String name){
		this.name = name;
	}
}