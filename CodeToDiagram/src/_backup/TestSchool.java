import annotations.objectdiagram.*;

public class TestSchool {
	public static void main(String[] args) {
		@ObjectDiagram(id="TestSchool1", name="TestSchool simple") {
			School s = @Instance(name="School") {new School()};
			Teacher t = @Instance{s.addTeacher("Andrea", 32)};
			Teacher t2 = @Instance{s.addTeacher("Paolo", 40)};
			ClassRoom cr = s.addClassRoom("v1");
			ClassRoom cr2 = s.addClassRoom("v3");
			@Breakpoint{}
			s.addCourse(t,cr);
			s.addCourse(t2,cr2);
			t.setName("Aco");
			@Breakpoint{}
			s.rmTeacher(t);
		}
	}
}