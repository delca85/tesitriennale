import annotations.activitydiagram.*;

public class Test3AD{
        public void a(){
                @ActivityDiagram(id="test",initialNode=true,finalNode=true){
                        b(true);
			b(false);
                }
        }
        public void b(boolean flag){
                @CallAction(id="callaction"){
                        System.out.println("test");
                }
		@Loop(id="loop"){
			for(int i=0;i<1;i++){
				@SendSignal(id="send"){
					System.out.println("Segnale!");
				}
				
				@Decision(id="if"){
				if(flag){
					@CallAction(id="condTrue"){
						
					}
				}else{
					@CallAction(id="condFalse"){
						
					}
				}
				}
				@AcceptEvent(id="ae"){
					System.out.println("accept!");
				}
			}
		}
        }
        public static void main(String[] args){
                new Test3AD().a();
        }

}

