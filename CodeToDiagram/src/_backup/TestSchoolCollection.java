import annotations.objectdiagram.*;
import java.util.ArrayList;

public class TestSchoolCollection {
	public static void main(String[] args) {
		@ObjectDiagram(id="TestSchool1", name="TestSchool Collection") {
			School s = @Instance(name="School") {new School()}; 
			Teacher t = @Instance(name="Science Teacher", single_instance=true){s.addTeacher("Andrea", 32)};
			Teacher t1 = @Instance{new Teacher("Alberto", 49)};
			ClassRoom cr = s.addClassRoom("v1");
			@Breakpoint{}
			ClassRoom cr2 = s.addClassRoom("v3");
			Course c =  @Instance(name="Math Course"){new Course(t,cr)}; 
			Course c2 = @Instance(name="Physics Course"){new Course(t,cr2)};
			@Breakpoint{}
			ArrayList<Course> courses = new ArrayList<Course>();
			courses.add(c);
			courses.add(c2);
			t.setName("Marco");
			s.rmTeacher(t);
			s.setCourses(courses);
		}
	}
}