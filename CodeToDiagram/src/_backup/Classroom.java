import annotations.objectdiagram.*;

public class ClassRoom {
	
	@Slot()protected String name;
	
	public ClassRoom(String name) {
		this.name= name;
	}
}