package activitydiagram;

import java.net.URLClassLoader;
import java.lang.reflect.*;
import java.lang.annotation.*;
import util.*;
import java.util.*;
import configuration.*;
import factory.DiagramFactory;
import annotations.activitydiagram.*;
import atjava.lang.annotation.*;
import org.apache.commons.lang.StringEscapeUtils;

public class CodeToActivityDiagram{
	private ADConfiguration conf;
	private GeneralConfiguration generalConf;
	private URLClassLoader classLoader;
	private DiagramFactory factory;
	private ControllableElement lastElement;
	private String lastAD;
	private List<String> addFinals;
	private HashMap<String,Tuple<ControllableElement,Annotation>> lastElements;
	private Set<String> duplicates;
	private Set<String> secondRound;
	private AnnotatedBlock ab;
	private AnnotatedExpression ae;
	private Object instance;
	private HashMap<String,ControlFlow> controlFlowsToLabel;
	private Stack<Tuple<ControllableElement,Tuple<Integer,Integer>>> ifs;
	private Map<String,Stack<String>> insides;
	private int cfToBeSaved;
	private int startSourceLine,endSourceLine;
	private String source;
	private String bodySource;

	public CodeToActivityDiagram(ADConfiguration conf,GeneralConfiguration generalConf){
		this.conf = conf;
		this.generalConf = generalConf;
		classLoader = new URLClassLoader(Util.StringsToURLs(generalConf
				.getClassPaths()));
		URLClassLoader modelClassLoader = new URLClassLoader(
				Util.StringsToURLs(generalConf.getModelClassPaths()));
		try {
			factory = ((DiagramFactory) modelClassLoader.loadClass(
					generalConf.getModelFactory()).newInstance());
		} catch (Exception e) {
			throw new RuntimeException("Cannot load correctly Model Factory : "
					+ e.getMessage());
		}
		lastElements=new HashMap<String,Tuple<ControllableElement,Annotation>>();
		addFinals=new ArrayList<String>();
		duplicates=new HashSet<String>();
		secondRound=new HashSet<String>();
		ifs=new Stack<Tuple<ControllableElement,Tuple<Integer,Integer>>>();
		insides=new HashMap<String,Stack<String>>();
		controlFlowsToLabel=new HashMap<String,ControlFlow>();
		setContext();
	}
	public void addCfToBeSaved(){
		cfToBeSaved++;
	}
	public void setInstance(Object instance){
		this.instance=instance;
	}
	private void setContext(){
		try{
			Class<?> c=Class.forName("activitydiagram.ActivityDiagramAspect");
			Method m=c.getMethod("setContext",CodeToActivityDiagram.class);
			m.invoke(null,this);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public void finalizeMerge(){
		ControllableElement merge;
		String adb;
		if(!ifs.isEmpty()){
			merge=ifs.peek().one;
			if(merge.getInsideOf()==null){
			   	adb=merge.getADName(); 	
				lastElements.put(adb+adb,Tuple.<ControllableElement,Annotation>create(merge,null));
			}
		}
	}
	public void finalize(){
		ActivityDiagramBuilder adb;
		ControllableElement ce;
		Tuple<ControllableElement,Annotation> t;
		annotations.activitydiagram.ControlFlow[] flows;
		Iterator<String> i=addFinals.iterator();
		while(i.hasNext()){
			adb=factory.getActivityDiagram(i.next());
			adb.addFinal("final");
			if(lastElements.containsKey(adb.getName()+adb.getName())){
			    	t=lastElements.get(adb.getName()+adb.getName());
				ce=t.one();
				flows=(annotations.activitydiagram.ControlFlow[]) getFromAnnotation(t.two(),"flows");
				if(flows==null || flows.length==0){
					ce.getFlows().clear();
				}
				ce.addCFlowTo("final","","","","");
			}
		}
	}
	public void build(){
		factory.buildActivityDiagrams();
		factory.exportToFile(generalConf.getOutputFolder()+"/ActivityDiagrams.java");
	}
	public void build(String s){
		factory.buildActivityDiagrams(generalConf.getOutputFolder()+s);
		factory.exportToFile(generalConf.getOutputFolder()+"/ActivityDiagrams.java");
	}
	public void activitydiagramEvent(Annotation a){
		System.out.println("AD EVENT");
		System.out.println(a);
		ActivityDiagram toAdd=(ActivityDiagram)a;
		lastAD=toAdd.id();
		System.out.println("Adding "+lastAD+" activity diagram");
		ActivityDiagramBuilder adb=factory.getActivityDiagram(lastAD);
		if(toAdd.initialNode()){
			lastElements.put(lastAD+lastAD,Tuple.<ControllableElement,Annotation>create(adb.addInit("in"),null));
			System.out.println("ADD INIT");
		}
		if(toAdd.finalNode()){
			addFinals.add(lastAD);
		}
		annotations.activitydiagram.ActivityAttr[] args =(annotations.activitydiagram.ActivityAttr[])getFromAnnotation(a,"args");
		for (annotations.activitydiagram.ActivityAttr arg : args)
			newAttribute(arg);
	}
	public void newAttribute(Annotation a) {
            String target = lastAD;
            String name = getFromAnnotation(a,"name").toString();
            String clazz = getFromAnnotation(a,"clazz").toString();
            ActivityDiagramBuilder adb = factory.getActivityDiagram(target);
            try {
                Method m;
                Class<?> cl=adb.getClass();
                m=cl.getMethod("addAttribute",new Class[]{String.class, String.class});
		m.invoke(adb, new Object[]{name, clazz});
            } catch(Exception e) {
		e.printStackTrace();
            }
	}
	public void blockAnnotationEvent(Annotation a,AnnotatedBlock ab){
		this.ab=ab;
		extractStartEnd(ab);
		annotationEvent(a);
		this.ab=null;
	}
	public void exprAnnotationEvent(Annotation a,AnnotatedExpression ae){
		this.ae=ae;
		extractStartEnd(ae);
		annotationEvent(a);
		this.ae=null;
	}
	public void resetStartEnd(){
		startSourceLine=-1;
		endSourceLine=-1;
	}
	public void extractStartEnd(AnnotatedBlock ab){
		startSourceLine=ab.startSourceLine();
		endSourceLine=ab.endSourceLine();
		try{
			source=ab.getSourceCode();
			bodySource=ab.getBodySourceCode();
		}catch(Exception e){
			source=e.getMessage();
			bodySource=e.getMessage();
		}
	}
	public void extractStartEnd(AnnotatedExpression ae){
		startSourceLine=ae.startSourceLine();
		endSourceLine=ae.endSourceLine();
		try{
			source=ae.getSourceCode();
			bodySource=ae.getBodySourceCode();
		}catch(Exception e){
			source=e.getMessage();
			bodySource=e.getMessage();
		}
	}
	public void pushInside(Annotation a){
		String target=getFromAnnotation(a,"ad_id").toString();
		String id=getFromAnnotation(a,"id").toString();
		target=target.equals("")?lastAD:target;
		
		Stack<String> stack;
		if(target!=null){
			if(insides.containsKey(target)){
				stack=insides.get(target);
			}else{
				stack=new Stack<String>();
				insides.put(target,stack);
			}
			stack.push(id);
			System.out.println("PUSHED ("+target+")"+insides.get(target).peek());
		}
	}
	public void popInside(Annotation a){
		String target=getFromAnnotation(a,"ad_id").toString();
		Stack<String> stack;
		target=target.equals("")?lastAD:target;
		if(target!=null){
			if(insides.containsKey(target)){
				stack=insides.get(target);
				stack.pop();

				//To avoid EmptyStackException later on
				if(stack.isEmpty())
				    insides.remove(target);
			}
		}
	}
	public void addFinalNode(Annotation a){
		String target=getFromAnnotation(a,"id").toString();
		boolean addFinal = Boolean.valueOf(getFromAnnotation(a,"finalNode").toString());
		ActivityDiagramBuilder adb=factory.getActivityDiagram(target);
		System.out.println("ADB: " + adb.getName());
		ControllableElement ce=lastElements.get(target+target)==null?null:lastElements.get(target+target).one;
		if(ce != null && addFinal){
		    	ce.getFlows().clear();
			adb.addFinal("final");
			System.out.println("FINALIZE : "+ce.getName()+" -> final");
			ce.addCFlowTo("final","","","","");
		}
		lastElements.remove(target+target);
	}
	public void removeLastElement(Annotation a){
		String target=getFromAnnotation(a,"ad_id").toString();
		String inside="";
		target=target.equals("")?lastAD:target;
		if(target!=null){
		    	if(insides.containsKey(target)){
				inside=insides.get(target).peek();
				System.out.println("PEEK = "+inside);
			}
		    	System.out.println("Removing : "+(target+(inside.equals("")?target:inside)));
			lastElements.remove(target+(inside.equals("")?target:inside));
		}
	}
	public void annotationEvent(Annotation a){
		System.out.println(a);
		ControllableElement ce;
		ControllableElement l=null;
		ControllableElement merge;
		ControllableElement nextMerge;
		Tuple<ControllableElement,Annotation> t;
		//String inside=getFromAnnotation(a,"inside").toString();
		String inside="";
		String id=getFromAnnotation(a,"id").toString();
		String target=getFromAnnotation(a,"ad_id").toString();
		String behavior;
		try {
			behavior=getFromAnnotation(a,"behavior").toString();
		} catch (NullPointerException e) {
			behavior = null;
		}
		System.out.println("Adding "+getFromAnnotation(a,"id"));
		target=target.equals("")?lastAD:target;
		ControlFlow cf;
		String label;
		boolean insideIf;
		if(target!=null){
			System.out.println("TARGET = "+target);
		    	if(insides.containsKey(target)){
				inside=insides.get(target).peek();
				System.out.println("PEEK = "+inside);
			}
			lastAD=target;
			ActivityDiagramBuilder adb=factory.getActivityDiagram(target);
			annotations.activitydiagram.ControlFlow[] flows=(annotations.activitydiagram.ControlFlow[])getFromAnnotation(a,"flows");
			try{
				if(!duplicates.contains(target+id)){
					ce=addActivity(adb,a.annotationType().getSimpleName(), id,StringEscapeUtils.escapeJava(bodySource),inside,flows,behavior);
					if(ce instanceof PinnableElement){
						annotations.activitydiagram.Pin[] pins=(annotations.activitydiagram.Pin[])getFromAnnotation(a,"pins");
						addPins((PinnableElement)ce,pins);
					}

					t=lastElements.get(target+(inside.equals("")?target:inside));
					if(t!=null){
						l=t.one();
						annotations.activitydiagram.ControlFlow[] flowsLast=(annotations.activitydiagram.ControlFlow[])getFromAnnotation(t.two(),"flows");
						System.out.println("LAST : "+l.getName()+" "+t.two()+" "+flowsLast);
						if(l!=null &&  (flowsLast == null || flowsLast.length==0)){
					        	cf=l.addCFlowTo(ce.getName(),"","UAL","","");
							if(cfToBeSaved>0){
								System.out.println("PUT CFLOW TO LABEL "+target+l.getName());
								controlFlowsToLabel.put(target+l.getName(),cf);
								cfToBeSaved--;
							}
					    		
							System.out.println("Added CFLOW from "+l.getName()+" to "+ce.getName());
						}
					}
					addFlows(ce,flows);
					duplicates.add(target+id);

				}else{
				   	ce=adb.getElement(id);
				   	t=lastElements.get(target+(inside.equals("")?target:inside));
					if(t!=null){
						l=t.one();
						annotations.activitydiagram.ControlFlow[] flowsLast=(annotations.activitydiagram.ControlFlow[])getFromAnnotation(t.two(),"flows");
						System.out.println("LAST : "+l.getName()+" "+t.two()+" "+flowsLast);
						if(flowsLast !=null && flowsLast.length==0 && l.getFlows().size()==0){
					        	cf=l.addCFlowTo(ce.getName(),"","","","");
							if(cfToBeSaved>0){
								controlFlowsToLabel.put(target+id,cf);
								cfToBeSaved--;
							}
					        	System.out.println("Added CFLOW from "+l.getName()+" to "+ce.getName());
						}
					}
					
				}

				lastElements.put(target+(inside.equals("")?target:inside),Tuple.<ControllableElement,Annotation>create(ce,a));
		}catch(Exception e){
				e.printStackTrace();
			}
		}else{
			System.err.println("No AD definied before!");
			//LOG
		}
	}
	
	private ControllableElement addActivity(ActivityDiagramBuilder adb,String cName,String name, String body, String inside,annotations.activitydiagram.ControlFlow[] flows,String behavior)throws Exception{
		ControllableElement ret=null,c;
		String outerMerge;	
		Method m;
		Class<?> clazz=adb.getClass();
		CompositeElement ce;
		Object obj=adb;
		String adName=adb.getName();
		if(!inside.equals("")){
			obj=adb.getElement(inside);
			clazz=obj.getClass();
		}else
			inside=adb.getName();
		System.out.println("OBJ : "+obj);	
		//m=clazz.getMethod("add"+cName,new Class[]{String.class});
		//System.out.println(m.toString());
		if (cName.equals("CallAction")) {
			m=clazz.getMethod("add"+cName,new Class[]{String.class, String.class, String.class});
			ret=(ControllableElement)m.invoke(obj,new Object[]{name, "UAL", body});
				
		}
		else if (cName.equals("CallBehavior")) {
			m=clazz.getMethod("add"+cName,new Class[]{String.class, String.class});
			ret=(ControllableElement)m.invoke(obj,new Object[]{name, behavior});
				
		}
		else {
		m=clazz.getMethod("add"+cName,new Class[]{String.class});
			//System.out.println("Adding something else!");
			ret=(ControllableElement)m.invoke(obj,new Object[]{name});
			}
		return ret;
	}
	public void setLabelAB(Annotation a,AnnotatedBlock ab,Object condition){
		String id=getFromAnnotation(a,"id").toString();
		String target=getFromAnnotation(a,"ad_id").toString();
		target=target.equals("")?lastAD:target;
		ControlFlow cf=controlFlowsToLabel.remove(target+id);
		try{
			System.out.println("CONDITION : "+condition);
			System.out.println(ab);
			System.out.println(target+id+" "+cf);
			if(cf!=null) {	//set name and guard
		    		cf.setName(condition.toString());
		    		cf.setGuard(condition.toString());
		    }		
		}catch(Exception e){
		    	e.printStackTrace();
			System.err.println("Could not retrive condition from annotation!");
		}
	}
	public void closeDecision(Annotation a){
		String id=getFromAnnotation(a,"id").toString();
		String target=getFromAnnotation(a,"ad_id").toString();
		//String inside=getFromAnnotation(a,"inside").toString();
		String inside="";
		target=target.equals("")?lastAD:target;
		ControllableElement ce,l;
		ControlFlow cf;
		Tuple<ControllableElement,Annotation> t;
		ActivityDiagramBuilder adb;
		if(target!=null){
		    	if(insides.containsKey(target)){
				inside=insides.get(target).peek();
				System.out.println("PEEK = "+inside);
			}
			lastAD=target;
			try{
				adb=factory.getActivityDiagram(target);
				System.out.println("searching for "+id+"merge");
				try{
					ce=adb.getElement(id+"merge");
				}catch(RuntimeException e){
					ce=addActivity(adb,"Merge",id+"merge",null,inside,null,null);
				}
				t=lastElements.get(target+(inside.equals("")?target:inside));
				if(t!=null){
					l=t.one;
					annotations.activitydiagram.ControlFlow[] flowsLast=(annotations.activitydiagram.ControlFlow[])getFromAnnotation(t.two,"flows");
					System.out.println("LAST : "+l.getName()+" "+t.two+" "+flowsLast);
					if(l!=null &&  (flowsLast == null || flowsLast.length==0)){
			        		cf=l.addCFlowTo(ce.getName(),"","","","");
						if(cfToBeSaved>0){
							System.out.println("PUT CFLOW TO LABEL "+target+l.getName());
							controlFlowsToLabel.put(target+l.getName(),cf);
							cfToBeSaved--;
						}
			    		
						System.out.println("Added CFLOW from "+l.getName()+" to "+ce.getName());
					}
				}
				lastElements.put(target+(inside.equals("")?target:inside),Tuple.<ControllableElement,Annotation>create(ce,null));
			}catch(Exception e){
				e.printStackTrace();
			}

		}

	}
	public boolean isElementInsideTopIf(){
		Tuple<Integer,Integer> boundries=ifs.peek().two;
		return startSourceLine>=boundries.one && startSourceLine <=boundries.two;

	}
	public Object getFromAnnotation(Annotation a,String prop){
		Object ret=null;
		try{
			Class<?> clazz=a.getClass();
			Method m=clazz.getMethod(prop,new Class[]{});
			ret= m.invoke(a,new Object[]{});
			System.out.println(ret);
		}catch(Exception e){
			System.err.println(prop+" does not exists!");
			//e.printStackTrace();
		}
		return ret;
	}
	public void addPins(PinnableElement pe,annotations.activitydiagram.Pin[] pins){
		if(pins!=null)
			for(annotations.activitydiagram.Pin pin:pins){
				if(pin.role()==PINTYPE.IN)
                     			pe.addInputPin(pin.id(),pin.type(),pin.upper(),pin.lower());
				else
					pe.addOutputPin(pin.id(),pin.type(),pin.upper(),pin.lower());

                }
	}
	public void addFlows(ControllableElement ce,annotations.activitydiagram.ControlFlow[] flows){
		if(flows!=null){
			for(annotations.activitydiagram.ControlFlow flow:flows){
				ce.addCFlowTo(flow.dest(),flow.name(),"UAL",flow.guard(),flow.weight());
			}
		}
	}
	public static void main(String[] args){
		ConfigurationManager cm = new ConfigurationManager("conf.xml");
		CodeToActivityDiagram ctad=new CodeToActivityDiagram(cm.getADConfig(),cm.getGeneralConfig());
		if(args.length>0){
			ctad.invokeMain(args[0],Util.copyOfRange(args,1,args.length));
		}
		//ctad.finalize();
		ctad.build("ClassDiagrams.java");
	}
	public void invokeMain(String mainClass,String[] args){
		try{
			Class<?> c=getClass(mainClass);
			Method main=c.getMethod("main",String[].class);
			System.out.println(mainClass+" "+args[0]+" "+args.getClass()+" "+main.getParameterTypes()[0]);
			main.invoke(null,new Object[]{args});
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	private Class<?> getClass(String className) {
                Class<?> c;
                try {
                        c = classLoader.loadClass(className);
                } catch (ClassNotFoundException e) {
                        throw new RuntimeException("Class Not Found : " + e.getMessage());
                }
                return c;
        }

}
