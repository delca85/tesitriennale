package activitydiagram;

import java.lang.reflect.*;
import java.lang.annotation.*;
import annotations.activitydiagram.*;
import util.*;
import org.aspectj.lang.*;
import ataspectj.reflect.*;
import atjava.lang.annotation.*;

public aspect ActivityDiagramAspect{
	
	private static CodeToActivityDiagram LISTENER;

	public pointcut ad():  (call(* *.atAspectjNewMethod*ActivityDiagram*(..)))  ;
	public pointcut callAction(): (call(* *.atAspectjNewMethod*CallAction*(..)))  ;
	public pointcut callBehavior(): (call(* *.atAspectjNewMethod*CallBehavior*(..)))  ;
	public pointcut acceptEvent(): (call(* *.atAspectjNewMethod*AcceptEvent*(..))) ;
	public pointcut conditional(): (call(* *.atAspectjNewMethod*Conditional*(..))) ;
	public pointcut decision(): (call(* *.atAspectjNewMethod*Decision*(..))) ;
	public pointcut cf(): (call(* *.atAspectjNewMethod*ControlFlow*(..))) ;
	public pointcut fin(): (call(* *.atAspectjNewMethod*Final*(..))) ;
	public pointcut fork(): (call(* *.atAspectjNewMethod*Fork*(..))) ;
	public pointcut merge(): (call(* *.atAspectjNewMethod*Merge*(..))) ;
	public pointcut join(): (call(* *.atAspectjNewMethod*Join*(..))) ;
	public pointcut init(): (call(* *.atAspectjNewMethod*Initial*(..))) ;
	public pointcut loop(): (call(* *.atAspectjNewMethod*Loop*(..))) ;
	public pointcut partition(): (call(* *.atAspectjNewMethod*Partition*(..))) ;
	public pointcut pin(): (call(* *.atAspectjNewMethod*Pin*(..))) ;
	public pointcut sendSignal(): (call(* *.atAspectjNewMethod*SendSignal*(..))) ;
	public pointcut struct(): (call(* *.atAspectjNewMethod*Structured*(..))) ;

	
	public pointcut adE():  (newAspectJPointcut_ActivityDiagram_Annotation(*))   ;
    public pointcut callActionE(): (newAspectJPointcut_CallAction_Annotation(*))   ;
    public pointcut callBehaviorE(): (newAspectJPointcut_CallBehavior_Annotation(*))   ;
    public pointcut acceptEventE(): (newAspectJPointcut_AcceptEvent_Annotation(*))  ;
    public pointcut conditionalE(): (newAspectJPointcut_Conditional_Annotation(*))  ;
    public pointcut decisionE(): (newAspectJPointcut_Decision_Annotation(*))  ;
    public pointcut cfE(): (newAspectJPointcut_ControlFlow_Annotation(*))  ;
    public pointcut finE(): (newAspectJPointcut_Final_Annotation(*))  ;
    public pointcut forkE(): (newAspectJPointcut_Fork_Annotation(*))  ;
    public pointcut mergeE(): (newAspectJPointcut_Merge_Annotation(*))  ;
    public pointcut joinE(): (newAspectJPointcut_Join_Annotation(*))  ;
    public pointcut initE(): (newAspectJPointcut_Initial_Annotation(*))  ;
    public pointcut loopE(): (newAspectJPointcut_Loop_Annotation(*))  ;
    public pointcut partitionE(): (newAspectJPointcut_Partition_Annotation(*))  ;
    public pointcut pinE(): (newAspectJPointcut_Pin_Annotation(*))  ;
    public pointcut sendSignalE(): (newAspectJPointcut_SendSignal_Annotation(*))  ;
    public pointcut structE(): (newAspectJPointcut_Structured_Annotation(*))  ;

	
	public static void setContext(CodeToActivityDiagram listener){
		LISTENER=listener;
		System.out.println("Listener setted");
	}
	private Object[] extractAnnotationAndBlock(JoinPoint jp){
		BlockJoinPointStaticPart bjp=(BlockJoinPointStaticPart) JoinPointFactory.getJoinPointStaticPart(jp);		
		String annName;
		AnnotatedBlock ab;
		Annotation ann;
		Method getAB,getAnnName;
		Method callBack;
		try{
			getAB=BlockJoinPointStaticPart.class.getDeclaredMethod("getAnnotatedBlock",new Class[]{});
			getAB.setAccessible(true);
			getAnnName=BlockJoinPointStaticPart.class.getDeclaredMethod("getAnnotationName",new Class[]{});
			getAnnName.setAccessible(true);
			annName=(String)getAnnName.invoke(bjp,new Object[]{});
			annName=annName.substring(annName.lastIndexOf('.')+1);	
			ab=(AnnotatedBlock)getAB.invoke(bjp,new Object[]{});
			ann=ab.getAnnotation();
			return new Object[]{ann,ab,annName};
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;

	}
	void around(): decision(){
		Object[] extract=extractAnnotationAndBlock(thisJoinPoint);
		Annotation ann;
		AnnotatedBlock ab;
		String annName;
		System.out.println("decision");
		if(extract!=null){
		    	System.out.println("not null");
			ann=(Annotation)extract[0];
			ab=(AnnotatedBlock)extract[1];
			annName=(String)extract[2];
			LISTENER.setInstance(thisJoinPoint.getThis());			
			LISTENER.blockAnnotationEvent(ann,ab);
			LISTENER.addCfToBeSaved();
			
			proceed();
			
			Object [] args=thisJoinPoint.getArgs();
			System.out.println("after: ");
			ann=(Annotation)extract[0];
			System.out.println("ann: " + ann);
			ab=(AnnotatedBlock)extract[1];
			try {
			  System.out.println("ab: " + ab.getBodySourceCode());
			  System.out.println("args[0]: " + args[0]);
			}catch(Exception e){
			  System.out.println("non va!");
			}

			//Since the evalutation of dinamyc properties of annotation in @Java is done AFTERWARDS the execution, i cannot retrive the values i'm looking for (boolean condition of the if).
			//while looking into the decompiled code i saw that the JoinPoint created before the call has the value i'm looking for.
			//Hopefully it is a deterministic behavior and is not just in this case that the value is inserted in the JoinPoint
			//The dynamic value is in args[0]

			LISTENER.setLabelAB(ann,ab,args[0]);
			LISTENER.closeDecision(ann);
		}	

	}
	void around(): struct() || partition() || conditional() || loop() {
		Object[] extract=extractAnnotationAndBlock(thisJoinPoint);
		Annotation ann;
		AnnotatedBlock ab;
		String annName;
		if(extract!=null){
			ann=(Annotation)extract[0];
			ab=(AnnotatedBlock)extract[1];
			annName=(String)extract[2];
			LISTENER.setInstance(thisJoinPoint.getThis());
			LISTENER.blockAnnotationEvent(ann,ab);
			LISTENER.pushInside(ann);
			proceed();
			LISTENER.removeLastElement(ann);
			LISTENER.popInside(ann);
		}
	}
	void around(): ad() {
		Object[] extract=extractAnnotationAndBlock(thisJoinPoint);
		Annotation ann;
		AnnotatedBlock ab;
		String annName;
		if(extract!=null){
			ann=(Annotation)extract[0];
			ab=(AnnotatedBlock)extract[1];
			annName=(String)extract[2];
			LISTENER.setInstance(thisJoinPoint.getThis());
			LISTENER.activitydiagramEvent(ann);
			System.out.println("Start activitydiagram");
			proceed();
			System.out.println("End activitydiagram");
			LISTENER.addFinalNode(ann);
		}
	}
	before():callAction() || callBehavior() || acceptEvent() || sendSignal() || pin() || init() || join() || merge() || fork() || fin() || cf() {
		System.out.println("before()");
		Object[] extract=extractAnnotationAndBlock(thisJoinPoint);
		Annotation ann;
		AnnotatedBlock ab;
		String annName;
		if(extract!=null){
			ann=(Annotation)extract[0];
			ab=(AnnotatedBlock)extract[1];
			annName=(String)extract[2];

			LISTENER.setInstance(thisJoinPoint.getThis());			
			if(annName.equalsIgnoreCase("activitydiagram"))
				LISTENER.activitydiagramEvent(ann);
			else
				LISTENER.blockAnnotationEvent(ann,ab);
		}
	}
	before(): adE() || callActionE() || callBehaviorE() || acceptEventE() || loopE() || sendSignalE() || structE() || pinE() || partitionE() || initE() || joinE() || mergeE() || forkE() || finE() || cfE() || decisionE() || conditionalE() {
		ExprJoinPointStaticPart ejp=(ExprJoinPointStaticPart) JoinPointFactory.getJoinPointStaticPart(thisJoinPoint);
		String annName;
                AnnotatedExpression ae;
                Annotation ann;
                Method getAE,getAnnName;
                Method callBack;
                System.out.println("Activity EXPR ANNOTATION");
               
		System.out.println("EXPR ANNOTATION");
                try{
                        getAE=ExprJoinPointStaticPart.class.getDeclaredMethod("getAnnotatedExpression",new Class[]{});
                        getAE.setAccessible(true);
                        getAnnName=ExprJoinPointStaticPart.class.getDeclaredMethod("getAnnotationName",new Class[]{});
                        getAnnName.setAccessible(true);
                        annName=(String)getAnnName.invoke(ejp,new Object[]{});
                        annName=annName.substring(annName.lastIndexOf('.')+1);
			System.out.println(ejp.getSourceLocation().getWithinType()+" "+thisJoinPoint.getThis().getClass());
			System.out.println("Annotation name: "+annName);
                        ae=(AnnotatedExpression)getAE.invoke(ejp,new Object[]{});
			System.out.println(ae);
                        ann=ae.getAnnotation();
			System.out.println(ann);
			
			LISTENER.setInstance(thisJoinPoint.getThis());

                        if(annName.equalsIgnoreCase("activitydiagram"))
                                LISTENER.activitydiagramEvent(ann);
                        else
                                LISTENER.exprAnnotationEvent(ann,ae);
                }catch(Exception e){
                        e.printStackTrace();
                }
	
	}
 pointcut newAspectJPointcut_Pin_Annotation(String ataspectj) : call(* wrapperExpressionAnnotation(..)) && args(ataspectj,..); pointcut newAspectJPointcut_CallAction_Annotation(String ataspectj) : call(* wrapperExpressionAnnotation(..)) && args(ataspectj,..); pointcut newAspectJPointcut_SendSignal_Annotation(String ataspectj) : call(* wrapperExpressionAnnotation(..)) && args(ataspectj,..); pointcut newAspectJPointcut_Initial_Annotation(String ataspectj) : call(* wrapperExpressionAnnotation(..)) && args(ataspectj,..); pointcut newAspectJPointcut_Decision_Annotation(String ataspectj) : call(* wrapperExpressionAnnotation(..)) && args(ataspectj,..); pointcut newAspectJPointcut_Structured_Annotation(String ataspectj) : call(* wrapperExpressionAnnotation(..)) && args(ataspectj,..); pointcut newAspectJPointcut_Join_Annotation(String ataspectj) : call(* wrapperExpressionAnnotation(..)) && args(ataspectj,..); pointcut newAspectJPointcut_Merge_Annotation(String ataspectj) : call(* wrapperExpressionAnnotation(..)) && args(ataspectj,..); pointcut newAspectJPointcut_Fork_Annotation(String ataspectj) : call(* wrapperExpressionAnnotation(..)) && args(ataspectj,..); pointcut newAspectJPointcut_ControlFlow_Annotation(String ataspectj) : call(* wrapperExpressionAnnotation(..)) && args(ataspectj,..); pointcut newAspectJPointcut_Conditional_Annotation(String ataspectj) : call(* wrapperExpressionAnnotation(..)) && args(ataspectj,..); pointcut newAspectJPointcut_ActivityDiagram_Annotation(String ataspectj) : call(* wrapperExpressionAnnotation(..)) && args(ataspectj,..); pointcut newAspectJPointcut_AcceptEvent_Annotation(String ataspectj) : call(* wrapperExpressionAnnotation(..)) && args(ataspectj,..); pointcut newAspectJPointcut_Partition_Annotation(String ataspectj) : call(* wrapperExpressionAnnotation(..)) && args(ataspectj,..); pointcut newAspectJPointcut_Loop_Annotation(String ataspectj) : call(* wrapperExpressionAnnotation(..)) && args(ataspectj,..); pointcut newAspectJPointcut_CallBehavior_Annotation(String ataspectj) : call(* wrapperExpressionAnnotation(..)) && args(ataspectj,..); pointcut newAspectJPointcut_Final_Annotation(String ataspectj) : call(* wrapperExpressionAnnotation(..)) && args(ataspectj,..);}
