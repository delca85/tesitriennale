import annotations.sequencediagram.*;

public class TestClientServer {
	public static void main(String[] args) {
                Server s = new Server("ServerA");
                @SequenceDiagram(id="Client/Server",name="Request Authentication") {
			Client c = new Client();
			c.getResource(s, "User", "GoodPass", "2424234");
		}
		@SequenceDiagram(id="Client/Server2",name="Request Authentication 2") {
			Client c2 = new Client();
			c2.getResource(s, "User", "BadPass", "2424234");
		}
	}
}