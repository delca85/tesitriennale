package util;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class Util {
	public static URL[] StringsToURLs(Object[] strings) {
		URL[] urls = new URL[strings.length];
		for (int i = 0; i < strings.length; i++) {
			try {
			    	System.out.println(strings[i]);
				urls[i] = new URL("file://" + strings[i] + File.separator);
			} catch (MalformedURLException e) {
				throw new RuntimeException("URL exception " + e.getMessage());
			}
		}
		return urls;
	}

	public static String[] extractNames(Class<?>[] cls, boolean qualifiedNames) {
		String[] ret = new String[cls.length];
		if (qualifiedNames)
			for (int i = 0; i < cls.length; i++)
				ret[i] = cls[i].getName();
		else
			for (int i = 0; i < cls.length; i++)
				if (cls[i].isArray())
					ret[i] = cls[i].getName();
				else
					ret[i] = cls[i].getSimpleName();
		return ret;
	}
	public static String toLowerCaseFirst(String str){
		return str.substring(0,1).toLowerCase()+str.substring(1);
	}
    	public static int countLines(String text){
		return text.split("\n|\r\n|\r").length;
	}
	public static String[] copyOfRange(String[] array,int start,int end){
	   String[] ret=new String[end-start];
	   for(int i=0;i<end-start;i++)
	       ret[i]=array[start+i];
	   return ret;
	}
}
