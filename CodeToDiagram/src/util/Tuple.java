package util;
import activitydiagram.*;
import java.lang.annotation.*;

public class Tuple<P1,P2>{
	public final P1 one;
	public final P2 two;
	public static <P1,P2> Tuple<P1,P2> create(P1 one,P2 two){
		return new Tuple<P1,P2>(one,two);
	}
	
	private Tuple(P1 one,P2 two){
		this.one=one;
		this.two=two;
	}

	public P1 one(){
	    return one;
	}
	public P2 two(){
	    return two;
	}
}
