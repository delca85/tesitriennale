package sequencediagram;

import java.lang.reflect.*;

public class MessageInfo {

	private String senderName;
	private Class senderClass;
	private String receiverName;
	private Class receiverClass;
	private Method method;
	private String sequenceDiagram;
	private boolean async;
	private Object[] args;

	public MessageInfo(String senderName, Class senderClass, String receiverName, Class receiverClass, Method m, String sequenceDiagram, boolean async, Object[] args) {
		this.senderName = senderName;
		this.senderClass = senderClass;
		this.receiverName = receiverName;
		this.receiverClass = receiverClass;
		this.method = m;
		this.sequenceDiagram = sequenceDiagram;
		this.async = async;
		this.args = args;
	}	
	
	public String getSenderName() {
		return senderName;
	}
	
	public Class getSenderClass() {
		return senderClass;
	}
	
	public String getReceiverName() {
		return receiverName;
	}
	
	public Class getReceiverClass() {
		return receiverClass;
	}
	
	public Method getMethod() {
		return method;
	}
	
	public String getSequenceDiagram() {
		return sequenceDiagram;
	}
	
	public void setSenderClass(String n) {
		senderName = n;
	}
	
	public void setSenderClass(Class c) {
		senderClass = c;
	}
	
	public void setReceiverName(String n) {
		receiverName = n;
	}
	
	public void setReceiverClass(Class c) {
		receiverClass = c;
	}
	
	public void setMethod(Method m) {
		method = m;
	}
	
	public void setTarget(String t) {
		sequenceDiagram = t;
	}
	
	public boolean getAsync() {
		return async;
	}
	
	public Object[] getParams() {
		return args;
	}
	/*
	public MessageInfo response() {
		return new MessageInfo(receiverName + "response", receiverClass, senderName, senderClass, method, sequenceDiagram);
	}
	*/
	public String toString() {
		return senderName + " " + senderClass.getSimpleName() + " " + receiverName + " " + receiverClass.getSimpleName() + " " + method + " " + sequenceDiagram;
	}
}