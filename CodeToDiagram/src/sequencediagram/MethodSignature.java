package sequencediagram;

import java.lang.reflect.*;

public class MethodSignature {

	Class clazz;
	String method;
	Object[] args;
	
	public MethodSignature(Class clazz, String method, Object[] args) {
		this.clazz = clazz;
		this.method = method;
		this.args = args;
	}
	
	public Class getClazz() {
		return clazz;
	}
	
	public String getMethod() {
		return method;
	}
	
	public Object[] getArgs() {
		return args;
	}
}