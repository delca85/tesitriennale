package sequencediagram;

import java.net.URLClassLoader;
import java.lang.reflect.*;
import java.lang.annotation.*;
import util.*;
import java.util.*;
import java.util.regex.*;
import configuration.*;
import factory.DiagramFactory;
import annotations.sequencediagram.*;
import atjava.lang.annotation.*;

public class CodeToSequenceDiagram{

	private SDConfiguration conf;
	private GeneralConfiguration generalConf;
	private URLClassLoader classLoader;
	private DiagramFactory factory;
	private String lastSD;
	private AnnotatedBlock ab;
	private AnnotatedExpression ae;
	private Object instance;
	private HashMap<String, Stack<String>> lifelineStack;
	private Stack<MessageInfo> messageStack;
	private Stack<MethodSignature> methodSignature;
	private HashMap<Object, String> lifelineHM;
	private int startSourceLine,endSourceLine;
	private String source;
	private String bodySource;
	
	public CodeToSequenceDiagram(SDConfiguration conf, GeneralConfiguration generalConf){
		this.conf = conf;
		this.generalConf = generalConf;
		classLoader = new URLClassLoader(Util.StringsToURLs(generalConf.getClassPaths()));
		URLClassLoader modelClassLoader = new URLClassLoader(
				Util.StringsToURLs(generalConf.getModelClassPaths()));
		try {
			factory = ((DiagramFactory) modelClassLoader.loadClass(
					generalConf.getModelFactory()).newInstance());
		} catch (Exception e) {
			throw new RuntimeException("Cannot load correctly Model Factory : "
					+ e.getMessage());
		}
		lifelineStack = new HashMap<String, Stack<String>>();
		lifelineHM = new HashMap<Object, String>();
		messageStack = new Stack<MessageInfo>();
		methodSignature = new Stack<MethodSignature>();
		setContext();
	}
	public void setInstance(Object instance){
		this.instance=instance;
	}
	public void build(){
		factory.buildSequenceDiagrams();
		factory.exportToFile(generalConf.getOutputFolder()+"/SequenceDiagrams.java");
	}
	public void build(String s){                       //the arg is the name of the class diagram we need to build the sequence
		factory.buildSequenceDiagrams(generalConf.getOutputFolder()+s);
		factory.exportToFile(generalConf.getOutputFolder()+"/SequenceDiagrams.java");
	}
	private void setContext(){
		try{
			Class<?> c=Class.forName("sequencediagram.SequenceDiagramAspect");
			Method m=c.getMethod("setContext",CodeToSequenceDiagram.class);
			m.invoke(null,this);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void addMethodSignature(Class clazz, String method, Object[] args) {
		String regex = "(.*)\\.(\\w*)\\((.*)\\)";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(method);
                if (matcher.matches()) {
                    //System.out.println("---------------------------------[][][][][][]------------------> nome metodo: " + matcher.group(2));
                    MethodSignature ms = new MethodSignature(clazz, matcher.group(2), args);
                    methodSignature.push(ms);
                }
	}
	
	public void sequenceDiagramEvent(Annotation a){
		System.out.println("SD EVENT");
		System.out.println(a);
		SequenceDiagram toAdd=(SequenceDiagram)a;
		lastSD=toAdd.id();
		System.out.println("Adding "+lastSD+" sequence diagram");
		SequenceDiagramBuilder sdb=factory.getSequenceDiagram(lastSD);
		System.out.println("Added SD with ID = " + lastSD);
		annotations.sequencediagram.NewLifeline[] lifelines =(annotations.sequencediagram.NewLifeline[])getFromAnnotation(a,"lifelines");
		for (annotations.sequencediagram.NewLifeline lifeline : lifelines)
			newLifelineEvent(lifeline);
	}
	public void blockAnnotationEvent(Annotation a,AnnotatedBlock ab, Object[] args){
		this.ab=ab;
		extractStartEnd(ab);
		if (a.annotationType().getSimpleName().equals("Message")) {
			System.out.println("BlockMessage Annotation");
			messageEvent(a, args);
		}
		else if (a.annotationType().getSimpleName().equals("Lifeline")) {
			System.out.println("NewLifeline Annotation");
			lifelineEvent(a);
		}
		else {
			System.out.println("Other expr Annotation");
		}
		this.ab=null;
	}
	public void exprAnnotationEvent(Annotation a,AnnotatedExpression ae, Object obj){
		this.ae=ae;
		extractStartEnd(ae);
		if (a.annotationType().getSimpleName().equals("Message")) {
			System.out.println("ExprMessage Annotation");
			messageEvent(a, obj);
		}
		else if (a.annotationType().getSimpleName().equals("Lifeline")) {
			System.out.println("NewLifeline Annotation");
			lifelineEvent(a);
		}
		else {
			System.out.println("Other expr Annotation");
		}
		this.ae=null;
	}
	public void resetStartEnd(){
		startSourceLine=-1;
		endSourceLine=-1;
	}
	public void extractStartEnd(AnnotatedExpression ae){
		startSourceLine=ae.startSourceLine();
		endSourceLine=ae.endSourceLine();
		try{
			source=ae.getSourceCode();
			bodySource=ae.getBodySourceCode();
		}catch(Exception e){
			source=e.getMessage();
			bodySource=e.getMessage();
		}
	}
	public void extractStartEnd(AnnotatedBlock ab){
		startSourceLine=ab.startSourceLine();
		endSourceLine=ab.endSourceLine();
		try{
			source=ab.getSourceCode();
			bodySource=ab.getBodySourceCode();
		}catch(Exception e){
			source=e.getMessage();
			bodySource=e.getMessage();
		}		
	}
	public void messageEvent(Annotation a, Object[] args){
		System.out.println("This is a MessageEvent : "+a.annotationType().getSimpleName() + " method " + parseMessage(bodySource)[1]);
		
		String target=getFromAnnotation(a,"sd_id").toString();
		target=target.equals("")?lastSD:target;
		
		if (!lifelineStack.containsKey(target))
                    lifelineStack.put(target, new Stack<String>());
		
		String sender = getFromAnnotation(a,"sender").toString();
		if (sender.equals("")) {
			System.out.println("Setting default sender");
			sender = this.lifelineStack.get(target).empty()?instance.getClass().getName().toString().toLowerCase():this.lifelineStack.get(target).peek();
			//sender = this.lifelineStack.empty()?instance.getClass().getName()+"addrand":this.lifelineStack.peek();
			//sender = this.lifelineHM.containsKey(this.instance)?this.lifelineHM.get(this.instance):lifelineHM.put(this.instance, this.instance.getClass().getName().toString().toLowerCase());
		}
		if (this.lifelineStack.get(target).empty()) {
			this.lifelineStack.get(target).push(sender);
		}
		String receiver = getFromAnnotation(a,"receiver").toString();
		//String message_name = getFromAnnotation(a,"name").toString();
		if (receiver.equals(""))
			receiver = parseMessage(bodySource)[0];
			if (receiver.equals("this"))
				receiver = lifelineStack.get(target).peek();
		//if (message_name.equals(""))
		
		Class receiverClass =  args[args.length-1].getClass();		
		String message_name = parseMessage(bodySource)[1];
		Object[] subArgs = Arrays.copyOfRange(args, 0, args.length-1);
		Class[] argsClass = new Class[subArgs.length];
		for (int i = 0; i < subArgs.length; i++) {
			argsClass[i] = subArgs[i].getClass();
		}	
		Method meth = null;
		try {
		meth = receiverClass.getMethod(message_name, argsClass);
		} catch (Exception e) {                        
			//e.printStackTrace();
		}
		if (meth == null) {
                    System.out.println("trying reverse");
                    subArgs = Arrays.copyOfRange(args, 1, args.length);
                    for (int i = 0; i < subArgs.length; i++) {
			argsClass[i] = subArgs[i].getClass();
                    }
                    receiverClass =  args[0].getClass();
                    try {
                        meth = receiverClass.getMethod(message_name, argsClass);
                    }
                    catch (Exception e) {
                        //e.printStackTrace();
                    }
		}
		if (meth == null) {
                    System.out.println("trying with a method of the same class");
                    receiverClass = instance.getClass();
                    argsClass = new Class[args.length];
                    subArgs = Arrays.copyOfRange(args, 0, args.length);
                    for (int i = 0; i < args.length; i++) {
			argsClass[i] = args[i].getClass();
                    }
                    try {
                        meth = receiverClass.getMethod(message_name, argsClass);
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
		}
		
		Object[] params = new Object[subArgs.length+1];
		for (int i = 0; i < subArgs.length; i++) {
			params[i] = subArgs[i];
		}
		params[subArgs.length] = null;
		//String async = getFromAnnotation(a,"asynchronous").toString();
		boolean async = Boolean.parseBoolean(getFromAnnotation(a,"asynchronous").toString());
		System.out.println("Sender: "+sender+" Receiver: "+receiver + "Async: " + async);
		
		this.lifelineStack.get(target).push(receiver);
		
		try {
		//Constructor messageConstr = MessageInfo.class.getConstructor(new Class[]{String.class, Class.class, String.class, Class.class, Method.class, String.class});
		//Object mi = messageConstr.newInstance(new Object[]{sender, instance.getClass(), receiver, null, null, target});
		//mi.setSenderName = sender;
		
		MessageInfo mi = new MessageInfo(sender, instance.getClass(), receiver, receiverClass, meth, target, async, params);
		//System.out.println(mi);
		this.messageStack.push(mi);
		SequenceDiagramBuilder sdb=factory.getSequenceDiagram(target);
		addLifeline(sdb, sender, this.instance.getClass().getCanonicalName());
		
		//System.out.println(this.messageStack.peek());
		/*String target=getFromAnnotation(a,"sd_id").toString();
		target=target.equals("")?lastSD:target;
		SequenceDiagramBuilder sdb=factory.getSequenceDiagram(target);
		addLifeline(sdb, sender);
		addLifeline(sdb, receiver);*/
		//addMessage(sdb, message_name, sender, receiver);
		
		//SequenceDiagramBuilder sdb=factory.getSequenceDiagram(messageStack.peek().getSequenceDiagram());
		addLifeline(sdb, messageStack.peek().getReceiverName(), receiverClass.getCanonicalName());
		addMessage(sdb, messageStack.peek());
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void messageEvent(Annotation a, Object o){
		System.out.println("This is a MessageEvent : "+a.annotationType().getSimpleName() + " method " + parseMessage(bodySource)[1]);
		
		
		String target=getFromAnnotation(a,"sd_id").toString();
		target=target.equals("")?lastSD:target;
		
		if (!lifelineStack.containsKey(target))
                    lifelineStack.put(target, new Stack<String>());
		
		SequenceDiagramBuilder sdb=factory.getSequenceDiagram(target);
		/*MethodSignature ms = null;
		do {
                    ms = methodSignature.peek();
                    System.out.println("Popped "+ ms.getMethod());
                    if (!sdb.getMessages().isEmpty()) {
                        if (sdb.getMessages().get(sdb.getMessages().size()-1).getMethod().getName().equals(ms.getMethod())) {
                            System.out.println("Dovrei rimuovere "+ms.getMethod()+ " e metterlo dentro " + parseMessage(bodySource)[1]);
                        }
                    }
                }while(!ms.getMethod().equals(parseMessage(bodySource)[1]));*/
		ArrayList<Message> toReAdd= new ArrayList<Message>();
		MethodSignature ms = methodSignature.peek();
		System.out.println("Peeked "+ ms.getMethod());
		/*if (!sdb.getMessages().isEmpty())
                    System.out.println("LAST MESS is " +sdb.getMessages().get(sdb.getMessages().size()-1).getMethod().getName());*/
		while(!ms.getMethod().equals(parseMessage(bodySource)[1])) {
                        if (!sdb.getMessages().isEmpty()) {
                            if (sdb.getMessages().get(sdb.getMessages().size()-1).getMethod().getName().equals(ms.getMethod())) {
                                //System.out.println("Dovrei rimuovere "+ms.getMethod()+ " e metterlo dentro " + parseMessage(bodySource)[1]);
                                toReAdd.add(sdb.getMessages().remove(sdb.getMessages().size()-1));
                                System.out.println("To Re-Add "+toReAdd.get(toReAdd.size()-1).getMethod().getName());
                            }
                        }
                        System.out.println("Popped " + methodSignature.pop().getMethod());
			ms = methodSignature.peek();
			System.out.println("Peeked " + ms.getMethod());
		}
		
		if (!toReAdd.isEmpty())       //se ho trovato messaggi interni da risistemare devo poppare la prima lifeline perchè è relativa al receiver del mesaggio che sto analizzando
                    lifelineStack.get(target).pop();
		
		String sender = getFromAnnotation(a,"sender").toString();
		if (sender.equals("")) {
			System.out.println("Setting default sender");
			sender = this.lifelineStack.get(target).empty()?instance.getClass().getName().toString().toLowerCase():this.lifelineStack.get(target).peek();
			//sender = this.lifelineStack.empty()?instance.getClass().getName()+"addrand":this.lifelineStack.peek();
			//sender = this.lifelineHM.containsKey(this.instance)?this.lifelineHM.get(this.instance):lifelineHM.put(this.instance, this.instance.getClass().getName().toString().toLowerCase());
		}
		if (this.lifelineStack.get(target).empty()) {
			this.lifelineStack.get(target).push(sender);
		}
		String receiver = getFromAnnotation(a,"receiver").toString();
		//String message_name = getFromAnnotation(a,"name").toString();
		if (receiver.equals(""))
			receiver = parseMessage(bodySource)[0];
			if (receiver.equals("this"))
				receiver = lifelineStack.get(target).peek();
		//if (message_name.equals(""))
		
		Class receiverClass = ms.getClazz();
		
		String message_name = ms.getMethod();
		
		Object[] args = ms.getArgs();
		Class[] argsClass = new Class[args.length];
		for (int i = 0; i < args.length; i++) {
			argsClass[i] = args[i].getClass();
		}	
		Method meth = null;
		try {
		meth = receiverClass.getMethod(message_name, argsClass);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Object[] params = new Object[args.length+1];
		for (int i = 0; i < args.length; i++) {
			params[i] = args[i];
		}
		params[args.length] = o;
		
		//String async = getFromAnnotation(a,"asynchronous").toString();
		boolean async = Boolean.parseBoolean(getFromAnnotation(a,"asynchronous").toString());
		System.out.println("Sender: "+sender+" Receiver: "+receiver + "Async: " + async);
		
		if (toReAdd.isEmpty())        //se non è vuoto vuol dire che ho già valutato i mex interni e non mi serve tener traccia del receiver
                    this.lifelineStack.get(target).push(receiver);
		
		try {
		//Constructor messageConstr = MessageInfo.class.getConstructor(new Class[]{String.class, Class.class, String.class, Class.class, Method.class, String.class});
		//Object mi = messageConstr.newInstance(new Object[]{sender, instance.getClass(), receiver, null, null, target});
		//mi.setSenderName = sender;
		
		MessageInfo mi = new MessageInfo(sender, instance.getClass(), receiver, receiverClass, meth, target, async, params);
		//System.out.println(mi);
		this.messageStack.push(mi);
		
		addLifeline(sdb, sender, this.instance.getClass().getCanonicalName());
		
		//System.out.println(this.messageStack.peek());
		/*String target=getFromAnnotation(a,"sd_id").toString();
		target=target.equals("")?lastSD:target;
		SequenceDiagramBuilder sdb=factory.getSequenceDiagram(target);
		addLifeline(sdb, sender);
		addLifeline(sdb, receiver);*/
		//addMessage(sdb, message_name, sender, receiver);
		
		addLifeline(sdb, messageStack.peek().getReceiverName(), receiverClass.getCanonicalName());
		addMessage(sdb, messageStack.peek());
		
		for (int i = toReAdd.size()-1; i >= 0; i--) {         //riaggiungo i mex interni sistemando sender e receiver
                        toReAdd.get(i).setInside(true);
                        if (toReAdd.get(i).getSender().equals(toReAdd.get(i).getReceiver()))
                            toReAdd.get(i).setReceiver(receiver);
                        toReAdd.get(i).setSender(receiver);                        
                        sdb.getMessages().get(sdb.getMessages().size()-1).addInsideMessage(toReAdd.get(i));
                        System.out.println("Readded "+toReAdd.get(i).getMethod().getName() + " changed sender to: " + receiver);
		}
		
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void newLifelineEvent(Annotation a) {
		System.out.println("This is a NewLifelineEvent : "+a.annotationType().getSimpleName());
		String target = lastSD;
		String type = getFromAnnotation(a,"type").toString();
		String name = getFromAnnotation(a,"name").toString();
		if (name.equals(""))
			name = type.toLowerCase();
		SequenceDiagramBuilder sdb=factory.getSequenceDiagram(target);
		addLifeline(sdb, name, type);
		
	}
	public void lifelineEvent(Annotation a) {
		System.out.println("This is a LifelineEvent : "+a.annotationType().getSimpleName());
		String target=getFromAnnotation(a,"sd_id").toString();
		target=target.equals("")?lastSD:target;
		String type;
		String name;
		String regex = "(new[ ]*)(\\w*)\\((.*)\\)";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(bodySource.trim());
		if (matcher.matches()) {
			System.out.println(matcher.group(1));
			System.out.println(matcher.group(2));
			System.out.println(matcher.group(3));
			type = matcher.group(2);
			name = getFromAnnotation(a,"name").toString();
			name = name.equals("")?type.toLowerCase():name;
			SequenceDiagramBuilder sdb=factory.getSequenceDiagram(target);
			addLifeline(sdb, name, type);
		}
	}
	public void addLifeline(SequenceDiagramBuilder sdb, String name, String c) {
		try {
			Method m;
			Class<?> clazz=sdb.getClass();
			Object obj=sdb;
			m=clazz.getMethod("addLifeline",new Class[]{String.class, String.class});
			m.invoke(obj,new Object[]{name, c});			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void addMessage(SequenceDiagramBuilder sdb, MessageInfo mess) {
		try {
			String sender = mess.getSenderName();
			String receiver = mess.getReceiverName();
			Class senderClass = mess.getSenderClass();
			Class receiverClass = mess.getReceiverClass();
			Method m;
			Class<?> clazz=sdb.getClass();
			Object obj=sdb;
			if (this.messageStack.size() > 1)		//se qui si può usare lifelineSTack (secondo me si può) posso eliminare messageStack mi sa...
				m=clazz.getMethod("addInsideMessage",new Class[]{String.class, Class.class, String.class, Class.class, Method.class, boolean.class, Object[].class});
			else
				m=clazz.getMethod("addMessage",new Class[]{String.class, Class.class, String.class, Class.class, Method.class, boolean.class, Object[].class});
			m.invoke(obj,new Object[]{sender, senderClass, receiver, receiverClass, mess.getMethod(), mess.getAsync(), mess.getParams()});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void sendResponse(Annotation a) {
		System.out.println("Sending response message");
		String target=getFromAnnotation(a,"sd_id").toString();
		target=target.equals("")?lastSD:target;
		this.lifelineStack.get(target).pop();
		this.messageStack.pop();
	}
	
	public String[] parseMessage(String mex) {
		System.out.println("try parsing "+mex);
		String objName = mex.trim().split("\\.")[0];
		if (objName.contains("="))
			objName = objName.split("=")[1].trim();
		String[] res = {objName, mex.trim().split("\\.")[1].split("\\(")[0]};
		return res;
	}
	
	public Object getFromAnnotation(Annotation a,String prop){
		Object ret=null;
		try{
			Class<?> clazz=a.getClass();
			Method m=clazz.getMethod(prop,new Class[]{});
			ret= m.invoke(a,new Object[]{});
			System.out.println(ret);
		}catch(Exception e){
			System.err.println(prop+" does not exists!");
			e.printStackTrace();
		}
		return ret;
	}
	
	public static void main(String[] args){
		ConfigurationManager cm = new ConfigurationManager("conf.xml");
		CodeToSequenceDiagram ctsd=new CodeToSequenceDiagram(cm.getSDConfig(),cm.getGeneralConfig());
		if(args.length>0){
			ctsd.invokeMain(args[0],Util.copyOfRange(args,1,args.length));
		}
		ctsd.build("ClassDiagrams.java");
	}
	
	public void invokeMain(String mainClass,String[] args){
		try{
			Class<?> c=getClass(mainClass);
			Method main=c.getMethod("main",String[].class);
			System.out.println(mainClass+" "+args[0]+" "+args.getClass()+" "+main.getParameterTypes()[0]);
			main.invoke(null,new Object[]{args});
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private Class<?> getClass(String className) {
            Class<?> c;
            try {
                c = classLoader.loadClass(className);
            }catch (ClassNotFoundException e) {
                throw new RuntimeException("Class Not Found : " + e.getMessage());
            }
            return c;
	}
}