package sequencediagram;

import java.lang.reflect.*;
import java.lang.annotation.*;
import annotations.sequencediagram.*;
import util.*;
import org.aspectj.lang.*;
import ataspectj.reflect.*;
import atjava.lang.annotation.*;

public aspect SequenceDiagramAspect{
	
	private static CodeToSequenceDiagram LISTENER;
	/*
	public pointcut sd(): (call(* *.atAspectjNewMethod*SequenceDiagram*(..))) ;
	public pointcut lifeline():  (call(* *.atAspectjNewMethod*Lifeline*(..)))  ;
	public pointcut message(): (call(* *.atAspectjNewMethod*Message*(..)))  ;
        */
        public pointcut sd(): @block(SequenceDiagram) ;
        public pointcut lifeline():  @block(Lifeline)  ;
        public pointcut message(): @block(Message)  ;
        
        public pointcut lifelineE(): @expr(Lifeline);
        public pointcut messageE(): @expr(Message);
        public pointcut sdE(): @expr(SequenceDiagram);
        /*
	public pointcut lifelineE(String ataspectj):
		call(* wrapperExpressionAnnotation(..)) &&
		args(ataspectj,..) &&
		if(ataspectj.contains("-@Lifeline("));

	public pointcut messageE(String ataspectj):
		call(* wrapperExpressionAnnotation(..)) &&
		args(ataspectj,..) &&
		if(ataspectj.contains("-@Message("));
	
	public pointcut sdE(String ataspectj):
		call(* wrapperExpressionAnnotation(..)) &&
		args(ataspectj,..) &&
		if(ataspectj.contains("-@SequenceDiagram("));
	*/	
	public pointcut catcher(): call(* *.*(..)) && !call(* wrapperExpressionAnnotation(..)) && !within(SequenceDiagramAspect);
	
	public static void setContext(CodeToSequenceDiagram listener){
		LISTENER=listener;
		System.out.println("Listener setted");
	}
	
	private Object[] extractAnnotationAndBlock(JoinPoint jp){
		BlockJoinPointStaticPart bjp=(BlockJoinPointStaticPart) JoinPointFactory.getJoinPointStaticPart(jp);		
		String annName;
		AnnotatedBlock ab;
		Annotation ann;
		Method getAB,getAnnName;
		Method callBack;
		try{
			getAB=BlockJoinPointStaticPart.class.getDeclaredMethod("getAnnotatedBlock",new Class[]{});
			getAB.setAccessible(true);
			getAnnName=BlockJoinPointStaticPart.class.getDeclaredMethod("getAnnotationName",new Class[]{});
			getAnnName.setAccessible(true);
			annName=(String)getAnnName.invoke(bjp,new Object[]{});
			annName=annName.substring(annName.lastIndexOf('.')+1);	
			ab=(AnnotatedBlock)getAB.invoke(bjp,new Object[]{});
			ann=ab.getAnnotation();
			return new Object[]{ann,ab,annName};
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;

	}
	
	before(): catcher() {
		Object [] args=thisJoinPoint.getArgs();
		if (thisJoinPoint.getTarget() != null && thisJoinPoint.getSignature() != null)
			LISTENER.addMethodSignature(thisJoinPoint.getTarget().getClass(), thisJoinPoint.getSignature().toString(), args);
		else if (thisJoinPoint.getSignature() != null && thisJoinPoint.getThis() != null) {
			LISTENER.addMethodSignature(thisJoinPoint.getThis().getClass(), thisJoinPoint.getSignature().toString(), args);
		}
   }
	
	before(): message() {
		Object[] extract=extractAnnotationAndBlock(thisJoinPoint);
		Annotation ann;
		AnnotatedBlock ab;
		String annName;
		System.out.println("message");
		if(extract!=null){
		    	System.out.println("not null");
		    	Object [] args=thisJoinPoint.getArgs();
		    	/*
		    	for (Object o : args)         							//devo prendere fino al penultimo perchè l'ultimo è l'oggetto su cui viene chiamato il metodo... posso usarlo ?
				System.out.println("----------------------> "+o.getClass());
                        */
			ann=(Annotation)extract[0];
			ab=(AnnotatedBlock)extract[1];
			annName=(String)extract[2];
			System.out.println("Begin of message");
			LISTENER.setInstance(thisJoinPoint.getThis());
			LISTENER.blockAnnotationEvent(ann,ab, args);
			try {
				System.out.println(ann);
			} catch (Exception e) {
			
			}
		}
	}
	
	after(): message() {
                Object[] extract=extractAnnotationAndBlock(thisJoinPoint);
		Annotation ann;
		AnnotatedBlock ab;
		String annName;
		System.out.println("End of message");
		Object [] args=thisJoinPoint.getArgs();
		System.out.println("after: ");
		ann=(Annotation)extract[0];
		ab=(AnnotatedBlock)extract[1];
		LISTENER.setInstance(thisJoinPoint.getThis());
		LISTENER.sendResponse(ann);
        }
	
	void around(): sd() {
		Object[] extract=extractAnnotationAndBlock(thisJoinPoint);
		Annotation ann;
		AnnotatedBlock ab;
		String annName;
		if(extract!=null){
			ann=(Annotation)extract[0];
			ab=(AnnotatedBlock)extract[1];
			annName=(String)extract[2];
			LISTENER.setInstance(thisJoinPoint.getThis());
			LISTENER.sequenceDiagramEvent(ann);
			//System.out.println("Begin of sequence diagram");
			try {
				System.out.println(ann);
			} catch (Exception e) {
			
			}
			proceed();
			System.out.println("End of Sequence diagram");			
		}
	}
	before(): lifeline() {             //to check!
		Object[] extract=extractAnnotationAndBlock(thisJoinPoint);
		System.out.println("LIFELINE!!!!");
		Annotation ann;
		AnnotatedBlock ab;
		String annName;
		if(extract!=null){
			ann=(Annotation)extract[0];
			ab=(AnnotatedBlock)extract[1];
			annName=(String)extract[2];

			LISTENER.setInstance(thisJoinPoint.getThis());
			if(annName.equalsIgnoreCase("sequencediagram"))
				LISTENER.sequenceDiagramEvent(ann);
		}
	}
	
	after(): messageE() {
		ExprJoinPointStaticPart ejp=(ExprJoinPointStaticPart) JoinPointFactory.getJoinPointStaticPart(thisJoinPoint);
		String annName;
                AnnotatedExpression ae;
                Annotation ann;
                Method getAE,getAnnName;
                Method callBack;
                //System.out.println("After Sequence EXPR ANNOTATION I must be a Message!!");
		//System.out.println("EXPR ANNOTATION");
                try{
                        getAE=ExprJoinPointStaticPart.class.getDeclaredMethod("getAnnotatedExpression",new Class[]{});
                        getAE.setAccessible(true);
                        getAnnName=ExprJoinPointStaticPart.class.getDeclaredMethod("getAnnotationName",new Class[]{});
                        getAnnName.setAccessible(true);
                        annName=(String)getAnnName.invoke(ejp,new Object[]{});
                        annName=annName.substring(annName.lastIndexOf('.')+1);
			//System.out.println(ejp.getSourceLocation().getWithinType()+" "+thisJoinPoint.getThis().getClass());
			//System.out.println("Annotation name: "+annName);
                        ae=(AnnotatedExpression)getAE.invoke(ejp,new Object[]{});
			//System.out.println(ae);
						ann=ae.getAnnotation();
			//System.out.println(ann);
			//System.out.println("Begin of message");
			LISTENER.setInstance(thisJoinPoint.getThis());

                        if (annName.equals("Message"))			//devo metterlo... qualcosa non funziona con le expr...
                            LISTENER.sendResponse(ann);
                }catch(Exception e){
                        e.printStackTrace();
                }
	}	
	
	before():  messageE() || lifelineE() || sdE() {
		ExprJoinPointStaticPart ejp=(ExprJoinPointStaticPart) JoinPointFactory.getJoinPointStaticPart(thisJoinPoint);
		JoinPoint jp = JoinPointFactory.getJoinPoint(thisJoinPoint);
		Object[] args = thisJoinPoint.getArgs();
		//for (Object o : args)
			//System.out.println("Argomento: " + o.toString());
		String annName;
                AnnotatedExpression ae;
                Annotation ann;
                Method getAE,getAnnName;
                Method callBack;
                System.out.println("Sequence EXPR ANNOTATION");
		System.out.println("EXPR ANNOTATION");
                try{
                        getAE=ExprJoinPointStaticPart.class.getDeclaredMethod("getAnnotatedExpression",new Class[]{});
                        getAE.setAccessible(true);
                        getAnnName=ExprJoinPointStaticPart.class.getDeclaredMethod("getAnnotationName",new Class[]{});
                        getAnnName.setAccessible(true);
                        annName=(String)getAnnName.invoke(ejp,new Object[]{});
                        annName=annName.substring(annName.lastIndexOf('.')+1);
			//System.out.println(ejp.getSourceLocation().getWithinType()+" "+thisJoinPoint.getThis().getClass());		//nullpointer con annotazione lifeline?!
			System.out.println("Annotation name: "+annName);
                        ae=(AnnotatedExpression)getAE.invoke(ejp,new Object[]{});
			System.out.println(ae);
                        ann=ae.getAnnotation();
			System.out.println(ann);
			
			LISTENER.setInstance(thisJoinPoint.getThis());

                        if(annName.equalsIgnoreCase("sequencediagram"))
                                LISTENER.sequenceDiagramEvent(ann);
                        else
                                LISTENER.exprAnnotationEvent(ann,ae,args[1]);
                }catch(Exception e){
                        e.printStackTrace();
                }
	
	}
}
