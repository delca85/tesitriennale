import annotations.sequencediagram.*;

public class Client {

        public void getResource(Server server, String name, String pwd, String resourceID) {
		String res = "";
		String cookie = @Message(name="askAuthentication") {server.requestAuthentication(name, pwd)};
		res = @Message(name="askResource") { server.requestResource(cookie, resourceID)};
		System.out.println(res); 
	}
}
