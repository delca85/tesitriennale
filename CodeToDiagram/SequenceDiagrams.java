import ramses.reification.*; 
import ramses.reification.PrimitiveType.Type;
import ramses.reification.sequencediagram.*;
import ramses.reification.classdiagram.*;
import ramses.reification.classdiagram.Class; 
import ramses.reification.exception.InterfaceDoesNotExistException;
import java.util.Stack;
public class SequenceDiagrams{
private static String blank="empty.emx";
private static Reification r;
private static ClassDiagram c;
private static Class cl;
private static Interface iface;
private static UMLType[] types;
private static UMLType ret;
private static String[] names;
private static UMLType[] exceptions;
private static SequenceDiagram sd;
private static SequenceMessage sdm;
private static LifeLine sdl;
private static Stack<SequenceMessage> messageStack = new Stack<SequenceMessage>();
public static void main(String[] args) throws Exception{
r=new Reification(blank);
r=new Reification(blank);
c=r.addClassDiagram("Vista2");
cl=getClass("AuthenticationManager","public",false,c);
ret=new PrimitiveType(Type.BOOLEAN);
types=new UMLType[2];
names=new String[2];
exceptions=null;
names[0]="arg0";
names[1]="arg1";
types[0]=getClass("String","public",false,c);
types[1]=getClass("String","public",false,c);
exceptions=new UMLType[0];
cl.addMethod("checkCredentials",1,types,names,ret, exceptions);
cl=getClass("Client","public",false,c);
ret=getClass("void","public",false,c);
types=new UMLType[4];
names=new String[4];
exceptions=null;
names[0]="arg0";
names[1]="arg1";
names[2]="arg2";
names[3]="arg3";
types[0]=getClass("Server","public",false,c);
types[1]=getClass("String","public",false,c);
types[2]=getClass("String","public",false,c);
types[3]=getClass("String","public",false,c);
exceptions=new UMLType[0];
cl.addMethod("getResource",1,types,names,ret, exceptions);
cl=getClass("Server","public",false,c);
cl.addAnAttribute("clientAuth","",getClass("String","public",false,c),true);
cl.addAnAttribute("name","",getClass("String","public",false,c),true);
ret=getClass("void","public",false,c);
types=new UMLType[1];
names=new String[1];
exceptions=null;
names[0]="arg0";
types[0]=getClass("String","public",false,c);
exceptions=new UMLType[0];
cl.addMethod("addAuthenticatedClient",1,types,names,ret, exceptions);
ret=getClass("String","public",false,c);
types=new UMLType[2];
names=new String[2];
exceptions=null;
names[0]="arg0";
names[1]="arg1";
types[0]=getClass("String","public",false,c);
types[1]=getClass("String","public",false,c);
exceptions=new UMLType[0];
cl.addMethod("requestResource",1,types,names,ret, exceptions);
ret=getClass("String","public",false,c);
types=new UMLType[2];
names=new String[2];
exceptions=null;
names[0]="arg0";
names[1]="arg1";
types[0]=getClass("String","public",false,c);
types[1]=getClass("String","public",false,c);
exceptions=new UMLType[0];
cl.addMethod("requestAuthentication",1,types,names,ret, exceptions);
sd=r.addSequenceDiagram("Client/Server2");
cl=c.getClass("Client");
sdl=sd.addNewLifeLine("client",cl);
cl=c.getClass("Server");
sdl=sd.addNewLifeLine("server",cl);
cl=c.getClass("Server");
types = new UMLType[2];
types[0]=getClass("String", "public", false, c);
types[1]=getClass("String", "public", false, c);
sdm=sd.addSynchronousMessage(sd.getLifeLine("client",c.getClass("Client")),sd.getLifeLine("server",cl), getMethod("requestAuthentication",cl,types), "UAL", new String[]{"User","BadPass","cookieBadPassUser"});
messageStack.push(sdm);
messageStack.pop();
cl=c.getClass("Server");
types = new UMLType[2];
types[0]=getClass("String", "public", false, c);
types[1]=getClass("String", "public", false, c);
sdm=sd.addSynchronousMessage(sd.getLifeLine("client",c.getClass("Client")),sd.getLifeLine("server",cl), getMethod("requestResource",cl,types), "UAL", new String[]{"cookieBadPassUser","2424234","Client not authenticated"});
messageStack.push(sdm);
messageStack.pop();
sd=r.addSequenceDiagram("Client/Server");
cl=c.getClass("Client");
sdl=sd.addNewLifeLine("client",cl);
cl=c.getClass("Server");
sdl=sd.addNewLifeLine("server",cl);
cl=c.getClass("Server");
types = new UMLType[2];
types[0]=getClass("String", "public", false, c);
types[1]=getClass("String", "public", false, c);
sdm=sd.addSynchronousMessage(sd.getLifeLine("client",c.getClass("Client")),sd.getLifeLine("server",cl), getMethod("requestAuthentication",cl,types), "UAL", new String[]{"User","GoodPass","cookieGoodPassUser"});
messageStack.push(sdm);
messageStack.pop();
cl=c.getClass("Server");
types = new UMLType[2];
types[0]=getClass("String", "public", false, c);
types[1]=getClass("String", "public", false, c);
sdm=sd.addSynchronousMessage(sd.getLifeLine("client",c.getClass("Client")),sd.getLifeLine("server",cl), getMethod("requestResource",cl,types), "UAL", new String[]{"cookieGoodPassUser","2424234","RES: 2424234"});
messageStack.push(sdm);
messageStack.pop();
r.save("SequenceDiagrams.emx");
}
public static Class getClass(String name,String modifiers,boolean abs,ClassDiagram cd){
Class ret;
 return (ret=cd.getClass(name))!=null?ret:cd.createNewClass(name, modifiers, abs);
}
public static Interface getInterface(String name,String modifiers,ClassDiagram cd){
Interface ret;
 try {return cd.getInterface(name);} catch (InterfaceDoesNotExistException e) {return cd.createNewInterface(name, modifiers, false);}
}
public static Method getMethod(String name,Class clazz,UMLType[] par){
	Method m = null;
int c;
	for (Method me : clazz.getAllMethods()) {
		if (me.getAllInParameters().length == par.length && me.getName().equals(name)) {
			for (c = 0; c < par.length; c++) {
				if (!me.getAllInParameters()[c].getType().equals(par[c]))
				break;
			}
			if (c == par.length) {
				m = me;
break;
			}
		}
	}
	return m;
}
}