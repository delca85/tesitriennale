import ramses.reification.*; 
import ramses.reification.sequencediagram.*;
import ramses.reification.classdiagram.*;
import ramses.reification.PrimitiveType.Type;
import ramses.reification.activitydiagram.*;
import ramses.reification.classdiagram.Class; 
import ramses.reification.exception.InterfaceDoesNotExistException;
public class ActivityDiagrams{
private static String blank="empty.emx";
private static Reification r;
private static ClassDiagram c;
private static Class cl;
private static Interface iface;
private static UMLType[] types;
private static UMLType ret;
private static String[] names;
private static ActivityDiagram ad;
private static ActivitySimpleElement ase;
private static ActivityElement ae;
private static GroupActivityNode gan;
private static UMLType pin;
public static void main(String[] args) throws Exception{
r=new Reification(blank);
r=new Reification(blank);
c=r.addClassDiagram("Vista2");
cl=getClass("ProvaIf","public",false,c);
cl.addAnAttribute("i_DAE_IC_0","",getClass("DAE_IC_0","public",false,c));
ret=getClass("void","public",false,c);
types=new UMLType[1];
names=new String[1];
names[0]="arg0";
types[0]=getClass("[Ljava.lang.String;","public",false,c);
cl.addMethod("main","public",types,names,ret);
ret=getClass("void","public",false,c);
types=new UMLType[2];
names=new String[2];
names[0]="arg0";
names[1]="arg1";
types[0]=new PrimitiveType(Type.BOOLEAN);
types[1]=new PrimitiveType(Type.BOOLEAN);
cl.addMethod("a","public",types,names,ret);
ad=r.addActivityDiagram("1");
ret=getClass("boolean", "public",false, c);
ae=ad.addActivityAttribute("t2", ret, "public");
ret=getClass("boolean", "public",false, c);
ae=ad.addActivityAttribute("t", ret, "public");
ae=ad.addCallAction("ipost","UAL","\n\n\t\t\t");
ae=ad.addCallAction("post","UAL","\n\t\t\tSystem.out.println(\"post\");\n\t\t\t//lista.add(\"exe\");\n\t\t");
ase=ad.addDecisionActivityNode("iif");
ase=ad.addDecisionActivityNode("if");
ase=ad.addMergeActivityNode("ifmerge");
ase=ad.addMergeActivityNode("iifmerge");
ae=ad.addCallAction("itrue","UAL","\n\t\t\t\t\tSystem.out.println(\"itrue\");\n\t\t\t\t    ");
ae=ad.addCallAction("false","UAL","\n\t\t\t    System.out.println(\"false\");\n\t\t\t");
ae=ad.addCallAction("ifalse","UAL","\n\t\t\t\t\tSystem.out.println(\"ifalse\");\n\t\t\t\t    ");
ase=getElement("ipost",ad);
ase.addControlFlowTo(getElement("ifmerge",ad),"","","","");
//start buildflow for element iif
/*buildflow1*/ase=getElement("iif",ad);
/*buildflow2*/ase.addControlFlowTo(getElement("itrue",ad),"true","UAL","t2","");
/*buildflow2*/ase.addControlFlowTo(getElement("ifalse",ad),"false","UAL","!(t2)","");
//end buildflow for element iif
//start buildflow for element if
/*buildflow1*/ase=getElement("if",ad);
/*buildflow2*/ase.addControlFlowTo(getElement("iif",ad),"true","UAL","t","");
/*buildflow2*/ase.addControlFlowTo(getElement("false",ad),"false","UAL","!(t)","");
//end buildflow for element if
//start buildflow for element ifmerge
/*buildflow1*/ase=getElement("ifmerge",ad);
/*buildflow2*/ase.addControlFlowTo(getElement("post",ad),"","UAL","","");
//end buildflow for element ifmerge
//start buildflow for element iifmerge
/*buildflow1*/ase=getElement("iifmerge",ad);
/*buildflow2*/ase.addControlFlowTo(getElement("ipost",ad),"","UAL","","");
//end buildflow for element iifmerge
ase=getElement("itrue",ad);
ase.addControlFlowTo(getElement("iifmerge",ad),"","","","");
ase=getElement("false",ad);
ase.addControlFlowTo(getElement("ifmerge",ad),"","","","");
ase=getElement("ifalse",ad);
ase.addControlFlowTo(getElement("iifmerge",ad),"","","","");
r.save("ActivityDiagrams.emx");
}
public static Class getClass(String name,String modifiers,boolean abs,ClassDiagram cd){
Class ret;
 return (ret=cd.getClass(name))!=null?ret:cd.createNewClass(name, modifiers, abs);
}
public static ActivitySimpleElement getElement(String name,ActivityDiagram ad) {ActivitySimpleElement ret=null;if((ret=ad.getAcceptEventAction(name))!=null) return ret;if((ret=ad.getCallAction(name))!=null) return ret;if((ret=ad.getCallBehavior(name))!=null) return ret;if((ret=ad.getActivityPartition(name))!=null) return ret;if((ret=ad.getConditionalActivityNode(name))!=null) return ret;if((ret=ad.getDecisionActivityNode(name))!=null) return ret;if((ret=ad.getFinalActivityNode(name))!=null) return ret;if((ret=ad.getForkActivityNode(name))!=null) return ret;if((ret=ad.getInitialActivityNode(name))!=null) return ret;if((ret=ad.getJoinActivityNode(name))!=null) return ret;if((ret=ad.getLoopActivityNode(name))!=null) return ret;if((ret=ad.getMergeActivityNode(name))!=null) return ret;if((ret=ad.getSendSignalAction(name))!=null) return ret;if((ret=ad.getStructuredActivityNode(name))!=null) return ret;return ret;}public static ActivitySimpleElement getElement(String name,GroupActivityNode ad) {ActivitySimpleElement ret=null;if((ret=ad.getAcceptEventAction(name))!=null) return ret;if((ret=ad.getCallAction(name))!=null) return ret;if((ret=ad.getCallBehavior(name))!=null) return ret;if((ret=ad.getConditionalActivityNode(name))!=null) return ret;if((ret=ad.getDecisionActivityNode(name))!=null) return ret;if((ret=ad.getFinalActivityNode(name))!=null) return ret;if((ret=ad.getForkActivityNode(name))!=null) return ret;if((ret=ad.getInitialActivityNode(name))!=null) return ret;if((ret=ad.getJoinActivityNode(name))!=null) return ret;if((ret=ad.getLoopActivityNode(name))!=null) return ret;if((ret=ad.getMergeActivityNode(name))!=null) return ret;if((ret=ad.getSendSignalAction(name))!=null) return ret;if((ret=ad.getStructuredActivityNode(name))!=null) return ret;return ret;}}