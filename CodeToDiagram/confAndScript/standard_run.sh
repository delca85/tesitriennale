#!/bin/bash
cd src/
CLASSPATH=$CLASSPATH:.:../../ataspectj/build/jar/ataspectjc.jar

BCEL=/usr/share/apache-ant/lib/bcel-5.2.jar #path to bcel
ASPECTJ=/path/to/aspectj1.7/lib/aspectjrt.jar

BOOTCP=/path/to/reverserRSA/ModelBuilder/bin:.:/path/to/atjava/build/classes:$BCEL:../commons-logging-1.1.2.jar:../commons-configuration-1.9.jar:../commons-lang-2.6.jar:../:../src:/path/to/ataspectj/build/classes:$ASPECTJ

#CLASSPATH=$CLASSPATH:$BOOTCP
java -Xbootclasspath/p:$BOOTCP $*
