#!/bin/bash
cd src/
CLASSPATH=$CLASSPATH:.:/path/to/ataspectj/build/jar/ataspectj.jar

BCEL=/usr/share/apache-ant/lib/bcel-5.2.jar  #path to bcel
ASPECTJ=/path/to/aspectj1.7/lib/aspectjrt.jar
#JUNIT=/path/to/reverser/lib/junit.jar

BOOTCP=/path/to/reverserRSA/ModelBuilder/bin:.:/path/to/atjava/build/classes:$BCEL:../commons-logging-1.1.2.jar:../commons-configuration-1.9.jar:../commons-lang-2.6.jar:..:/path/to/ataspectj/build/classes:$ASPECTJ:$JUNIT

CLASSPATH=$CLASSPATH:$BOOTCP
java -Xbootclasspath/p:$BOOTCP ataspectj.AtAspectjMain -verbose -debug -Xbootclasspath/p:$BOOTCP $*