package exporter;

public interface Exporter {

	public void exportToFile(String name);

}