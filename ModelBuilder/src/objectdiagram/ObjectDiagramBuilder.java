package objectdiagram;

import java.util.*;
import java.lang.reflect.Field;
import java.lang.annotation.*;

import util.BuilderArrayList;
import util.BuilderHashMap;
import util.DeepClone;
import builder.Builder;
import builder.FlowBuilder;


public abstract class ObjectDiagramBuilder implements Builder {
	protected String name;
	protected BuilderHashMap<String, Instance> instances;
	protected BuilderHashMap<String, BuilderArrayList<Link>> links;		//chiavi = nomi delle istanze sorgente, oggetti = array di link che hanno la chiave come sorgente
	protected BuilderHashMap<String, ObjectDiagramBuilder> breakpoints; //chiavi=nomi bp, oggetti breakpoint
	protected HashMap<String, ArrayList<String>> slots;	//chiavi = nomi delle classi, oggetti=nome dei campi che voglio rappresentare nel diagramma
	protected HashMap<String, String> instancesNameObject;	//chiavi = nomi istanze, oggetti = oggetti link a istanze associate
	
	private int breakpointCounter;		//suffisso da aggiungere per creare nome di ogni breakpoint, viene incrementato ogni volta che si incontra un nuovo bp
	private HashMap<String, Integer> idCounter;		//chiavi = classi, oggetti = #istanze create finora
	private HashMap<String, ArrayList<LinkInfo>> tempSrcLinks;	//chiavi = istanze non ancora create, oggetti = informazioni di link di cui le chiavi sono sorgente
	private HashMap<String, ArrayList<LinkInfo>> tempDestLinks;	//chiavi = istanze non ancora create, oggetti = informazioni di link di cui le chiavi sono destinazione
	private HashMap<String, String> tempInstances;	//chiavi=objectID, oggetti=nomi instance associate
	private ArrayList<String>classes;		//array contenente le classi con singleInstance=true, di cui ci sia già un'istanza nel diagramma

	public ObjectDiagramBuilder(String name){
		this.name = name;
		instances = new BuilderHashMap<String, Instance>();
		links = new BuilderHashMap<String, BuilderArrayList<Link>>();
		breakpoints = new BuilderHashMap<String, ObjectDiagramBuilder>();
		slots = new HashMap<String, ArrayList<String>>();
		
		idCounter = new HashMap<String, Integer>();
		tempSrcLinks = new HashMap<String, ArrayList<LinkInfo>>();
		tempDestLinks = new HashMap<String, ArrayList<LinkInfo>>();
		tempInstances = new HashMap<String, String>();
		instancesNameObject = new HashMap<String, String>();
		breakpointCounter=0;
		classes= new ArrayList<String>();
	}

	public String getName(){
		return name;
	}
	
	public BuilderHashMap<String, Instance> getInstancesHashMap(){
		return instances;
	}

	public BuilderHashMap<String, BuilderArrayList<Link>> getLinks(){
		return links;
	}

	public Instance getInstance(Object o){
		String oid = getObjectID(o);
		Instance in = instances.get(oid);
		if (in != null)
			return in;
		throw new InstanceNotFoundException("Instance not found. Object: " + o);
	}

	public Instance getInstanceFromName(String name) {
		Instance in = instances.get(instancesNameObject.get(name));
		if (in != null)
			return in;
		throw new InstanceNotFoundException("Instance not found: " + name);
	}

	public ArrayList<Instance> getInstances(Object o) {		//restituisce le istanze linkative ad una collezione di oggetti
		if (! (o instanceof Collection) )
			throw new IllegalArgumentException("Object passed as argument has to be a collection");		
		String oid;
		Instance in;
		ArrayList<Instance> ins = new ArrayList<Instance>();
		for (Object obj : ((Collection) o)) {
			oid = getObjectID(obj);
			in = instances.get(oid);
			if (in != null)
				ins.add(in);
		}
		return ins;
	}

	public void addInstance(Instance in){
		String className = in.getClassName();
		if (!classes.contains(className)) {		//se singleInstance=true, devo controllare che non ci sia già un'istanza di quella classe 
			String inName, oid;
			inName = in.getName();
			oid = getObjectID(in.getObject());
			if(!instances.containsKey(oid)){
				instances.put(oid, in);
				instancesNameObject.put(inName, oid);			//mantengo info di link tra nome stringa e obj
				if (in.getSingleInstance())
					classes.add(className);
				System.out.println("[OBJECTDIAGRAMBUILDER]New Instance added, name: " + inName);
			}		
			if (getTempInstance(in.getObject()) != null)		//se l'istanza creata, era tra quelle temporanee, devo creare ed aggiungere le link che la coinvolgono
				addLinksFromTemp(in.getObject(), in.getName(), in.getSingleInstance());
		}		
	}

	public void addSlots(Class clazz, ArrayList<String> slotsToBeAdded) {
		String className = clazz.getName();
		for (String f : slotsToBeAdded)
			System.out.println("[OBJECTDIAGRAMBUILDER]New Slot added for class " + className + ", name: " + f);
		slots.put(className, slotsToBeAdded);
	}

	public void addLink(Link link) {
		String src = link.getSrc();
		if (! links.containsKey(src)){						//se non è ancora stata inserita alcuna link che abbia come sorgente src, devo creare array
			BuilderArrayList<Link> linkArr = new BuilderArrayList<Link>();
			links.put(src, linkArr);
		}
		if (!links.get(src).contains(link)) {				//se per l'istanza sorgente non è inserita la link che voglio aggiungere in questo momento, la aggiungo
			links.get(src).add(link);
			System.out.println("[OBJECTDIAGRAMBUILDER]New Link added, src: " + link.getSrc() + " dest: " + link.getDest() + " label: " + link.getLabel());
		}
	}
	
	public boolean rmLink(Link link) {
		String src = link.getSrc();
		if (links.containsKey(src))
			return links.get(src).remove(link);
		return false;
	}
	
	public String getInstanceName(Object o, String name, boolean singleInstance) {
		String className, result=name;
		className = getClassName(o);
		result = name;
		if (getTempInstance(o) == null) {		//se non è un'istanza memorizzata come temporanea, creo istanza secondo il regolare procedimento di costruzione
			//se ho ricevuto come name una stringa vuota o il nome della classe, aggiungo contatore per evitare che un nome vuoto individui un'Instance, o lo stesso nome ne identifichi più di una
			if (name.equals("") || name.equals(className) || (instancesNameObject.keySet().contains(name) && ! instancesNameObject.get(name).equals(getObjectID(o)))) {
				int counter = getCounter(className);
				result = className + "_" + (counter);
				setCounter(className);
			}
		}
		else {		//se è un'istanza presente tra quelle temporanee
			if (name.equals("") || name.equals(className) || (instancesNameObject.keySet().contains(name) && ! instancesNameObject.get(name).equals(getObjectID(o))))	//se nome non definito in fase di annotazione, recupero quello associato all'istanza temporanea
				result = getTempInstance(o);
		}		
		return result;
	}

	public void endEvent(){
		if (breakpoints.size() > 0)
			for (String bp : breakpoints.keySet()){
				System.out.println("[OBJECTDIAGRAMBUILDER]Breakpoint, name: " + bp);
				System.out.println(breakpoints.get(bp));
			}
		System.out.println("[OBJECTDIAGRAMBUILDER]ObjectDiagram, name: " + name);
		for (Instance in : instances.values())		
			addSlots(in);
		System.out.println(this.toString());
	}

	public boolean equals(Object other){
		if (other instanceof ObjectDiagramBuilder){
			ObjectDiagramBuilder otherODB = (ObjectDiagramBuilder) other;
			//confronto degli insiemi generati dalle HashMap di instances e link per determinare uguaglianza tra odb. obj di links sono arrayList quindi vanno trattati diversamente
			return instances.entrySet().equals(otherODB.instances.entrySet()) && links.keySet().equals(otherODB.links.keySet()) && links.values().containsAll(otherODB.links.values()) && otherODB.links.values().containsAll(links.values());
		}
		return false;
	}

	public String toString(){
		StringBuffer sb=new StringBuffer();
		sb.append("instances(" + instances.size() + "):\n");
		for (Instance in : instances.values())
			sb.append(in.toString() + "\n");
		sb.append("links(" + linksNumber() + "):\n");
		for (ArrayList<Link> linkArr : links.values())
			for (Link link : linkArr)
				sb.append(link.toString() + "\n");
		return sb.toString();
	}

	public void breakpoint() {
		String name=this.name+"_"+breakpointCounter;
		ObjectDiagramBuilder clone = this.clone();
		clone.name = name;
		//inserisco i valori dei campi annotati nelle istanze del diagramma definito da questo breakpoint
		if (slots.size() != 0) {
			clone.slots=DeepClone.deepClone(slots);
			for (Instance in : clone.instances.values()){
				clone.addSlots(in);
				in.setODB(clone);
			}
			for (BuilderArrayList<Link> linkArr : clone.links.values())
				for (Link link : linkArr)
					link.setODB(clone);
		}
		breakpoints.put(name, clone);
		System.out.println("[OBJECTDIAGRAMBUILDER]New Breakpoint added, name: " + clone.getName());
		breakpointCounter++;
	}

	public abstract void addSlots(Instance in);

	public abstract void addLinksFromTemp(Object o, String name, boolean singleInstance);

	public abstract ObjectDiagramBuilder clone();

	protected String getInstanceNameForLink(String name, Object o) {
		String className, result=null;
		className = getClassName(o);
		if (containsInstance(o))		//se l'istanza è già presente nell'odb, recupero il nome precedentemente associatole
			result = getInstance(o).getName();
		else {
			if (getTempInstance(o) == null){
				if (!name.equals("") && !name.equals(className) && !(instancesNameObject.keySet().contains(name) && ! instancesNameObject.get(name).equals(getObjectID(o))) )		//se l'istanza non è tra quelle temporanee ed è stato indicato un nome in fase di annotazione
					addTempInstance(o, name);							//la aggiungo, associandole il nome indicato
				else {		//se non è stato indicato un nome significativo, lo genero
					addTempInstance(o, className + "_" + getCounter(className));
					setCounter(className);	
				}
			}
			result = getTempInstance(o);	//cmq result deve essere il nome associato a o nella map delle istanze temporanee
		}
		return result;
	}


	protected void addTempSrcLinks(String src, LinkInfo linkInfo) {
		if (tempSrcLinks.containsKey(src))													//se già ci sono linkInfo tmp aggiungo la mia
				tempSrcLinks.get(src).add(linkInfo);
		else {
			ArrayList<LinkInfo> linkInfoArray = new ArrayList<LinkInfo>();		//altrimenti creo nuovo arrayList e lo metto nella hashMap
			linkInfoArray.add(linkInfo);
			tempSrcLinks.put(src, linkInfoArray);
		}
	}

	protected void addTempDestLinks(String dest, LinkInfo linkInfo) {
		if (tempDestLinks.containsKey(dest))													//se già ci sono linkInfo tmp aggiungo la mia
				tempDestLinks.get(dest).add(linkInfo);
		else {
			ArrayList<LinkInfo> linkInfoArray = new ArrayList<LinkInfo>();		//altrimenti creo nuovo arrayList e lo metto nella hashMap
			linkInfoArray.add(linkInfo);
			tempDestLinks.put(dest, linkInfoArray);
		}
	}

	protected boolean containsInstanceFromName(String name) {
		return instancesNameObject.containsKey(name);
	}

	protected boolean containsInstance(Object o) {
		return instances.containsKey(getObjectID(o));
	}

	protected boolean contains(Instance in) {
		return instances.containsValue(in);
	}

	protected boolean containsSlotsClass(Class clazz) {
		return slots.containsKey(clazz.getName());
	}

	protected ArrayList<LinkInfo> getTempSrcLinks(Object src) {
		return tempSrcLinks.get(getTempInstance(src));
	}

	protected ArrayList<LinkInfo> getTempDestLinks(Object dest) {
		return tempDestLinks.get(getTempInstance(dest));
	}
	
	protected void rmTempSrcLinks(Object src) {
		tempSrcLinks.remove(getTempInstance(src));
	}

	protected void rmTempDestLinks(Object dest) {
		tempDestLinks.remove(getTempInstance(dest));
	}

	protected void rmTempInstance(Object o) {
		tempInstances.remove(getObjectID(o));
	}

	private int linksNumber() {
		int result=0;
		for (BuilderArrayList<Link> linkArr : links.values())
			result += linkArr.size();
		return result;
	}

	private void addTempInstance(Object o, String name) {
		if (!tempInstances.containsKey(getObjectID(o)))
			tempInstances.put(getObjectID(o), name);
	}

	private String getTempInstance(Object o) {
		return tempInstances.get(getObjectID(o));
	}

	private String getClassName(Object o) {
		return o.getClass().getName();
	}
	
	private String getObjectID(Object o) {
		return "" + o.hashCode();
	}

	private int getCounter(String className) {
		if (!idCounter.containsKey(className))
			return 0;
		return idCounter.get(className);
	}

	private void setCounter(String className) {
		if (!idCounter.containsKey(className))
			idCounter.put(className, 0);
		int counter = idCounter.get(className);
		idCounter.put(className, new Integer(counter+1));
	}
}
