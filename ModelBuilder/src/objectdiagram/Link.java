package objectdiagram;

import java.util.*;
import java.lang.reflect.*;

import util.BuilderArrayList;
import builder.Builder;
import builder.FlowBuilder;

public abstract class Link implements Builder {

	protected ObjectDiagramBuilder odb;
	protected String label;
	protected String src;		//mantengo i nomi delle istanze, anziché le istanze vere e proprie perché essi sono identificatori univoci delle istanze
	protected String dest;

	public Link(String label, String src, String dest, ObjectDiagramBuilder odb){
		if (odb.containsInstanceFromName(src) && odb.containsInstanceFromName(dest) ) {		//se sia src che dest sono istanze già presenti nell'od, creo la link
			this.odb = odb;
			this.label = label;
			this.src = src;
			this.dest = dest;
		}
		if (!odb.containsInstanceFromName(src) && !odb.containsInstanceFromName(dest)){		//se non sono presenti entrambe, significa che si tratta di un'istanza single_instance true, la cui classe è già rappresentata da un'altra istanza
			System.out.println("[LINK]Istanza che non verrà creata presente nella link, link non creata neanche in forma temporanea");
		}
		if (!odb.containsInstanceFromName(src) && odb.containsInstanceFromName(dest)) {
			System.out.println("[LINK]Instance src non presente, src: " + src + " dest: " + dest);
			LinkInfo linkInfo = new LinkInfo(label, dest, odb.getName());		//creo nuova istanza linkInfo relativa a questa link 
			odb.addTempSrcLinks(src, linkInfo);										//aggiungo una link temporanea che verrà resa reale e rimossa quando l'istanza relativa a src verrà creata
		}
		if (!odb.containsInstanceFromName(dest) && odb.containsInstanceFromName(src)) {
			System.out.println("[LINK]Instance dest non presente, dest: " + dest + " src: " + src);
			LinkInfo linkInfo = new LinkInfo(label, src, odb.getName());		//creo nuova istanza linkInfo relativa a questa link 
			odb.addTempDestLinks(dest, linkInfo);								//aggiungo una link temporanea che verrà resa reale e rimossa quando l'istanza relativa a dest verrà creata
		}
	}

	public String getODName(){
		if (odb == null)
			return null;
		return odb.getName();
	}
	public String getLabel(){
		return label;
	}
	public void setLabel(String label){
		this.label = label;
	}

	public String getSrc(){
		return src;
	}

	public String getDest(){
		return dest;
	}
	
	public void setSrc(String src){
		if (!odb.getInstancesHashMap().containsKey(src))
			throw new InstanceNotFoundException("Existent instances is required to create a link from it");		
       	this.src = src;
    }
        
    public void setDest(String dest){
        if (!odb.getInstancesHashMap().containsKey(dest))
			throw new InstanceNotFoundException("Existent instances is required to create a link to it");		
       	this.dest = dest;
    }

    public void setODB(ObjectDiagramBuilder odb){
    	this.odb = odb;
    }

    @Override
    public boolean equals(Object other) {
    	if (other instanceof Link) {
    		Link otherLink = (Link) other;
    		return getODName().equals(otherLink.getODName()) && getSrc().equals(otherLink.getSrc()) && getDest().equals(otherLink.getDest()) && getLabel().equals(otherLink.getLabel());
    	}
    	return false;
    }

    @Override
    public String toString(){
		StringBuffer sb=new StringBuffer();
		sb.append("name: " + label + " src: " + src + " dest: " + dest);
		return sb.toString();
	}

    public abstract Link clone();
}