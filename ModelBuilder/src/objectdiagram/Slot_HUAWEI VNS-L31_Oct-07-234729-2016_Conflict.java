package objectdiagram;

import java.lang.reflect.*;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;


import util.BuilderArrayList;
import builder.Builder;
import builder.FlowBuilder;

public abstract class Slot implements Builder{

	protected ObjectDiagramBuilder odb;
	protected Instance in;		//attribut per recuperare tutte le informazioni possibili, tramite reflection, sul campo
	protected String name;
	protected Object value;

	private static Map<String,Class> builtInMap = new HashMap<String,Class>();		//mappa per conversione dei tipi degli array da primitivi a wrapper
	static {
       builtInMap.put("int", Integer.class);
       builtInMap.put("long", Long.class );
       builtInMap.put("double", Double.class );
       builtInMap.put("float", Float.class );
       builtInMap.put("bool", Boolean.class );
       builtInMap.put("char", Character.class );
       builtInMap.put("byte", Byte.class );
       builtInMap.put("void", Void.class );
       builtInMap.put("short", Short.class );
	}

	public Slot(Instance in, String name, Object value, ObjectDiagramBuilder odb) {
		this.odb = odb;
		this.in = in;
		this.name = name;
		this.value = value;

	}	

	public boolean equals(Object other) {
		if (other instanceof Slot) {
			Slot otherF = (Slot) other;
			return name.equals(otherF.getName());
		}
		return false;
	}

	public Instance getInstance(){
		return in;
	}

	public String getName() {
		return name;
	}
	
	public Object getValue() {
		return value;
	}

	public List<String> getValueString(){
		List<String> result= new ArrayList<String>();
		Collection valueC=null;
		boolean isAnArray = value.getClass().isArray();
		if (value != null && isAnArray){	//se il campo è di tipo Array, devo modificarlo per usarlo come una collezione
			Class elementClazz = value.getClass().getComponentType();
			String type = elementClazz.getSimpleName();
			valueC = fromArrayToArrayList(value, type, elementClazz);
		}
		if (value != null && Collection.class.isAssignableFrom(value.getClass()) || isAnArray){
			if (!isAnArray)
				valueC = (Collection) value; 
			for (Object v : valueC){
				if (odb.containsInstance(v))
					result.add(odb.getInstance(v).getName());
				if (!odb.containsInstance(v))
					result.add(v.toString());
			}
		}
		if (!isAnArray && value != null && !Collection.class.isAssignableFrom(value.getClass())) {
			if (odb.containsInstance(value))		//se il valore del campo è un'istanza, recupero il nome
				result.add(odb.getInstance(value).getName());
			if (!odb.containsInstance(value))
				result.add(value.toString());
		}
		return result;
	}

	public void setName(String n) {
		name = n;
	}

	public void setValue(Object v) {
		value = v;
	}
	
	public abstract Slot clone();

	public String toString() {
		String result = null;
		boolean multiplicity =Collection.class.isAssignableFrom(value.getClass()) || value.getClass().isArray();
		if (!multiplicity && getValueString().size() != 0)
			result = getValueString().get(0);
		if (multiplicity)
			result = getValueString().toString();
		return name + ": " + result;
	}

	private <T> ArrayList<T> getModels(Class<T> type) {
        ArrayList<T> arrayList = new ArrayList<T>();
        return arrayList;
    }

    private <T> ArrayList<T> fromArrayToArrayList(Object value, String type, Class elementClazz){
		ArrayList<T> result = null;
		if (builtInMap.containsKey(type)){		//array di tipo primitivo
			Class clazzArr = builtInMap.get(type);
			result = getModels(clazzArr);
		}
		if (!builtInMap.containsKey(type))		//array di istanze
			result = getModels(elementClazz);
		if (type.equals("int")){
			for (int i = 0; i < ((int []) value).length; i++)
				((ArrayList<Integer>)result).add(((int []) value)[i]);
		}
		if (type.equals("long")){
			for (int i = 0; i < ((long []) value).length; i++)
				((ArrayList<Long>)result).add(((long []) value)[i]);
		}
		if (type.equals("double")){
			for (int i = 0; i < ((double []) value).length; i++)
				((ArrayList<Double>)result).add(((double []) value)[i]);
		}
		if (type.equals("float")){
			for (int i = 0; i < ((float []) value).length; i++)
				((ArrayList<Float>)result).add(((float []) value)[i]);
		}
		if (type.equals("boolean")){
			for (int i = 0; i < ((boolean []) value).length; i++)
				((ArrayList<Boolean>)result).add(((boolean []) value)[i]);
		}
		if (type.equals("char")){
			for (int i = 0; i < ((char []) value).length; i++)
				((ArrayList<Character>)result).add(((char []) value)[i]);
		}

    	return result;
    }


}