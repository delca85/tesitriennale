package objectdiagram;

import java.util.*;

import util.BuilderArrayList;
import util.DeepClone;
import builder.Builder;
import builder.FlowBuilder;

public abstract class Instance implements Builder {

	protected Object obj;
	protected String name;			//identificatore univoco delle Instance
	protected boolean singleInstance;
	protected BuilderArrayList<Slot> slots;
	protected ObjectDiagramBuilder odb;

	private static HashMap<String, Integer> idCounter = new HashMap<String, Integer>();		//chiavi = classi, oggetti = # istanze create finora

	public Instance(Object o, String name, boolean singleInstance, ObjectDiagramBuilder odb) {
			this.odb = odb;
			this.obj = o;
			this.singleInstance = singleInstance;
			//od fornisce stringa corretta per campo nome
			this.name = odb.getInstanceName(o, name, singleInstance);	
	}	


	
	public String getODName(){
		return odb.getName();
	}
	
	public Object getObject(){
		return obj;
	}

	public String getName(){
		return name;
	}

	public BuilderArrayList<Slot> getSlots() {
		return slots;
	}

	public void setName(String name){
		this.name=name;
	}

	public boolean getSingleInstance() {
		return singleInstance;
	}

	public String getClassName() {
		return obj.getClass().getName();
	}

	public void setODB(ObjectDiagramBuilder odb){
		this.odb = odb;
	}

	public void setSlots(BuilderArrayList<Slot> slots){
		this.slots = DeepClone.deepClone(slots);
	}

	@Override
    public boolean equals(Object other) {
    	if (other instanceof Instance) {
    		Instance otherInstance = (Instance) other;
    		return getODName().equals(otherInstance.getODName()) && getName().equals(otherInstance.getName());		//essendo il nome identificatore univoco, basta quello per determinare se due istanze sono uguali o meno
    	}
    	return false;
    }

	@Override
	public String toString(){
		StringBuffer sb=new StringBuffer();
		sb.append("name: " +name + " object: " + obj + " singleInstance: " + singleInstance + "\n");
		if (slots != null)
			for (Slot s : slots)
					sb.append(s + "\n");
		return sb.toString();
	}

	public abstract Instance clone();

}
