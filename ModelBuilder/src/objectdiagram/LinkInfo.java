package objectdiagram;

import java.lang.reflect.*;

public class LinkInfo {

	private String objectDiagram;
	private String label;
	private String other;

	public LinkInfo(String label, String other, String objectDiagram) {
		this.objectDiagram = objectDiagram;
		this.label = label;
		this.other = other;
	}	
	
	public String getObjectDiagram() {
		return objectDiagram;
	}
	
	public String getLabel() {
		return label;
	}
	
	public String getOther() {
		return other;
	}
	
	
	public void setObjectDiagram(String o) {
		objectDiagram = o;
	}
	
	public void setLabel(String l) {
		label = l;
	}
	
	public void setOther (String o) {
		other = o;
	}
	
	public boolean equals(Object otherO) {
		if (otherO instanceof LinkInfo) {
			LinkInfo otherRelInfo = (LinkInfo) otherO;
			return objectDiagram.equals(otherRelInfo.getObjectDiagram()) && label.equals(otherRelInfo.getLabel()) && other.equals(otherRelInfo.getOther());
		}
		return false;
	}
}