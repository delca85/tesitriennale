package classdiagram;

import builder.Builder;

public abstract class MethodBuilder implements Builder {
	protected String name;
	protected String returnType;
	protected String modifiers;
	protected String[] argsName;
	protected String[] argsType;
	protected ClassDiagramBuilder cd;
	
	public MethodBuilder(ClassDiagramBuilder cd,String name, String returnType, String modifiers,
			String[] argsName,String[] argsType) {
		this.name = name;
		this.returnType = returnType;
		this.modifiers = modifiers;
		this.argsName = argsName;
		this.argsType=argsType;
		this.cd=cd;
	}
	
}
