package classdiagram;

import builder.Builder;

public abstract class FieldBuilder implements Builder {
	protected String name;
	protected String type;
	protected String modifiers;
	protected boolean multiplicity;
	protected ClassDiagramBuilder cd;
	
	public FieldBuilder(ClassDiagramBuilder cd,String name,String type, String modifiers, boolean multiplicity){
		this.name=name;
		this.type=type;
		this.modifiers=modifiers;
		this.multiplicity=multiplicity;
		this.cd=cd;
	}
	
}
