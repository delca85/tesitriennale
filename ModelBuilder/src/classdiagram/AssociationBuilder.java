package classdiagram;

import builder.Builder;

public abstract class AssociationBuilder implements Builder {
	protected String label;
	protected String target;
	protected String sourceLowerValue;
	protected String sourceUpperValue;
	protected String targetLowerValue;
	protected String targetUpperValue;
	protected ClassDiagramBuilder cd;
	
	public AssociationBuilder(ClassDiagramBuilder cd,String label, String target,
			String sourceLowerValue, String sourceUpperValue,
			String targetLowerValue, String targetUpperValue) {
		super();
		this.label = label;
		this.target = target;
		this.sourceLowerValue = sourceLowerValue;
		this.sourceUpperValue = sourceUpperValue;
		this.targetLowerValue = targetLowerValue;
		this.targetUpperValue = targetUpperValue;
		this.cd=cd;
	}
	
	
}
