package classdiagram;



import util.BuilderHashMap;

import builder.Builder;

public abstract class ClassDiagramBuilder implements Builder{
	protected String name;
	protected BuilderHashMap<String,ClassBuilder> cbs;
	protected BuilderHashMap<String,ClassBuilder> ibs;
	public ClassDiagramBuilder(String name) {
		this.name=name;
		cbs = new BuilderHashMap<String,ClassBuilder>();
		ibs= new BuilderHashMap<String,ClassBuilder>();
	}
	public void addInterface(ClassBuilder ib){
		ibs.put(ib.getName(),ib);
	}
	public void addClass(ClassBuilder cb){
		cbs.put(cb.getName(),cb);
	}
	public String getName(){
		return name;
	}
	public abstract ClassBuilder addClass(String name);
	public abstract ClassBuilder addInterface(String name);
	
	public ClassBuilder getType(String name){
		if(cbs.containsKey(name))
			return cbs.get(name);
		else if(ibs.containsKey(name))
			return ibs.get(name);
		else
			throw new RuntimeException("Type "+name+" not found!");
	}
}
