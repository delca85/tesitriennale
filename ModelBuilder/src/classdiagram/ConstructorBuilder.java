package classdiagram;

import builder.Builder;

public abstract class ConstructorBuilder implements Builder{
	protected String modifiers;
	protected String[] argsName;
	protected String[] argsType;
	protected ClassDiagramBuilder cd;
	
	public ConstructorBuilder(ClassDiagramBuilder cd,String modifiers, String[] argsName,
			String[] argsType) {
		super();
		this.modifiers = modifiers;
		this.argsName = argsName;
		this.argsType = argsType;
		this.cd=cd;
	}
	
}
