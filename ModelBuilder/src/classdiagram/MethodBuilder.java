package classdiagram;

import builder.Builder;

public abstract class MethodBuilder implements Builder {
	protected String name;
	protected String returnType;
	protected int modifiers;
	protected String[] argsName;
	protected String[] argsType;
	protected String[] exceptions;
	protected ClassDiagramBuilder cd;
	
	// public MethodBuilder(ClassDiagramBuilder cd,String name, String returnType, String modifiers,
	// 		String[] argsName,String[] argsType) {
	// 	this.name = name;
	// 	this.returnType = returnType;
	// 	this.modifiers = modifiers;
	// 	this.argsName = argsName;
	// 	this.argsType=argsType;
	// 	this.cd=cd;
	// }
	
	public MethodBuilder(ClassDiagramBuilder cd,String name, String returnType, int modifiers,
			String[] argsName,String[] argsType, String[] exceptions) {
		// this(cd, name, returnType, modifiers, argsName, argsType);
		this.name = name;
		this.returnType = returnType;
		this.modifiers = modifiers;
		this.argsName = argsName;
		this.argsType=argsType;
		this.cd=cd;
		this.exceptions=exceptions;
	}
	
}
