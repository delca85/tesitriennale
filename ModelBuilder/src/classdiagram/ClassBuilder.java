package classdiagram;

import util.BuilderArrayList;

import builder.Builder;

public abstract class ClassBuilder implements Builder {
	protected String name;
	protected boolean isAbstract;
	protected String ext;
	protected String visibility;
	protected String[] impls;
	protected ClassDiagramBuilder cd;
	protected BuilderArrayList<FieldBuilder> fbs;
	protected BuilderArrayList<MethodBuilder> mbs;
	protected BuilderArrayList<ConstructorBuilder> cbs;
	protected BuilderArrayList<AssociationBuilder> abs;
	
	public ClassBuilder(ClassDiagramBuilder cd,String name,String visibility,boolean isAbstract,String ext,String[] impls){	
		fbs=new BuilderArrayList<FieldBuilder>();
		mbs=new BuilderArrayList<MethodBuilder>();
		cbs=new BuilderArrayList<ConstructorBuilder>();
		abs=new BuilderArrayList<AssociationBuilder>();
		this.visibility=visibility;
		this.name=name;
		this.isAbstract=isAbstract;
		this.ext=ext;
		this.impls=impls;
		this.cd=cd;
	}
	public abstract FieldBuilder addField(String name,String type,String modifiers, boolean multiplicity);
	// public abstract MethodBuilder addMethod(String name,String returnType,String modifiers,String[] argsName,String[] argsType);
	
	public abstract MethodBuilder addMethod(String name,String returnType,int modifiers,String[] argsName,String[] argsType, String[] exceptions);
	public abstract ConstructorBuilder addConstructor(String modifiers,String[] argsName,String[] argsType);
	public abstract AssociationBuilder addAssociation(String target, String label,
			String sourceLowerValue, String sourceUpperValue,
			String targetLowerValue, String targetUpperValue);
	public boolean isAbstract() {
		return isAbstract;
	}
	public void setAbstract(boolean isAbstract) {
		this.isAbstract = isAbstract;
	}
	public String getName() {
		return name;
	}
	public void addField(FieldBuilder fb){
		fbs.add(fb);
	}
	public void addMethod(MethodBuilder mb){
		mbs.add(mb);
	}
	public void addConstructor(ConstructorBuilder cb) {
		cbs.add(cb);
	}
	public void addAssociation(AssociationBuilder ab){
		abs.add(ab);
	}
	public void addExtends(String ext){
		this.ext=ext;
	}
	public void addImplements(String[] impls){
		this.impls=impls;
	}
	public String getModifiers() {
		return visibility;
	}
	
}
