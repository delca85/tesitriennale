package classdiagram;

import util.BuilderArrayList;
import builder.Builder;

public abstract class InterfaceBuilder implements Builder {
	protected String name;
	protected String modifiers;
	protected String ext;
	protected BuilderArrayList<MethodBuilder> mbs;
	
	
	public InterfaceBuilder(String name, String modifiers, String ext){
		this.name = name;
		this.modifiers = modifiers;
		this.ext = ext;
		mbs=new BuilderArrayList<MethodBuilder>();
	}
	public String getName() {
		return name;
	}
	public void addMethod(MethodBuilder mb){
		mbs.add(mb);
	}
}
