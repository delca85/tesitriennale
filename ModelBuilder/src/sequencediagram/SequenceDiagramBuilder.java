package sequencediagram;

import builder.Builder;
import builder.FlowBuilder;
import util.BuilderArrayList;
import util.BuilderHashMap;

public abstract class SequenceDiagramBuilder implements Builder, FlowBuilder {
	protected String name;
	protected BuilderHashMap<String, Lifeline> lifelines;
	protected BuilderArrayList<Message> messages;
		
	public SequenceDiagramBuilder(String name) {
		this.name = name;
		lifelines = new BuilderHashMap<String, Lifeline>();
		messages = new BuilderArrayList<Message>();
	}
	public String getName(){
		return name;
	}
	
	public void addLifeline(Lifeline ce){
		if(!lifelines.containsKey(ce.getName())){
			lifelines.put(ce.getName(),ce);
		}
			
	}
	
	public void addMessage(Message ce){
		messages.add(ce);
	}
	
	public void addInsideMessage(Message ce){
		messages.get(messages.size()-1).addInsideMessage(ce);
	}

	public Lifeline getElement(String name) {
		Lifeline ce=lifelines.get(name);
		if (ce!=null)
			return ce;
		/*else {
			Message m=messages.get(name);
			if (m!=null)
				return m;
		}*/
		throw new RuntimeException("Lifeline not found : " + name);
	}

	public BuilderHashMap<String, Lifeline> getLifelines() {
		return lifelines;
	}
	
	public BuilderArrayList<Message> getMessages() {
		return messages;
	}

	
}
