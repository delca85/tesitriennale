package sequencediagram;

import java.util.*;

import util.BuilderArrayList;
import builder.Builder;
import builder.FlowBuilder;

public abstract class Lifeline implements Builder {

	protected String name;
	protected SequenceDiagramBuilder sd;
	protected String varName;
	protected String clazz;

	public Lifeline(String name, String clazz, SequenceDiagramBuilder sd,String varName) {
		this.name = name;
		this.sd = sd;
		this.varName = varName;
		this.clazz = clazz;
	}
	
	public String getSDName(){
		return sd.getName();
	}
	public String getVarName(){
		return varName;
	}
	public void setVarName(String varName){
		this.varName=varName;
	}
	public String getName(){
		return name;
	}
	public String getLifelineClass(){
		return clazz;
	}

}
