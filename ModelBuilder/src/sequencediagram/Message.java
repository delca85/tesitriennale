package sequencediagram;

import java.util.*;
import java.lang.reflect.*;

import util.BuilderArrayList;
import builder.Builder;
import builder.FlowBuilder;

public abstract class Message implements Builder {

	protected Method method;
	protected SequenceDiagramBuilder sd;
	protected String varName;
	protected String sender;
	protected String receiver;
	protected Class receiverClass;
	protected Class senderClass;
	protected ArrayList<Message> insideMessage;
	protected boolean inside;
	protected Object[] params;

	public Message(SequenceDiagramBuilder sd,String varName, String sender, String receiver, Class senderClass, Class receiverClass, Method m, Object[] params) {
		this.sd = sd;
		this.varName = varName;
		this.sender = sender;
		this.receiver = receiver;
		this.senderClass = senderClass;
		this.receiverClass = receiverClass;		
		this.method = m;
		this.insideMessage = new ArrayList<Message>();
		this.inside = false;
		this.params = params;
	}
	
	public Message(SequenceDiagramBuilder sd,String varName, String sender, String receiver, Class senderClass, Class receiverClass, Method m, boolean inside, Object[] params) {
		this.sd = sd;
		this.varName = varName;
		this.sender = sender;
		this.receiver = receiver;
		this.senderClass = senderClass;
		this.receiverClass = receiverClass;		
		this.method = m;
		this.insideMessage = new ArrayList<Message>();
		this.inside = inside;
		this.params = params;
	}
	
	public String getSDName(){
		return sd.getName();
	}
	public String getVarName(){
		return varName;
	}
	public void setVarName(String varName){
		this.varName=varName;
	}
	public Method getMethod(){
		return method;
	}
	public String getSender(){
		return sender;
	}
	public String getReceiver(){
		return receiver;
	}
	
	public void addInsideMessage(Message m) {
		this.insideMessage.add(m);
	}
	
	public boolean isInside() {
		return inside;
	}
	
	public void setInside(boolean inside) {
            this.inside = inside;
	}
	
	public Object[] getParams() {
            return params;
	}
	
	public void setSender(String sender) {
            this.sender = sender;
        }
        
        public void setReceiver(String receiver) {
            this.receiver = receiver;
        }
}