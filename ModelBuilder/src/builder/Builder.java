package builder;

public interface Builder{
	public String build();
}
