package builder;

public interface FlowBuilder {
	public String buildFlows();
}
