package activitydiagram;

import util.BuilderArrayList;

public abstract class PinnableElement extends ControllableElement {

	protected BuilderArrayList<Pin> pins;

	public PinnableElement(String name, String language, String body, ActivityDiagramBuilder ad, String varName) {
		super(name, language, body, ad,varName);
		pins=new BuilderArrayList<Pin>();
	}
	
	public PinnableElement(String name, ActivityDiagramBuilder ad, String varName) {
		super(name, ad,varName);
		pins=new BuilderArrayList<Pin>();
	}
	public PinnableElement(String name, String behavior, ActivityDiagramBuilder ad, String varName) {
		super(name, behavior, ad,varName);
		pins=new BuilderArrayList<Pin>();
	}
	
	public abstract void addInputPin(String name, String type, int upper,
			int lower);

	public abstract void addOutputPin(String name, String type, int upper,
			int lower);

	public void addPin(Pin p) {
		pins.add(p);
	}
}
