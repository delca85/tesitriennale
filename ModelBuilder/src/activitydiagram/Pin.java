package activitydiagram;

import builder.Builder;

public abstract class Pin implements Builder{
	protected String name;
	protected String type;
	protected int upper;
	protected int lower;
	public Pin(String name, String type, int upper, int lower) {
		super();
		this.name = name;
		this.type = type;
		this.upper = upper;
		this.lower = lower;
	}
	public String getName() {
		return name;
	}
	public String getType() {
		return type;
	}
	public int getUpper() {
		return upper;
	}
	public int getLower() {
		return lower;
	}
	
}
