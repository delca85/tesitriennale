package activitydiagram;

public interface Composite {
	public ControllableElement getElement(String name);
	
	public PinnableElement addAcceptEvent(String name);

	public PinnableElement addCallAction(String name, String language, String body);

	public CompositeElement addPartition(String name);

	public CompositeElement addConditional(String name);

	public ControllableElement addDecision(String name);

	public ControllableElement addFinal(String name);

	public ControllableElement addInit(String name);

	public ControllableElement addFork(String name);

	public ControllableElement addJoin(String name);

	public ControllableElement addMerge(String name);

	public CompositeElement addLoop(String name);

	public PinnableElement addSendSignal(String name);

	public CompositeElement addStructured(String name);

}
