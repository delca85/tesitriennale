package activitydiagram;

import util.BuilderHashMap;
import builder.Builder;

public abstract class CompositeElement extends PinnableElement implements Composite {
	
	protected BuilderHashMap<String,ControllableElement> activities;
	
	public CompositeElement(String name,ActivityDiagramBuilder ad,String varName) {
		super(name,ad,varName);
		activities=new BuilderHashMap<String,ControllableElement>();
	}
	
	public void addActivity(ControllableElement ab){
		activities.put(ab.getName(),ab);
	}
	public ControllableElement getElement(String name){
		ControllableElement ret=activities.get(name);
		if(ret!=null)
				return ret; 
		throw new RuntimeException("Element "+name+" not found");
	}
	public BuilderHashMap<String,ControllableElement> getActivities(){
		return activities;
	}
	
	
}
