package activitydiagram;

import builder.Builder;

public abstract class ControlFlow implements Builder{
	protected String target;
	protected String name;
	protected String guard;
	protected String weight;
	protected String language;
	
	public ControlFlow(String target, String name, String guard, String weight) {
		super();
		this.target = target;
		this.name = name;
		this.guard = guard;
		this.weight = weight;
		this.language = "";
	}
	public ControlFlow(String target, String name, String language, String guard, String weight) {
		super();
		this.target = target;
		this.name = name;
		this.guard = guard;
		this.weight = weight;
		this.language = language;
	}
	public ControlFlow(String target){
		this(target,"","","");
	}
	public ControlFlow(String target,String name){
		this(target,name,"","");
	}
	public void setName(String name){
		this.name=name;
	}
	public void setGuard(String guard){
		this.guard=guard;
	}
	public String getTarget() {
		return target;
	}
	public String getName() {
		return name;
	}
	public String getLanguage() {
		return name;
	}
	public String getGuard() {
		return guard;
	}
	public String getWeight() {
		return weight;
	}
	
}
