package activitydiagram;

import builder.Builder;

public abstract class ActivityAttribute implements Builder {
	protected String name;
	protected String clazz;
	protected String modifier;
	protected boolean isStatic;
	protected ActivityDiagramBuilder ad;	
	
	public ActivityAttribute(String name,String clazz, ActivityDiagramBuilder ad){
		this.name=name;
		this.clazz=clazz;
		this.modifier="public";
		this.isStatic=false;
		this.ad=ad;
	}
	
	public ActivityAttribute(String name,String clazz, String modifier, boolean isStatic, ActivityDiagramBuilder ad){
		this.name=name;
		this.clazz=clazz;
		this.modifier=modifier;
		this.isStatic=isStatic;
		this.ad=ad;
	}
	
	public String getADName(){
		return ad.getName();
	}
	public String getName(){
		return name;
	}
	public String getClazz(){
		return clazz;
	}
	public String getModifier(){
		return modifier;
	}
	public boolean isStatic(){
		return isStatic;
	}
	
}
