package activitydiagram;

import java.util.*;

import util.BuilderArrayList;

public abstract class ControllableElement extends ElementBuilder{

	protected BuilderArrayList<ControlFlow> flows;

	public ControllableElement(String name,String language, String body,ActivityDiagramBuilder ad,String varName) {
		super(name, language, body, ad,varName);
		flows=new BuilderArrayList<ControlFlow>();
	}
	
	public ControllableElement(String name,ActivityDiagramBuilder ad,String varName) {
		super(name, ad,varName);
		flows=new BuilderArrayList<ControlFlow>();
	}
	
	public ControllableElement(String name,String behavior, ActivityDiagramBuilder ad,String varName) {
		super(name, behavior, ad,varName);
		flows=new BuilderArrayList<ControlFlow>();
	}
	
	public abstract ControlFlow addCFlowTo(String to, String name, String language, String guard,String weight);

	public abstract ControlFlow addCFlowFrom(String from, String name, String guard,String weight);

	protected void addControlFlow(ControlFlow cf) {
		flows.add(cf);
	}
	public ControlFlow getControlFlow(String to){
		Iterator<ControlFlow> i=flows.iterator();
		ControlFlow cf;
		while(i.hasNext()){
			cf=i.next();
			if(cf.getTarget().equals(to)){
				return cf;
			}
		}
		return null;
	}
	public BuilderArrayList<ControlFlow> getFlows(){
		return flows;
	}
	public boolean removeCFlowTo(String to){
		Iterator<ControlFlow> i=flows.iterator();
		ControlFlow cf;
		while(i.hasNext()){
			cf=i.next();
			System.out.println(cf);
			if(cf.getTarget().equals(to)){
				flows.remove(cf);
				System.out.println("CF FOUND!");
				return true;
			}
		}
		return false;
	}
	public boolean isCFlowToSet(String to){
		Iterator<ControlFlow> i=flows.iterator();
		ControlFlow cf;
		while(i.hasNext()){
			cf=i.next();
			if(cf.getTarget().equals(to)){
				return true;
			}
		}
		return false;
	}
}
