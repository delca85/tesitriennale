package activitydiagram;

import builder.Builder;
import builder.FlowBuilder;
import util.BuilderArrayList;
import util.BuilderHashMap;

public abstract class ActivityDiagramBuilder implements Builder,Composite,FlowBuilder {
	protected String name;
	protected BuilderHashMap<String, ControllableElement> elements;
	protected BuilderHashMap<String, ActivityAttribute> attributes;

	
	public ActivityDiagramBuilder(String name) {
		this.name = name;
		elements = new BuilderHashMap<String, ControllableElement>();
		attributes = new BuilderHashMap<String, ActivityAttribute>();
	}
	public String getName(){
		return name;
	}
	public void addActivity(ControllableElement ce){
		if(!elements.containsKey(ce.getName())){
			elements.put(ce.getName(),ce);
		}
			
	}
	
        public void addAttribute(ActivityAttribute attr) {
            if(!attributes.containsKey(attr.getName())){
			attributes.put(attr.getName(),attr);
            }
	}
	
	public ControllableElement getElement(String name) {
		ControllableElement ce=elements.get(name);
		if (ce!=null)
			return ce;
		else
			throw new RuntimeException("Element not found : " + name);
	}

	public BuilderHashMap<String, ControllableElement> getElements() {
		return elements;
	}

	
}
