package activitydiagram;

import builder.Builder;
import builder.FlowBuilder;

public abstract class ElementBuilder implements Builder,FlowBuilder {
	protected String name;
	protected String language;
	protected String body;
	protected String behavior;
	protected ActivityDiagramBuilder ad;
	//protected String insideOf;
	protected CompositeElement insideOf;
	protected String varName;
	
	public ElementBuilder(String name,String language, String body,ActivityDiagramBuilder ad,String varName){
		this.name=name;
		this.language=language;
		this.body=body;
		this.ad=ad;
		this.varName=varName;
		this.behavior="";
	}
	public ElementBuilder(String name,ActivityDiagramBuilder ad,String varName){
		this.name=name;
		this.language="";
		this.body="";
		this.ad=ad;
		this.varName=varName;
		this.behavior="";
	}
	public ElementBuilder(String name,String behavior, ActivityDiagramBuilder ad,String varName){
		this.name=name;
		this.language="";
		this.body="";
		this.ad=ad;
		this.varName=varName;
		this.behavior=behavior;
	}
	public String getADName(){
		return ad.getName();
	}
	public String getVarName(){
		return varName;
	}
	public void setVarName(String varName){
		this.varName=varName;
	}
	public String getName(){
		return name;
	}
	public CompositeElement getInsideOf(){
		return insideOf;
	}
	public void setInsideOf(CompositeElement insideOf) {
		this.insideOf=insideOf;
	}
	
}
