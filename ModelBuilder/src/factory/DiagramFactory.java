package factory;


import util.BuilderHashMap;
import util.BuilderLinkedHashMap;

import activitydiagram.ActivityDiagramBuilder;


import classdiagram.ClassDiagramBuilder;
import sequencediagram.SequenceDiagramBuilder;
import objectdiagram.ObjectDiagramBuilder;
import exporter.Exporter;

public abstract class DiagramFactory implements Exporter{
	
	protected BuilderHashMap<String,ClassDiagramBuilder> classDiagrams;
	protected BuilderLinkedHashMap<String,ActivityDiagramBuilder> activityDiagrams;
	protected BuilderHashMap<String,SequenceDiagramBuilder> sequenceDiagrams;
	protected BuilderHashMap<String,ObjectDiagramBuilder> objectDiagrams;
	
	protected DiagramFactory(){
		classDiagrams=new BuilderHashMap<String,ClassDiagramBuilder>();
		activityDiagrams=new BuilderLinkedHashMap<String,ActivityDiagramBuilder>();
		sequenceDiagrams= new BuilderHashMap<String, SequenceDiagramBuilder>();
		objectDiagrams= new BuilderHashMap<String, ObjectDiagramBuilder>();
	}
	

	public abstract ClassDiagramBuilder getClassDiagram(String name);
	public abstract ActivityDiagramBuilder getActivityDiagram(String name);
	public abstract SequenceDiagramBuilder getSequenceDiagram(String name);
	public abstract ObjectDiagramBuilder getObjectDiagram(String name);
	public abstract void buildClassDiagrams();
	public abstract void buildActivityDiagrams();
	public abstract void buildSequenceDiagrams();
	public abstract void buildObjectDiagrams(String classDiagramFile);
	public abstract void buildSequenceDiagrams(String classDiagramFile);
	public abstract void buildActivityDiagrams(String classDiagramFile);
}
