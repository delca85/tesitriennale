package util;

import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;

import activitydiagram.CompositeElement;
import builder.Builder;
import builder.FlowBuilder;

public class BuilderHashMap<K,V extends Builder> extends HashMap<K, V> implements Builder,FlowBuilder {

	//@Override
	public String build() {
		StringBuffer sb=new StringBuffer();
		Iterator<V> i=this.values().iterator();
		while(i.hasNext()){
			sb.append(i.next().build());
		}
		return sb.toString();
	}
	
	//@Override
	public String buildFlows() {
		StringBuffer sb=new StringBuffer();
		Iterator<V> i=this.values().iterator();
		while(i.hasNext()){
			sb.append(((FlowBuilder)i.next()).buildFlows());
		}
		return sb.toString();
	}
	
	//@Override
	public V get(Object key){
		V ret=null;
		Iterator<K> i=keySet().iterator();
		if(containsKey(key))
			return super.get(key);
		
		while(i.hasNext()){
			K item=i.next();
			V value=super.get(item);
			if(item.equals(key))
				return value;
			if(value instanceof CompositeElement){
				value=(V) ((CompositeElement)value).getElement(key.toString());
				if(value!=null)
					return value;
			}
		}
			
		return ret;
	}
}
