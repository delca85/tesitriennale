package util;

import java.util.LinkedHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

import activitydiagram.CompositeElement;
import builder.Builder;
import builder.FlowBuilder;

public class BuilderLinkedHashMap<K,V extends Builder> extends LinkedHashMap<K, V> implements Builder,FlowBuilder {

	//@Override
	public String build() {
		StringBuffer sb=new StringBuffer();
		//Iterator<V> i=this.values().iterator();
		List<V> list = new ArrayList(this.values());
		for( int i = list.size() -1; i >= 0 ; i --){
			sb.append(list.get(i).build());
		}
		/*while(i.hasNext()){
			sb.append(i.next().build());
		}*/		
		return sb.toString();
	}
	
	//@Override
	public String buildFlows() {
		StringBuffer sb=new StringBuffer();
		Iterator<V> i=this.values().iterator();
		while(i.hasNext()){
			sb.append(((FlowBuilder)i.next()).buildFlows());
		}
		return sb.toString();
	}
	
	//@Override
	public V get(Object key){
		V ret=null;
		Iterator<K> i=keySet().iterator();
		if(containsKey(key))
			return super.get(key);
		
		while(i.hasNext()){
			K item=i.next();
			V value=super.get(item);
			if(item.equals(key))
				return value;
			if(value instanceof CompositeElement){
				System.out.println("compo search");
				value=(V) ((CompositeElement)value).getElement(key.toString());
				if(value!=null)
					return value;
			}
		}
			
		return ret;
	}
}
