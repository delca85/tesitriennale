/** 
 *  Deepclone for collection - Bianca Del Carretto
 */

package util;

import builder.Builder;
import objectdiagram.*;
import java.util.*;
import java.lang.reflect.Array;


public final class DeepClone {

    private DeepClone(){}

    public static <X> X deepClone(final X input) {
        if (input == null) {
            return input;
        } else if (input instanceof Instance) {
            return (X) ((Instance) input).clone();
        } else if (input instanceof Link) {
            return (X) ((Link) input).clone();
        } else if (input instanceof Slot) {
            return (X) ((Slot) input).clone();  
        } else if (input instanceof BuilderHashMap<?, ?>) {
            return (X) deepCloneBuilderHashMap((BuilderHashMap<?, ?>) input);
        } else if (input instanceof BuilderArrayList<?>){
            return (X) deepCloneBuilderArrayList((BuilderArrayList<?>) input);
        } else if (input instanceof Map<?, ?>) {
            return (X) deepCloneMap((Map<?, ?>) input);
        } else if (input instanceof Collection<?>) {
            return (X) deepCloneCollection((Collection<?>) input);
        } else if (input instanceof Object[]) {
            return (X) deepCloneObjectArray((Object[]) input);
        } else if (input.getClass().isArray()) {
            return (X) clonePrimitiveArray((Object) input);
        }

        return input;
    }

    private static Object clonePrimitiveArray(final Object input) {
        final int length = Array.getLength(input);
        final Object copy = Array.newInstance(input.getClass().getComponentType(), length);
        // deep clone not necessary, primitives are immutable
        System.arraycopy(input, 0, copy, 0, length);
        return copy;
    }

    private static <E> E[] deepCloneObjectArray(final E[] input) {
        final E[] clone = (E[]) Array.newInstance(input.getClass().getComponentType(), input.length);
        for (int i = 0; i < input.length; i++) {
            clone[i] = deepClone(input[i]);
        }

        return clone;
    }

    private static <E> Collection<E> deepCloneCollection(final Collection<E> input) {
        Collection<E> clone;
        // this is of course far from comprehensive. extend this as needed
        if (input instanceof LinkedList<?>) {
            clone = new LinkedList<E>();
        } else if (input instanceof SortedSet<?>) {
            clone = new TreeSet<E>();
        } else if (input instanceof Set) {
            clone = new HashSet<E>();
        } else {
            clone = new ArrayList<E>();
        }

        for (E item : input) {
            clone.add(deepClone(item));
        }

        return clone;
    }

    private static <E extends Builder> Collection<E> deepCloneBuilderArrayList(final Collection<E> input) {
        BuilderArrayList<E> clone = new BuilderArrayList<E>();
        for (E item : input) {
            clone.add(deepClone(item));
        }

        return clone;
    }

    private static <K, V extends Builder> BuilderHashMap<K, V> deepCloneBuilderHashMap (final Map<K, V> map){
        BuilderHashMap<K, V> clone = new BuilderHashMap<K, V>();
        for (Map.Entry<K, V> entry : map.entrySet()) {
            clone.put(deepClone(entry.getKey()), deepClone(entry.getValue()));
        }

        return clone;
    }

    private static <K, V> Map<K, V> deepCloneMap(final Map<K, V> map) {
        Map<K, V> clone;
        // this is of course far from comprehensive. extend this as needed
        if (map instanceof LinkedHashMap<?, ?>) {
            clone = new LinkedHashMap<K, V>();
        } else if (map instanceof TreeMap<?, ?>) {
            clone = new TreeMap<K, V>();
        } else {
            clone = new HashMap<K, V>();
        }
        for (Map.Entry<K, V> entry : map.entrySet()) {
            clone.put(deepClone(entry.getKey()), deepClone(entry.getValue()));
        }
        return clone;
    }
}