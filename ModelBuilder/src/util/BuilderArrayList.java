package util;

import java.util.ArrayList;
import java.util.Iterator;

import builder.Builder;
import builder.FlowBuilder;

public class BuilderArrayList<E extends Builder> extends ArrayList<E> implements Builder,FlowBuilder {
//	@Override
	public String build() {
		StringBuffer sb=new StringBuffer();
		Iterator<E> i=iterator();
		while(i.hasNext()){
			sb.append(i.next().build());
		}
		return sb.toString();
	}

//	@Override
	public String buildFlows() {
		StringBuffer sb=new StringBuffer();
		Iterator<E> i=iterator();
		while(i.hasNext()){
			sb.append(((FlowBuilder)i.next()).buildFlows());
		}
		return sb.toString();
	}	
}
