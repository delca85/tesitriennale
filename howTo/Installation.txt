HOW TO INSTALL REVERSER

TOOLS NEEDED:

- java-6
- aspectj 1.7
- bcel-5.2
- atjava r125 + patch + "executable"
- ataspectj

INSTRUCTION:

java 6: Install java 6 and make sure to use it. Download it from: http://www.oracle.com/technetwork/java/javasebusiness/downloads/java-archive-downloads-javase6-419409.html
aspectj: Install aspectj from https://eclipse.org/aspectj/downloads.php.
Download bcel-5.2 from http://www.java2s.com/Code/Jar/b/Downloadbcel52jar.htm and put it in your CLASSPATH.

atjava:	(https://thor.disi.unige.it/svn/atjava/trunk/)
        - add /path/to/aspectj1.8/lib/aspectjrt.jar to your CLASSPATH
		- Modify your PATH to include /path/to/aspectj1.8/bin
		- edit build.xml: in taskdef ajc fileset dir according to your path to aspectj
		- ant compile_1
		- copy mypatch-rtjar.sh in the home directory of atjava, if it isn’t already there
		- edit mypatch-rtjar.sh in accord to the path of your machine (RTJAR, ATJAVA_HOME) and run it
		- ant jar to generate the jar files
		- copy in bin directory the "executable" atjava and atjavac (if they aren’t already there)
		- add /path/to/atjava/bin/ to your PATH
		- edit the BOOTCP variable in bin/atjava according to your machine path
		- add to your CLASSPATH /path/to/atjava/build/classes

ataspectj:	(https://thor.disi.unige.it/svn/ataspectj/trunk/ataspectj/)
		- edit build.xml in accord to the path of your machine (value of property with name="java.lang.reflect")
		- ant compile
		- ant jar
		- add ataspectjc, adding /path/to/ataspectj to your PATH

ramses:		(https://adapt-lab.di.unimi.it/svn/refactoring/trunk/ramsesRSA)
		- mkdir bin
		- ant compile
		- ant jar
		- add to your CLASSPATH ramses.jar, /path/to/ramsesRSA/lib/xercesImpl.jar, /path/to/ramsesRSA/lib/xml-apis.jar

reverser:	(https://adapt-lab.di.unimi.it/svn/reverser/trunk/reverserRSA/)
		- cd ModelBuilder
		- run compile.sh (check path but they should be ok)
		- cd ScriptBuilder
		- run compile.sh (check path but they should be ok)
		- cd CodeToDiagram
		- copy all the stuff inside confAndScript/ in CodeToDiagram renaming them without “standard_”
		- edit the path inside all of the file just copied
		- ant compile
